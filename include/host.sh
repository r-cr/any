
BINHOSTBASEDIR=${PREFIX}/bin
BINHOSTTCHDIR=${PREFIX}/${any_host_tch_dir}/bin
BINHOSTPACKDIR=${PREFIX}/${any_host_pkg_dir}/bin

env_host_prior_set()
{

    PATH=${BINHOSTPACKDIR}:${BINHOSTBASEDIR}:${PATH}

}

env_host_additional_set()
{
    PATH=${PATH}:${BINHOSTPACKDIR}:${BINHOSTBASEDIR}
}

# including this, we declare need in explicit host binaries in addition to the system-wide ones
# when sandboxed, we do NOT need that, as system-wide environment is sane


env_host_prior_set
