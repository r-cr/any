vars="$(set | grep -e '^use_[a-z0-9]*=' | grep -v -e '^use_all=' || nonfail)"

draft_use=""
for content in ${use_all} ; do
    draft_use="${draft_use} ${content}"
done

for single_var in ${vars} ; do
    content="${single_var%%=*}" || nonfail
    content="${content#use_}" || nonfail
    bool_flag=${single_var#use_${content}=} || nonfail

    if test -n "${bool_flag}" && test "${bool_flag}" = 'true' ; then
        bool_flag='1'
    fi
    if test -n "${bool_flag}" && test "${bool_flag}" = 'false' ; then
        bool_flag='0'
    fi

    if test -n "${content}" -a "${bool_flag}" = '1' ; then
        draft_use="${draft_use} ${content}"
    fi
    if test -n "${content}" -a "${bool_flag}" = '0' ; then
        draft_use="${draft_use} -${content}"
    fi
done

prepared_use=
for flag in ${draft_use} ; do
    check_flag="${flag#-}" || nonfail
    if test "${check_flag}" != "${flag}" ; then
        prepared_use="$(printf "%s\n" "${prepared_use}" | \
            sed -e "s/ ${check_flag} / /g" -e "s/ ${check_flag}\$//" || nonfail)"
    else
        prepared_use="${prepared_use} ${flag}"
    fi
done

USE="${prepared_use}"

unset content single_var bool_flag check_flag vars prepared_use draft_use
