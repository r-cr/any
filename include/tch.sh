if test "${any_bool_loaddump}" != '1' ; then
    if test -x "$(local_which "${PLAIN_CC}" 2> /dev/null)" ; then

        export LD="$( ${PLAIN_CC} -print-prog-name=ld )"
        export CXXLD="$( ${PLAIN_CXX} -print-prog-name=ld )"
        export AS="$( ${PLAIN_CC} -print-prog-name=as )"

        export RANLIB="$( ${PLAIN_CC} -print-prog-name=ranlib )"
        export STRIP="$( ${PLAIN_CC} -print-prog-name=strip )"
        export OBJDUMP="$( ${PLAIN_CC} -print-prog-name=objdump )"
        export AR="$( ${PLAIN_CC} -print-prog-name=ar )"
        export NM="$( ${PLAIN_CC} -print-prog-name=nm )"

    fi
fi
