export LD="${PLAIN_CCLD}"
export CXXLD="${PLAIN_CXXLD}"

export AS="as"
export AR="ar"
export RANLIB="ranlib"
export STRIP="strip"
export NM="nm"
export OBJDUMP="objdump"
