##
## samples of setting up configuration for your platform
##

## very common variant: linux-based os, gcc toolchain,
## amd64-compatible cpu, simple usage of toolchain and environment
##

[id]
rdd_prf_id = any, amd64, gcc, linux

##
## OpenBSD OS, clang toolchain, amd64-compatible cpu
##

# [id]
# rdd_prf_id = any, amd64, clang, openbsd

## -------------------------------------------------
##
## extend your configuration here: add data words you need
## (section 'all' is included inside section 'any' with highest priority)
##

[all]
rdd_prf_id = plainroot, plaincc, tch, plainpath

## example of full set for specific project
##

# [all]
# rdd_prf_id = fetchstatic, deb, packdeb, plaincc, tch

## -------------------------------------------------
##
## useful opts

## use this for creating packages with proper system permissions on files inside
# any_util_root = fakeroot

## add text id 'abcd' into version string for all packages.
## useful to detect locally built packages.
# any_release_id = abcd

## -------------------------------------------------
##
## essential combinations for compiler behaviour and toolchain:
##

## plain compiler, no control over build dependcies,
## root is entire filesystem /,
## toolchain elements are initialised with -print-prog-name
## (default set)
## (the most close to manual building)

# [all]
# rdd_prf_id = plainroot, tch

## plain compiler, build dependcies are controlled,
## toolchain elements are initialised with -print-prog-name

# [all]
# rdd_prf_id = plaincc, tch

## compiler with wrapper, which functions are controlled with any_wrap_*
## toolchain elements are initialised with -print-prog-name
## (the most feature-rich set)

# [all]
# rdd_prf_id = wrapcc, tch

## plain compiler, no control over build dependencies,
## root is /,
## toolchain elements are initialised with static values like 'ar', 'ld'

# [all]
# rdd_prf_id = plainroot, plaintch

## additional mode to keep build files and sources closer to root directory
## (see description of plainpath below)
# [all]
# rdd_prf_id = plainroot, plaintch, plainpath


## compiler with wrapper, no control over build dependencies,
## root is /,
## toolchain elements are initialised with -print-prog-name
## (this could be used for simple cross-building)

# [all]
# rdd_prf_id = plainroot, wrapcc, tch

## -------------------------------------------------
## engine build modes
##
## add these data words to your configuration for desired effect
## do not replace your entire config string, add only values after
## rdd_prf_id =
##

## makes the working directory look like this:
##    ./package-1.2.3/
##    ./src/package-1.2.3.tgz
##    ./patch/package-1.2.3/one.patch
##    ./patch/package-1.2.3/two.patch

# rdd_prf_id = plainpath

## turns off the control over build context
## that means, build files can be taken directly from /usr/include, /usr/lib
## details:
## plaincc mode is turned on
## DEPEND variable is ignored
## ROOT is /
## nothing is written to ROOT by the engine
## pkg_root, pkg_postroot, pkg_preroot are not called

# rdd_prf_id = plainroot

## use wrapper over c and c++ compiler, which will enforce options,
##   needed for platform
## wrappers also are able to enforce to read all build files
##   from isolated context directory
##

# rdd_prf_id = wrapcc

## turns off wrappers around compiler
## compiler is used directly and no extra-functionality is applied
##

# rdd_prf_id = plaincc

## init variables of toolchain:
##   tch inits with the call of compiler
##   plaintch inits with statically values ('ar', 'ld')

# rdd_prf_id = tch
# rdd_prf_id = plaintch

## with this word build context becomes shared between all packages,
##   so each package reads and installs context into single directory
## without this word (by default) build context is individual for each package
##

# rdd_prf_id = staticdeproot

## if you want to build something for host, use this word
##

# rdd_prf_id = host

## when cross-building, use the word to determine specific actions
##   in build files
##

# rdd_prf_id = cross

## when building in native, use the word to determine specific actions
##   in build files
## one word from 'native' and 'cross' is enough, choose an appropriate one
##   for your project
##

# rdd_prf_id = native
