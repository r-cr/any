#!/bin/sh

COPY=${COPY:-"cp -Rf"}
MKDIR=${MKDIR:-"mkdir -p"}
DESTDIR=${DESTDIR:-"${HOME}/rrr"}
PREFIX=${PREFIX:-}
P=${P:-"any"}
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${P}}"
MANDIR="${MANDIR:-${PREFIX}/share/man}"
ENGINEDIR="${ENGINEDIR:-${PREFIX}/share/any/workdir}"

install_engine()
{
    ${MKDIR} "${DESTDIR}${ENGINEDIR}/"
    ${COPY} ./. "${DESTDIR}${ENGINEDIR}/"
}

set -o xtrace

WORKDIR=${WORKDIR:-}
COMMANDS=${COMMANDS:-}

while test -n "$1" ; do
    arg="$1"
    case $arg in
        *=*)
            value="${arg#*=}"
            varname="${arg%%=*}"
            eval "${varname}=\"${value}\""
        ;;
    esac
    shift
done

if test -n "${WORKDIR}" ; then
    install_engine
    exit
fi

if test -n "${COMMANDS}" ; then
    (
    cd any
    ./install.sh COMMANDS="${COMMANDS}" "${@}"
    )
    exit
fi

${MKDIR} \
    "${DESTDIR}${DOCDIR}/" \
    "${DESTDIR}${MANDIR}/"

# all separate tools are already here with needed dir structure
${COPY} rrr/* "${DESTDIR}/"

# the core engine, reinstall, as it can be changed
(
cd any
./install.sh "${@}"
)

# integrated docs
if test -d doc/html/ ; then
    ${COPY} doc/html/ "${DESTDIR}${DOCDIR}/"
fi
if test -d doc/man/ ; then
    ${COPY} \
        doc/man/man1/ \
        doc/man/man3/ \
        doc/man/man7/ \
        "${DESTDIR}${MANDIR}/"
fi
