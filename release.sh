#
# script to make release archive from all subprojects.
#

# devdoc utility is needed

# to create integral release from dev sources,
# put sources of all subprojects into BASEDIR like this:
#
# git clone ${REPO}/any ${BASEDIR}/any
# git clone ${REPO}/subcom ${BASEDIR}/subcom
# git clone ${REPO}/anymods ${BASEDIR}/mods
# git clone ${REPO}/anyutils ${BASEDIR}/utils
# git clone ${REPO}/devdoc ${BASEDIR}/devdoc
#
# then launch the release.sh:
#
# BASEDIR=/your/path ./release.sh
#
# source release will be in current directory as:
# ./any-${VERSION}/
#
# you may setup as well VERSION and RELEASEDIR (where result is located)
#
# to create installable package from the source release,
# use sample-builds/any.build

VERSION="${VERSION:-0.16.0}"
basedir="${BASEDIR:-$(pwd)}"
releasedir="${RELEASEDIR:-$(pwd)/any-${VERSION}}"

rm -rf ${releasedir}
mkdir -p ${releasedir}
git clone ${basedir}/any ${releasedir}/any
rm -rf ${releasedir}/any/.git
cp -Rf ${releasedir}/any/example.conf.d ${releasedir}/rdd.conf.d
install -m644 ${releasedir}/any/example.rdd.def ${releasedir}/rdd.def
install -m644 ${releasedir}/any/rel-README-any ${releasedir}/README-any
install -m644 ${releasedir}/any/makefile ${releasedir}/makefile
install -m755 ${releasedir}/any/rel-install.sh ${releasedir}/install.sh

(
cd ${releasedir}/any
DESTDIR=${releasedir}/rrr ./install.sh
)
(
cd ${basedir}/subcom
DESTDIR=${releasedir}/rrr ./install.sh
)
(
cd ${basedir}/mods
ADIR=${releasedir} DESTDIR=${releasedir}/rrr ./install.sh
)
(
cd ${basedir}/utils
DESTDIR=${releasedir}/rrr ./install.sh
)
(
cd ${basedir}/devdoc
DESTDIR=${releasedir}/rrr ./install.sh
)

# render documentation

cp -Rf ${basedir}/mods/self/srcdoc ${releasedir}/any/srcdoc/mods
for dir in ${basedir}/mods/* ; do
    if test -d ${dir}/srcdoc ; then
        modname=$(basename ${dir})
        cp -Rf ${dir}/srcdoc ${releasedir}/any/srcdoc/mods/${modname}
    fi
done
rm -rf ${releasedir}/any/srcdoc/mods/self/
cp -Rf ${basedir}/subcom/srcdoc ${releasedir}/any/srcdoc/subcom
cp -Rf ${basedir}/utils/srcdoc ${releasedir}/any/srcdoc/utils
cp -Rf ${basedir}/devdoc/srcdoc ${releasedir}/any/srcdoc/devdoc

mkdir -p ${releasedir}/doc
(
export PATH="${releasedir}/rrr/bin:$PATH"
devdoc -t ${releasedir}/any/srcdoc/html-template -f "html man" ${releasedir}/any/srcdoc
)
mv var/html ${releasedir}/doc/html
mv var/man ${releasedir}/doc/man
