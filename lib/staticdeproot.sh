# keeping root untouched, cleaning only current pack to avoid self-dependencies

pkg_context()
{
    pkg_precontext

    save_trace_and_hide_verbose
    pkg_context_extend_pre

    root_clean_pack ${P}
    root_unregister

    pkg_context_extend
    restore_trace_state

    pkg_postcontext
}
