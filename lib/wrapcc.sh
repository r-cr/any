prepare_wrap()
{
    mkdir -p "${pack_EXEC}"

    table_replace=''
    table_skip=''

    printf "#!/bin/sh

#    The script is generated from source file ${BASEDIR}any/bin/wrapcc.in.

WRAPCC='${PLAIN_CC}'
PREFIX_LIST='${PREFIX_LIST}'
root_TRACKED_DIRS='${root_TRACKED_DIRS}'
ROOT='${ROOT}'
ENGINE_CFLAGS='${ENGINE_CFLAGS}'
ENGINE_LDFLAGS='${ENGINE_LDFLAGS}'
OPT_FLAGS='${OPT_FLAGS}'

TABLE_REPLACE='${table_replace}'
TABLE_SKIP_PREPEND='${table_skip}'

# ------------------------------------------------------
\n" > "${pack_EXEC}/wrapcc"

cat "${BASEDIR}any/bin/wrapcc.in" >> "${pack_EXEC}/wrapcc"
chmod +x "${pack_EXEC}/wrapcc"

    printf "#!/bin/sh

#    The script is generated from source file ${BASEDIR}any/bin/wrapcc.in.

WRAPCC='${PLAIN_CXX}'
PREFIX_LIST='${PREFIX_LIST}'
root_TRACKED_DIRS='${root_TRACKED_DIRS}'
ROOT='${ROOT}'
ENGINE_CFLAGS='${ENGINE_CXXFLAGS}'
ENGINE_LDFLAGS='${ENGINE_LDFLAGS}'
OPT_FLAGS='${OPT_FLAGS}'

TABLE_REPLACE='${table_replace}'
TABLE_SKIP_PREPEND='${table_skip}'

# ------------------------------------------------------
\n" > "${pack_EXEC}/wrapcxx"

cat "${BASEDIR}any/bin/wrapcc.in" >> "${pack_EXEC}/wrapcxx"
chmod +x "${pack_EXEC}/wrapcxx"
}
