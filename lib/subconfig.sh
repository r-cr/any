

# ----------- engine methods ------------------

subconfig_cache_create()
{
    if [ ! -d ${SUBCONFIG_DIR}/ ] ; then
        return 0
    fi
    if ause 'native' ; then
        return 0
    fi

    cache_file="${CONFIGCACHE}"
    mkdir -p $(dirname ${CONFIGCACHE})
    : > ${cache_file}
    for word in ${AUSE} ; do
        if [ -f ${SUBCONFIG_DIR}/${word}.config.cache ] ; then
            cat ${SUBCONFIG_DIR}/${word}.config.cache >> ${cache_file}
        fi
    done
    
    if test ! -s "${cache_file}" ; then
        rm -f "${cache_file}"
    fi
}

subconfig_cache_install()
{
    if [ -f "${CONFIGCACHE}" ] ; then
        find ${S} -name "configure" | while read linefile ; do
            dirconf=$(dirname ${linefile})
            cp -fp ${CONFIGCACHE} ${dirconf}/
        done
    fi
}

subconfig_sub_install()
{
    guess_file=
    sub_file=
    if [ -f "${SUBCONFIG_DIR}/config.guess" ] ; then
        guess_file="${SUBCONFIG_DIR}/config.guess"
    fi
    if [ -f "${SUBCONFIG_DIR}/config.sub" ] ; then
        sub_file="${SUBCONFIG_DIR}/config.sub"
    fi

    if [ -n "${guess_file}" -o -n "${sub_file}" ] ; then
        find ${S} -name "config.sub" -o -name "config.guess" | while read linefile ; do
            if [ "$(basename ${linefile})" = 'config.guess' ] ; then
                if [ -n "${guess_file}" ] ; then
                    cp -pf ${guess_file} ${linefile}
                    chmod 755 ${linefile}
                fi
            else
                if [ -n "${sub_file}" ] ; then
                    cp -pf ${sub_file} ${linefile}
                    chmod 755 ${linefile}
                fi
            fi
        done
    fi
}

# ----------- map interfaces ------------------

patches_auto()
{
    subconfig_cache_create
    subconfig_cache_install

    subconfig_sub_install
}

