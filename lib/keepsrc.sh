# when one needs to store previous sources and not reunpack them
# for example, during debug and performing hacks right inside the build directory

# do not refetch, sources are in place
# just go to the battle place
src_fetch()
{
    tobuild
}

# do not apply patches again, they are done already
src_prepare()
{
    return
}
