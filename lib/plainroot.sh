#
# turn off the control over build context
#

# leave alone the root directory,
# do not check build depends
pkg_context()
{
    return
}

# do not add anything to root after the build
pkg_root()
{
    return
}
