sane_module_store()
{
    is_text_good "$1" || return $?
    module_name="$1"

    if [ -d ${T}/${module_name} ] ; then
        rm -rf ${MINTDIR}/${module_name}/
        mkdir -p ${MINTDIR}/
        cp -pRf ${T}/${module_name}/ ${MINTDIR}/
        alog "The contents is saved into ${MINTDIR}/${module_name}/"
    else
        awarn "Mint results in ${T}/${module_name} were not found"
    fi

}

sane_module_compare()
{
    is_text_good "$1" || return $?
    module_name="$1"

    result_dir=
    if [ -d "${MINTDIR}/${module_name}/" ] ; then
        result_dir="${MINTDIR}/${module_name}"
    elif [ -d "${MINTNEUTRALDIR}/${module_name}/" ] ; then
        result_dir="${MINTNEUTRALDIR}/${module_name}"
    else
        skiplater "
There is no etalon content to compare with in directories:
${MINTDIR}/${module_name}/
${MINTNEUTRALDIR}/${module_name}/
You can install the current content of the package as the etalon one
with the call of ${module_name}_store method.
Current content for the module can be viewed inside ${T}/${module_name}/
"
        return 0
    fi

    diff -r ${result_dir}/ ${T}/${module_name}/
}

sane_available_checkers()
{
    for word in ${AUSE} ; do
        meth_to_check="${word}_checker_map"
        meth_code='0'
        meth_output="$(type ${meth_to_check} 2> /dev/null)" || meth_code='1'
        if [ ${meth_code} = '0' ] && [ -n "${meth_output}" ] ; then
            echo ${meth_to_check}
        fi
    done
}

sane_map()
{
    for checker in $(sane_available_checkers) ; do
        ${checker} || nonfail
    done
}
