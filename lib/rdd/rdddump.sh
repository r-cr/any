#!/bin/sh

RDD_MAP_LIBS="rdd_map_libs"
DUMP="dump.sh"

rdddump_path()
{
    dataset="$1"
    root="$2"
    entry="$3"
    if test -z "${root}" ; then
        root="$(rdd_root 2>/dev/null)"
    fi

    dataentries="$(rdddata_value "${dataset}" "${RDD_DATAWORD_ENTRY}" )"
    dataentries="$( rdd_split_words "${dataentries}" )"

    final_dir="${entry}"
    for word in ${dataentries} ; do
        final_dir="${final_dir}_${word}"
    done
    final_dir="${final_dir%_}"

    if test "${root}" = '/' ; then
        root=
    fi
    path="${root}/var/dump/${final_dir}"
    printf "%s\n" "${path}"
}

rdddump_log()
{
    dump_path="$1"

    logpath="${dump_path}/log"
    printf "%s\n" "${logpath}"
}

rdddump_split_multiline_words()
{
    string="$1"
    string="${string#*=}"

    local store_ifs="${IFS}"
    IFS=','
    for word in ${string} ; do
        word="${word## }"
        word="${word%% }"
        printf "%s\n" "${word}"
    done
    IFS="${store_ifs}"
}

rdddump_lib_parse()
{
    libentry="$1"
    root="$2"

    rdddump_split_multiline_words "${libentry}" | while read line_input ; do
        flag_inline=
        candidate=
        case "${line_input}" in
            'inline '*)
                flag_inline=1
                line_input="${line_input#inline }"
                line_input="${line_input## }"
            ;;
        esac
        case "${line_input}" in
            'shell '*)
                line_input="${line_input#shell }"
                line_input="${line_input## }"
            ;;
            *' '*)
                rdd_error "Only shell is supported for libraries: ${RDD_MAP_LIBS} = ${libentry}"
                return 2
            ;;
        esac
        case "${line_input}" in
            '/'*)
                # path is absolute, nothing more
                candidate="${line_input}"
            ;;
            *)
                candidate="${root}/${line_input}"
            ;;
        esac
        if test ! -f "${candidate}" ; then
            rdd_error "Incorrect library is given inside ${RDD_MAP_LIBS}: ${candidate} does not exist"
            return 3
        fi

        if test -n "${flag_inline}" ; then
            cat "${candidate}"
        else
            printf ". '%s'\n" "${candidate}"
        fi
    done
}

rdd_dump()
{
    method_MODE="dump"
    rdd_main "${@}"
    unset method_MODE
}

rdd_drop()
{
    method_MODE="drop"
    rdd_main "${@}"
    unset method_MODE
}
