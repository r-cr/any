#!/bin/sh

rddlist_validated_path()
{
    candidate="$1"
    base="$2"
    if test -z "${candidate}" ; then
        return
    fi

    case "${candidate}" in
        /*)
            # path is absolute, starts with /
            if test -d "${candidate}" ; then
                printf "%s\n" "${candidate}"
                return
            else
                rdd_error "incorrect path for lists inside rdd_list_path: ${candidate}"
                return 1
            fi
        ;;
    esac
    if test -d "${base}/${candidate}" ; then
        validated="$(cd ${base}/${candidate} ; pwd -P)"
        printf "%s\n" "${validated}"
    else
        rdd_error "incorrect path for lists inside rdd_list_path: ${candidate}"
        return 1
    fi
}

rddlist_get_file_object()
{
    listobject="$1"
    # must be non-empty
    listpath="$2"

    case "${listobject}" in
        /*)
            # path is absolute, starts with /
            if test -f "${listobject}" ; then
                printf "%s\n" "${listobject}"
                return
            else
                rdd_error "list with incorrect absolute path: ${listobject}"
                return 1
            fi
        ;;
    esac
    if test -f "${listobject}" ; then
        base_dir_list="${listobject%/*}"
        if test "${base_dir_list}" = "${listobject}" ; then
            base_dir_list='.'
        fi
        expanded_dir_list="$(cd ${base_dir_list} ; pwd -P)"
        file_name_list="${listobject##*/}"
        printf "%s\n" "${expanded_dir_list}/${file_name_list}"
        return
    fi
    if test -f "${listpath}/${listobject}" ; then
        printf "%s\n" "${listpath}/${listobject}"
        return
    fi
}

rddlist_read_file()
{
    input="$1"

    sed -e '/^#/d' -e '/^$/d' < "${input}"

# reading like this can be buggy
# at least in bash-5.0
#    while read line_input ; do
#        case "${line_input}" in
#            '')
#            ;;
#            '#'*)
#            ;;
#            *)
#                printf "%s\n" "${line_input}"
#            ;;
#        esac
#    done < "${input}"
}

rddlist_parse_object()
{
    listobject="$1"
    # must be non-empty
    listpath="$2"

    toparse="${listobject}"
    while test -n "${toparse}" ; do
        word_to_parse="${toparse%%
*}"
        # remove current line from outer array
        check_toparse="${toparse#*
}"
        if test "${check_toparse}" = "${toparse}" ; then
            # last line
            toparse=
        else
            toparse="${check_toparse}"
        fi
        # in case line inside the list will contain multiple words
        word_to_parse="${word_to_parse%% *}"

        filelist="$(rddlist_get_file_object "${word_to_parse}" "${listpath}" )"
        if test -n "${filelist}" ; then
            # freshly read object is file actually, add its content to array
            toparse="$(rddlist_read_file "${filelist}" )
${toparse}"
        else
            printf "%s\n" "${word_to_parse}" 2> /dev/null
        fi
    done
}

rddlist_filter()
{
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi

    while read line_input <&0 ; do
        case "${line_input}" in
            '['*)
                printf "%s\n" "${line_input}"
            ;;
            "rdd_list_entry"*)
                printf "%s\n" "${line_input}"
            ;;
            "rdd_list_path"*)
                printf "%s\n" "${line_input}"
            ;;
        esac
    done
}

rdd_list()
{
    # copy-paste from rdd_data
    # allows not to load entire dataset of rdd_data, but minimalistic subset for listing
    data_words="$( rdd_ause "${@}" )"
    root_dir="$(rdd_root 2>/dev/null)"
    # inner filter implementation
    # perf results are worse then with outer grep
    #    filter_start="rddlist_filter"
    filter_start="grep -e \[.*\] -e ${RDD_LIST_ENTRY} -e ${RDD_LIST_PATH}"

    config_start="$( rdddata_general_config_array "${root_dir}" | ${filter_start} )"
    def_start="$( rdddata_def_config_array "${root_dir}" | ${filter_start} )"
    cmd_start="$( rdd_cmd "${@}" | ${filter_start} )"
    # fetch the needed vars
    start_dict="$(
    (
    for word in ${data_words} ; do
        printf "%s\n%s\n" "${config_start}" "${def_start}" | rdddata_filter_by_dataword "${word}"
    done

    printf "%s\n" "${cmd_start}"
    ) | rdddata_fetch_start_dict | rdddata_prepare_generated
    )"
    list_entry="$( rdddata_value "${start_dict}" ${RDD_LIST_ENTRY} )"
    list_path="$( rdddata_value "${start_dict}" ${RDD_LIST_PATH} )"

    rddlist_print_objects "${list_entry}" "${list_path}" "${root_dir}"
}

rddlist_firstentry()
{
    dataset="$1"

    list_value="$( rdddata_value "${dataset}" "rdd_list_entry" )"
    objects="$( rdd_split_words "${list_value}" )"
    firstentry="${objects%% *}"

    printf "%s\n" "${firstentry}"
}

rddlist_print_objects()
{
    list_entry="${1}"
    list_config_path="${2}"
    root="${3}"

    objects_array="$( rdd_split_words "${list_entry}" )"
    listpath="$( rddlist_validated_path "${list_config_path}" "${root}" )"
    if test -z "${listpath}" ; then
        # empty rdd_list_path is replaced with rdd_root
        # that allows to set up lists with relative paths from command line or inside lists:
        # ports/list/mine.src
        listpath="${root}"
    fi
    for object in ${objects_array} ; do
        rddlist_parse_object "${object}" "${listpath}"
    done
}
