#!/bin/sh

RDD_DATAWORD_ALL='rdd_prf_all'

RDD_LOG_NUM_TERM='rdd_log_num_terminal'
RDD_DEF_LOG_NUM_TERM='3'
RDD_LOG_NUM_LOW='3'
RDD_LOG_NUM_HIGH='9'
RDD_ERR_REDIRECT='rdd_tune_stderr_redirect'
RDD_OUT_REDIRECT='rdd_tune_stdout_redirect'

RDD_ATOM_ID='rdd_atom_id'
RDD_ATOM_DUMPDIR='rdd_atom_dumpdir'
RDD_ATOM_PATH='rdd_atom_path'
RDD_LOG_STDOUT='rdd_log_stdout'
ATOM_CONFIG='atom.conf'
RDD_LIST_ENTRY='rdd_list_entry'
RDD_LIST_PATH='rdd_list_path'

RDD_ATOM_FALLBACK='repo'

rdddata_input_configs()
{
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi
    sed \
        -e '# remove empty and commented strings' \
        -e '/^[[:space:]]*#/d' \
        -e '/^[[:space:]]*$/d' \
        <& 0
}

rdddata_general_config_array()
{
    base_dir="${1}"

    dirconfig="$(rdd_configdir "${base_dir}" "${RDD_BASEDIR}" )"

    if test -z "${dirconfig}" ; then
        return 1
    fi
    if test -z "$( echo "{dirconfig}/"*.conf )" ; then
        return 1
    fi
    cat "${dirconfig}/"*.conf | rdddata_input_configs
}

rdddata_def_config_array()
{
    base_dir="${1}"

    if test ! -f "${base_dir}/rdd.def" ; then
        return 1
    fi

    cat "${base_dir}/rdd.def" | rdddata_input_configs
}

rdddata_single_config_array()
{
    config="${1}"

    if test ! -f "${config}" ; then
        return 1
    fi

    cat "${config}" | rdddata_input_configs
}

rdddata_filter_by_dataword()
{
    dataword="$1"

    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi

    flag_right_section=
    while read -r line_input <&0 ; do
        case "${line_input}" in
            "[${dataword}]")
                flag_right_section=1
            ;;
            '['*)
                flag_right_section=
            ;;
            *)
                if test -n "${flag_right_section}" ; then
                    printf "%s\n" "${line_input}"
                fi
            ;;
        esac
    done
}

rdddata_prepare_for_envload()
{
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi
    sed \
        -e '# filtering non-data variables, specific to rdd' \
        -e "/^${RDD_DATAWORD}[[:space:]]*=/d" \
        -e "/^${RDD_ATOM_ID}[[:space:]]*=/d" \
        -e "/^${RDD_ATOM_DUMPDIR}[[:space:]]*=/d" \
        -e "/^${RDD_LOG_STDOUT}[[:space:]]*=/d" \
        \
        -e '# normalising the entries' \
        -e "s/[[:space:]]*=[[:space:]]*/=/" \
        <& 0
}
rdddata_prepare_generated()
{
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi
    sed \
        -e '# normalising the entries' \
        -e "s/[[:space:]]*=[[:space:]]*/=/" \
        <& 0
}

rdddata_prepare_shell()
{
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi
    # put values of variables inside double quotes "",
    # so the result is usable for shell
    sed \
        -e 's/=/="/' \
        -e 's/$/"/' \
        <& 0
}

rdddata_detect_atom_config()
{
    atom_path="$1"
    atom_id="$2"

    config_candidate="${atom_path}/${atom_id}/${ATOM_CONFIG}"
    found_config=
    if test -f "${config_candidate}" ; then
        found_config=1
    else
        postfix="${atom_id#[a-zA-Z0-9]}"
        atom_hash="${atom_id%${postfix}}"
        config_candidate="${atom_path}/${atom_hash}/${atom_id}/${ATOM_CONFIG}"
        if test -f "${config_candidate}" ; then
            found_config=1
        fi
    fi
    if test -n "${found_config}" ; then
        printf "%s\n" "${config_candidate}"
    fi
}

rdddata_fetch_start_dict()
{
    key="$1"
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi

    list_entry=
    list_path=
    atom_path=
    while read line_input <&0 ; do
        case "${line_input}" in
            "${RDD_LIST_ENTRY}"*)
                list_entry="${line_input}"
            ;;
            "${RDD_LIST_PATH}"*)
                list_path="${line_input}"
            ;;
            "${RDD_ATOM_PATH}"*)
                atom_path="${line_input}"
            ;;
        esac
    done

    printf "%s\n" "${list_entry}"
    printf "%s\n" "${list_path}"
    printf "%s\n" "${atom_path}"
}

rdddata_reverse_string_order()
{
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi

    result=
    while read -r line_input <&0 ; do
        result="${line_input}
${result}"
    done
    printf "%s\n" "${result}"
}

rdddata_glue_sorted_dict()
{
    var_dict="$1"

    checker_dict="$(printf "%s\n" "${var_dict}" | rdddata_reverse_string_order)"
    printf "%s\n" "${var_dict}" | while read -r line_input ; do
        key="${line_input%%=*}"
        # here we need to find actual value for each key
        # resulting value will be last in $var_dict or first in $checker_dict,
        # created specially for the search
        printf "%s\n" "${checker_dict}" | while read -r checker_line ; do
            case "${checker_line}" in
                "${key}="*)
                    # the result found
                    printf "%s\n" "${checker_line}"
                    break
                ;;
            esac
        done
    done | sort | uniq
}

rdddata_isgroup()
{
    mode="$1"

    case "${mode}" in
        data|dump|drop)
            # these modes do not use group, no output
            return
        ;;
        call|rdd)
            # the modes are needed in group
            printf "%s\n" "1"
        ;;
    esac
}

rdddata_option_num_terminal()
{
    control_dataset="$1"
    for_atom="$2"

    # fallback value
    final_num_term="${RDD_DEF_LOG_NUM_TERM}"
    flag_need_print=1
    num_term="$(rdddata_value "${control_dataset}" "${RDD_LOG_NUM_TERM}")"
    if test -n "${for_atom}" -a -z "${num_term}" ; then
        flag_need_print=
    fi

    # check if num_term is correct number at all
    # not true on empty num_term as well
    if test "${num_term}" -eq "${num_term}" 2> /dev/null ; then
        # check on correctness of given number
        if test "${num_term}" -ge ${RDD_LOG_NUM_LOW} -a "${num_term}" -le ${RDD_LOG_NUM_HIGH} ; then
            final_num_term="${num_term}"
            flag_need_print=
        fi
    fi

    if test -n "${flag_need_print}" ; then
        printf "%s\n" "${RDD_LOG_NUM_TERM}=${final_num_term}"
    fi
}

rdddata_option_log()
{
    control_dataset="$1"
    dumpdir="$2"

    out_redirect="$(rdddata_value "${control_dataset}" "${RDD_OUT_REDIRECT}")"
    err_redirect="$(rdddata_value "${control_dataset}" "${RDD_ERR_REDIRECT}")"

    if test "${out_redirect}" = '1' -o "${err_redirect}" = '1' ; then
        logfile="$( rdddump_log "${dumpdir}" )"
        printf "%s\n" "${RDD_LOG_STDOUT}=${logfile}"
    fi
}

rdd_main()
{
    # method_MODE is outer parameter, what to do here:
    #   data: just output plain data
    #   dump: output shell code
    #   drop: drop library shell code
    #   call: drop executable shell code
    #   rdd:  launch the tasks

    data_words="$( rdd_ause "${@}" )"
    root_dir="$(rdd_root 2>/dev/null)"
    config_array="$( rdddata_general_config_array "${root_dir}" )"
    def_array="$( rdddata_def_config_array "${root_dir}" )"
    cmd_array="$( rdd_cmd "${@}" )"
    personal_array=

    # first minimalistic parse
    # try to determine:
    #   - first personal object config
    #   - objects to handle as they are in lists
    # 
    # the tricky part is: the path to config with personal data is stored in other configs
    # and it should be determined correctly, as entries from different configs may redefine each other
    # so there is a need in an additional parse in some way
        # that is lightweight non-sorted data with needed path
    filter_start="grep -e \[.*\] -e ${RDD_ATOM_PATH} -e ${RDD_LIST_ENTRY} -e ${RDD_LIST_PATH}"
    config_start="$( printf "%s\n" "${config_array}" | ${filter_start} )"
    def_start="$( printf "%s\n" "${def_array}" | ${filter_start} )"
    cmd_start="$( printf "%s\n" "${cmd_array}" | ${filter_start} )"
    # fetch the needed path
    start_dict="$(
    (
    for word in ${data_words} ; do
        printf "%s\n%s\n" "${config_start}" "${def_start}" | rdddata_filter_by_dataword "${word}"
    done

    printf "%s\n" "${cmd_start}"
    ) | rdddata_fetch_start_dict | rdddata_prepare_generated
    )"
    list_entry="$( rdddata_value "${start_dict}" ${RDD_LIST_ENTRY} )"
    list_path="$( rdddata_value "${start_dict}" ${RDD_LIST_PATH} )"
    atom_inner_path="$( rdddata_value "${start_dict}" ${RDD_ATOM_PATH} )"
    if test -n "${atom_inner_path}" ; then
        case "${atom_inner_path}" in
            /*)
            # path is absolute, starts with /
                normalised_path="${atom_inner_path}"
            ;;
            *)
                prefix_atom_path="${root_dir%/}"
                normalised_path="${prefix_atom_path}/${atom_inner_path}"
            ;;
        esac
        atom_path="$(cd "${normalised_path}" ; pwd -P)"
    else
        atom_path="${root_dir}"
    fi
    is_group="$(rdddata_isgroup "${method_MODE}" )"
    if test -n "${is_group}" ; then
        # initial dataset will be without personalised objects.
        # The name of first object is just first entry from command line
        atom_id="$( rdd_firstentry "${list_entry}" )"
    else
        # no groups are used. Read first object from general list
        atom_id="$(
        rddlist_print_objects "${list_entry}" "${list_path}" "${root_dir}" | ( read line <& 0 ; printf "%s\n" "${line}" )
        )"

        # so now we detect, whether personal config is present
        atom_config="$( rdddata_detect_atom_config "${atom_path}" "${atom_id}" )"
        # if present, there is one additional array of data
        if test -n "${atom_config}" ; then
            personal_array="$( rdddata_single_config_array "${atom_config}" )"
        fi
    fi
    if test -z "${atom_id}" ; then
        atom_id="${RDD_ATOM_FALLBACK}"
    fi

    # sort data sections by datawords
    var_dict="$(
    (
    for word in ${data_words} ; do
        printf "%s\n%s\n%s\n" "${config_array}" "${personal_array}" "${def_array}" | rdddata_filter_by_dataword "${word}"
    done

    printf "%s\n" "${cmd_array}"

    ) | rdddata_prepare_for_envload
    )"
    var_dict="${var_dict}
    $(rdddata_option_num_terminal "${var_dict}")"

    raw_dataset="$(
    rdddata_glue_sorted_dict "${var_dict}"
    )"

    control_dataset="$( printf "%s\n" "${raw_dataset}" | rdddata_control_data )"
    dumpdir="$( rdddump_path "${control_dataset}" "${root_dir}" "${atom_id}" )"

    # here we have data from rdd_ variables
    # now some logic is available for generated variables
    
    dataset="$(
    (
    printf "%s\n" "${raw_dataset}"

    # generated variables for final dataset

    printf "%s\n" "${RDD_DATAWORD_ALL}=${data_words}"
    printf "%s\n" "${RDD_ATOM_ID}=${atom_id}"
    printf "%s\n" "${RDD_ATOM_DUMPDIR}=${dumpdir}"
    rdddata_option_log "${control_dataset}" "${dumpdir}"
    ) | sort
    )"

    if test "${method_MODE}" = 'data' ; then
        printf "%s\n" "${dataset}" 2> /dev/null
        return
    fi
    control_dataset="$( printf "%s\n" "${dataset}" | rdddata_control_data )"

    if test "${method_MODE}" = 'dump' ; then
        libentry="$(rdddata_value "${control_dataset}" "${RDD_MAP_LIBS}" )"
        rddcall_libdump "${dataset}" "${root_dir}" "${libentry}"
        return
    fi

    if test "${method_MODE}" = 'drop' ; then
        libentry="$(rdddata_value "${control_dataset}" "${RDD_MAP_LIBS}" )"
        mkdir -p "${dumpdir}"
        dumpfile="${dumpdir}/${DUMP}"

        rddcall_libdump "${dataset}" "${root_dir}" "${libentry}" > "${dumpfile}"

        if test -f "${dumpfile}" ; then
            printf "%s\n" "${dumpfile}"
        fi
        return
    fi

    # only grouped work goes further
    if test -z "${is_group}" ; then
        return
    fi
    # grouped execution
    script_group="$( rddcall_alldump "${dataset}" "${control_dataset}" "${dumpdir}" "${root_dir}" "${is_group}" )"
    rdddata_handle_script "${script_group}" "${method_MODE}" "silent"

    # cyclic handling of each atom in list
    #FIXME: reading with while-cycle does not work in a strange manner:
    #       only first string is read. But! if we set up empty map: rdd_map_entry=true, reading begins to work.
    #       all utilies, besides main rdd (callrdd, lsrdd) worked fine.
#    printf "%s\n" "${objects}" | while read atom_id ; do

    for atom_id in $( rddlist_print_objects "${list_entry}" "${list_path}" "${root_dir}" ) ; do

            # -------------------------------------------------------
            # common part with retrieving of essential dataset, upper

        # so now we detect, whether personal config is present
        atom_config="$( rdddata_detect_atom_config "${atom_path}" "${atom_id}" )"
        # if present, there is one additional array of data
        if test -n "${atom_config}" ; then
            personal_array="$( rdddata_single_config_array "${atom_config}" )"

            # fetch data with higher priority and apply it on top of generic dataset
            atom_var_dict="$(
            (
            for word in ${data_words} ; do
                printf "%s\n%s\n" "${personal_array}" "${def_array}" | rdddata_filter_by_dataword "${word}"
            done

            printf "%s\n" "${cmd_array}"

            ) | rdddata_prepare_for_envload
            )"
            atom_sum_dict="${raw_dataset}
${atom_var_dict}
$( rdddata_option_num_terminal "${atom_var_dict}" "atom" )"

            atom_raw_dataset="$(
            rdddata_glue_sorted_dict "${atom_sum_dict}"
            )"
            atom_control_dataset="$( printf "%s\n" "${atom_raw_dataset}" | rdddata_control_data )"
        else
            # if there is no personalised data, just copy the generic data
            atom_raw_dataset="${raw_dataset}"
            atom_control_dataset="$( printf "%s\n" "${raw_dataset}" | rdddata_control_data )"
        fi

        # we need to recreate dataset for each atom with such changes, as its name, log path
        atom_dumpdir="$( rdddump_path "${atom_control_dataset}" "${root_dir}" "${atom_id}" )"

        # here we have data from rdd_ variables
        # now some logic is available for generated variables

        atom_dataset="$(
        (
        printf "%s\n" "${atom_raw_dataset}"

        # generated variables for final dataset

        printf "%s\n" "${RDD_DATAWORD_ALL}=${data_words}"
        printf "%s\n" "${RDD_ATOM_ID}=${atom_id}"
        printf "%s\n" "${RDD_ATOM_DUMPDIR}=${atom_dumpdir}"
        rdddata_option_log "${atom_control_dataset}" "${atom_dumpdir}"
        ) | sort
        )"
        atom_control_dataset="$( printf "%s\n" "${atom_dataset}" | rdddata_control_data )"

        # end of common data part
        # -------------------------------------------------------

        script_atom="$( rddcall_alldump "${atom_dataset}" "${atom_control_dataset}" "${atom_dumpdir}" "${root_dir}" )"
        rdddata_handle_script "${script_atom}" "${method_MODE}"
    done
}

rdd_data()
{
    method_MODE="data"
    rdd_main "${@}"
    unset method_MODE
}

# get value of var from given dataset
rdddata_value()
{
    dataset="$1"
    var="$2"

    value="${dataset##*
${var}=}"
    if test "${value}" != "${dataset}" ; then
        value="${value%%
*}"
        printf "%s\n" "${value}"
    else
        value="${dataset##${var}=}"
        if test "${value}" != "${dataset}" ; then
            value="${value%%
*}"
            printf "%s\n" "${value}"
        fi
    fi
}

rdddata_control_data()
{
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi

    while read -r line_input <&0 ; do
        case "${line_input}" in
            "rdd_"*)
                printf "%s\n" "${line_input}"
            ;;
        esac
    done
}

rdddata_handle_script()
{
    script="$1"
    mode="$2"
    silence="$3"

    if test "${mode}" = 'rdd' ; then
        # actually exec in main mode
        "${script}"
    elif test "${mode}" = 'call' ; then
        if test -z "${silence}" ; then
            printf "%s\n" "${script}"
        fi
    fi
}

rdd_serial()
{
    method_MODE="rdd"
    rdd_main "${@}"
    unset method_MODE
}
