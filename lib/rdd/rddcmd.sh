#!/bin/sh

RDD_DATAWORD_ENTRY='rdd_prf_entry'
RDD_LIST_ENTRY='rdd_list_entry'
RDD_MAP_ENTRY='rdd_map_entry'

rdd_cmd()
{
    # clean up
    rddcmd_DATA=
    rddcmd_OBJECT=
    rddcmd_METHOD=

    count=0
    # handling all command line args
    cmd_array="$(
    for arg ; do
        case "${arg}" in
            *=*)
                printf "%s\n" "${arg}"
                continue
            ;;
        esac
        case ${count} in
            0)
                rddcmd_DATA="${arg}"
                count=$((count + 1))
            ;;
            1)
                rddcmd_OBJECT="${arg}"
                count=$((count + 1))
            ;;
            2)
                rddcmd_METHOD="${arg}"
                count=$((count + 1))
            ;;
        esac
    done
    )"
    if test -n "${rddcmd_DATA}" ; then
        printf "%s\n" "${RDD_DATAWORD_ENTRY}=${rddcmd_DATA}"
    fi
    if test -n "${rddcmd_OBJECT}" ; then
        printf "%s\n" "${RDD_LIST_ENTRY}=${rddcmd_OBJECT}"
    fi
    if test -n "${rddcmd_METHOD}" ; then
        printf "%s\n" "${RDD_MAP_ENTRY}=${rddcmd_METHOD}"
    fi
    if test -n "${cmd_array}" ; then
        printf "%s\n" "${cmd_array}"
    fi
}
