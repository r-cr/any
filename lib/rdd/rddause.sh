#!/bin/sh

RDD_DEFAULT_DATAWORD='default'

rddause_create_data_array()
{
    if test ! -t 0 ; then
        current_key=
        current_value=

        while read line_input <&0 ; do
            case "${line_input}" in
            '['*)
                if test -n "${current_key}" -a -n "${current_value}" ; then
                    printf "%s\n" "${current_key} $(rdd_split_words "${current_value}")"
                fi
                current_key="${line_input}"
                current_value=
                ;;
            *)
                current_value="${line_input}"
                ;;
            esac
        done
        if test -n "${current_key}" -a -n "${current_value}" ; then
            printf "%s\n" "${current_key} $(rdd_split_words "${current_value}")"
        fi
    else
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi
}

rddause_extract_value_from_array()
{
    key="$1"
    dictionary="$2"

    value="${dictionary##*\[${key}\] }"
    if test "${value}" != "${dictionary}" ; then
        value="${value%%
*}"

        printf "%s\n" "${value}"
    fi

}

rddause_datawords_from_array()
{
    entry="$1"
    ause_dictionary="$2"

    unread=" ${entry}"
    loaded_ause=

    while test -n "${unread}" ; do
        current_word="${unread##* }"
        unread="${unread% *}"
        case " ${loaded_ause} " in
        *" ${current_word} "*)
            continue
            ;;
        *)
            case " ${loaded_ause} " in
                *" ${current_word} "*)
                    # add only fresh datawords, which are absent in input string
                ;;
                *)
                    loaded_ause="${current_word} ${loaded_ause}"
                ;;
            esac
            inner_words="$(rddause_extract_value_from_array ${current_word} "${ause_dictionary}")"
            if test -n "${inner_words}" ; then
                unread="${unread} ${inner_words}"
            fi
            ;;
        esac
    done

    loaded_ause="${loaded_ause%% }"
    printf "%s\n" "${loaded_ause}"
}

rdd_ause()
{
    root_dir="$(rdd_root 2>/dev/null)"
    cmd_config="$(rdd_cmd "${@}" )"
    ause_dictionary=
    ause_words="$(printf "%s\n" "${cmd_config}" | rdd_merge_datawords)"
    if test -z "${ause_words}" ; then
        ause_words=${RDD_DEFAULT_DATAWORD}
    fi

    if test -f "${root_dir}/rdd.def" ; then
        def_config="${root_dir}/rdd.def"
    fi
    gen_configdir="$(rdd_configdir "${root_dir}" "${RDD_BASEDIR}" )"

    if test -n "${def_config}" -o -n "${gen_configdir}" ; then
        if test -z "${def_config}" ; then
            # to avoid grep errors on empty file name
            def_config='/dev/null'
        fi
        ause_dictionary="$( \
            \
            POSIXLY_CORRECT=1 \
            grep -h \
            -e "^\[[a-zA-Z0-9_]*\]" \
            -e "^[[:space:]]*rdd_prf_id[[:space:]]*=[[:space:]]*" \
            "${def_config}" \
            "${gen_configdir}"/*.conf \
            \
            | rddause_create_data_array
                   )"
        conf_words="$( rddause_datawords_from_array "${ause_words}" "${ause_dictionary}" )"
        if test -n "${conf_words}" ; then
            ause_words="${conf_words}"
        fi
    fi

    if test -n "${ause_words}" ; then
        printf "%s\n" "${ause_words}"
    fi
    
}
