#!/bin/sh

CALLDUMP="calldump"
GROUPDUMP="groupdump"

RDD_MAP_AUTOPOST="rdd_map_autopost"
RDD_MAP_AUTOPRE="rdd_map_autopre"
RDD_MAP_ENTRY="rdd_map_entry"
RDD_MAP_GROUP="rdd_map_group"
RDD_MAP_POSTMORTEM="rdd_map_postmortem"

DEF_SHELL='sh'
RDD_LANG_SHELL="rdd_lang_shell"

RDD_DEBUG="${RDD_DEBUG:-}"

rddcall_generic_maps()
{
    dataset="$1"

    # premap
    premap="$(rdddata_value "${dataset}" "${RDD_MAP_AUTOPRE}")"
    rdddump_split_multiline_words "${premap}"
    # generic map
    mainmap="$(rdddata_value "${dataset}" "${RDD_MAP_ENTRY}")"
    rdddump_split_multiline_words "${mainmap}"
    # aftermap
    aftermap="$(rdddata_value "${dataset}" "${RDD_MAP_AUTOPOST}")"
    rdddump_split_multiline_words "${aftermap}"
}

rddcall_terminal_maps()
{
    dataset="$1"

    terminalmap="$(rdddata_value "${dataset}" "${RDD_MAP_POSTMORTEM}")"
    rdddump_split_multiline_words "${terminalmap}"
}

rddcall_group_maps()
{
    dataset="$1"

    groupmap="$(rdddata_value "${dataset}" "${RDD_MAP_GROUP}")"
    rdddump_split_multiline_words "${groupmap}"
}

rddcall_which()
{
    arg="$1"

    if test -x "${arg}" ; then
        printf "%s\n" "${arg}"
        return 0
    fi

    IFS=:
    for dir in ${PATH} ; do
        if test -x "${dir}/${arg}" ; then
            printf "%s\n" "${dir}/${arg}"
            return 0
        fi
    done
    return 1
}

rddcall_print_log()
{
    control_dataset="$1"
    dumpdir="$2"

    out_redirect="$(rdddata_value "${control_dataset}" "${RDD_OUT_REDIRECT}")"
    err_redirect="$(rdddata_value "${control_dataset}" "${RDD_ERR_REDIRECT}")"

    if test "${out_redirect}" != '1' -a "${err_redirect}" != '1' ; then
        # no log
        return
    fi
    logfile="$( rdddump_log "${dumpdir}" )"
    num_term="$( rdddata_value "${control_dataset}" "${RDD_LOG_NUM_TERM}" )"

    printf "exec %s<&1
: > %s ; exec 1> %s
exec 2>&1
" "${num_term}" "${logfile}" "${logfile}"

}

rddcall_libdump()
{
    dataset="$1"
    root="$2"
    libentry="$3"

    # insert early debug tracing into shell execution
    if test -n "${RDD_DEBUG}" ; then
        printf "%s\n" "set -x"
    fi
    printf "%s\n" "export RDD_ROOT='${root}'"
    printf "%s\n" "${dataset}" | rdddata_prepare_shell
    rdddump_lib_parse "${libentry}" "${root}"
}

rddcall_execdump()
{
    control_dataset="${1}"
    dumpdir="${2}"
    is_group="${3}"

    if test -n "${is_group}" ; then
        execdump="${dumpdir}/${GROUPDUMP}"
    else
        execdump="${dumpdir}/${CALLDUMP}"
    fi
    dumpfile="${dumpdir}/${DUMP}"
    
    shellfile="$(rdddata_value "${control_dataset}" "${RDD_LANG_SHELL}" )"
    if test -z "${shellfile}" ; then
        shellfile="${DEF_SHELL}"
    fi
    shellfile="$(rddcall_which "${shellfile}" )"

    (
    printf "#!%s\n" "${shellfile}"
    rddcall_print_log "${control_dataset}" "${dumpdir}"

    # main part
    printf "
(
#!%s < /dev/null
. '%s'
" "${shellfile}" "${dumpfile}"

    if test -n "${is_group}" ; then
        rddcall_group_maps "${control_dataset}"
    else
        rddcall_generic_maps "${control_dataset}"
    fi

    printf "
)
"
    # aftermath part
    if test -z "${is_group}" ; then

        printf '
(

rdd_exit_status=$?
export rdd_exit_status
if test "${rdd_exit_status}" != 0 ; then
#!%s < /dev/null' "${shellfile}"
        printf "
. '%s'
" "${dumpfile}"

        rddcall_terminal_maps "${control_dataset}"

        printf "
fi

)
"
    fi

    ) > "${execdump}"

    if test -f "${execdump}" ; then
        chmod +x "${execdump}"
        printf "%s\n" "${execdump}"
    fi

}

rddcall_alldump()
{
    dataset="$1"
    control_dataset="$2"
    dumpdir="$3"
    root="$4"
    group="$5"

# creating dumps

    libentry="$(rdddata_value "${control_dataset}" "${RDD_MAP_LIBS}" )"
    mkdir -p "${dumpdir}"
    dumpfile="${dumpdir}/${DUMP}"

    rddcall_libdump "${dataset}" "${root}" "${libentry}" > "${dumpfile}"

# creating executable dump

    rddcall_execdump "${control_dataset}" "${dumpdir}" "${group}"
}

rdd_call()
{
    method_MODE="call"
    rdd_main "${@}"
    unset method_MODE
}
