#!/bin/sh

RDD_VERSION="2.0"

MSG_NOTHING_FOUND="
Warning: no rdd configs were found (rdd.conf.d/, rdd.def).
Using current directory as the root of working directory.
"

MSG_NO_DIR="Error: no valid current directory."
ERR_NO_DIR='2'

RDD_DATAWORD_ENTRY='rdd_prf_entry'
RDD_LIST_ENTRY='rdd_list_entry'
RDD_MAP_ENTRY='rdd_map_entry'
RDD_DATAWORD='rdd_prf_id'

rdd_root()
{
    current_dir="$(pwd -P)"
    if ! test -d "${current_dir}" ; then
        printf "%s\n" "${MSG_NO_DIR}"
        exit ${ERR_NO_DIR}
    fi
    
    found_dir=
    checking_dir="${current_dir}"
    while test -n "${checking_dir}" ; do
        if test -r "${checking_dir}/rdd.def" -o -d "${checking_dir}/rdd.conf.d/" ; then
            found_dir="${checking_dir}"
            break
        fi
    
        checking_dir="${checking_dir%/*}"
    done
    if test -z "${checking_dir}" ; then
        if test -r "/rdd.def" -o -d "/rdd.conf.d/" ; then
            found_dir="/"
        fi
    fi
    
    if test -n "${found_dir}" ; then
        printf "%s\n" "${found_dir}"
    else
        if test -n "${RDD_INNER_DEBUG}" ; then
            printf "%s\n" "${MSG_NOTHING_FOUND}" 1>&2
        fi
        printf "%s\n" "${current_dir}"
    fi
}

rdd_split_words()
{
    string="$1"
    string="${string#*=}"

    local value=
    local store_ifs="${IFS}"
    IFS=','
    for word in ${string} ; do
        word="${word## }"
        word="${word%% }"
        value="${value} ${word}"
    done
    IFS="${store_ifs}"

    value="${value## }"
    printf "%s\n" "${value}"
}

rdd_cmd()
{
    rddcmd_DATA=
    rddcmd_OBJECT=
    rddcmd_METHOD=

    count=0
    # handling all command line args
    for arg ; do
        case "${arg}" in
            *=*)
                continue
            ;;
        esac
        case ${count} in
            0)
                rddcmd_DATA="${arg}"
                count=$((count + 1))
            ;;
            1)
                rddcmd_OBJECT="${arg}"
                count=$((count + 1))
            ;;
            2)
                rddcmd_METHOD="${arg}"
                count=$((count + 1))
            ;;
        esac
    done
    cmd_array="$(
    for arg ; do
        case "${arg}" in
            *=*)
                printf "%s\n" "${arg}"
                continue
            ;;
        esac
    done
    )"

    if test -n "${rddcmd_DATA}" ; then
        printf "%s\n" "${RDD_DATAWORD_ENTRY}=${rddcmd_DATA}"
    fi
    if test -n "${rddcmd_OBJECT}" ; then
        printf "%s\n" "${RDD_LIST_ENTRY}=${rddcmd_OBJECT}"
    fi
    if test -n "${rddcmd_METHOD}" ; then
        printf "%s\n" "${RDD_MAP_ENTRY}=${rddcmd_METHOD}"
    fi
    if test -n "${cmd_array}" ; then
        printf "%s\n" "${cmd_array}"
    fi
}


rdd_merge_datawords()
{
    if test -t 0 ; then
        printf "%s\n" "Method reads from stdin" 1>&2
        return 1
    fi

    entry_words=
    more_words=
    while read line_input <&0 ; do
        case "${line_input}" in
            "${RDD_DATAWORD_ENTRY}="*)
                entry_words="${line_input}"
            ;;
            "${RDD_DATAWORD}="*)
                more_words="${line_input}"
            ;;
        esac
    done
    entry_words="$(rdd_split_words "${entry_words}" )"
    more_words="$(rdd_split_words "${more_words}" )"

    printf "%s\n" "${entry_words} ${more_words}"
}

rdd_error()
{
    printf "%s\n" "${@}" >&2
}

rdd_firstentry()
{
    value="$1"

    objects="$( rdd_split_words "${value}" )"
    firstentry="${objects%% *}"

    printf "%s\n" "${firstentry}"
}

rdd_configdir()
{
    base_dir="${1}"
    install_dir="${2}"

    dirconfig=
    if test -d "${base_dir}/rdd.conf.d/" ; then
        dirconfig="${base_dir}/rdd.conf.d/"
    else
        # if there are another local configs, we have actual working directory
        # and do not work with global configs
        if test -f "${base_dir}/rdd.def" ; then
            return 1
        fi
        # no working directory. Try to read global config, located somewhere at the root of rdd installation
        remaining_path="${install_dir}"
        tocontinue=1
        while test -n "${tocontinue}" ; do
            if test -d "${remaining_path}/etc/rdd/" ; then
                dirconfig="${remaining_path}/etc/rdd/"
                break
            else
                if test -z "${remaining_path}" ; then
                    tocontinue=
                    break
                fi
                remaining_path="${remaining_path%/*}"
            fi
        done
    fi

    if test -n "${dirconfig}" ; then
        printf "%s\n" "${dirconfig}"
    fi
}
