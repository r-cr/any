# resolving build dependencies

resolve_rebuild()
{
    if test -n "${1}" -a "${1}" = 'complete' ; then
        # request complete rebuild, disregarding available deployed pool
        available_current=
    else
        available_current="$( depend_get_available )"
    fi
    build_available="$( depend_get_available_desired )"
    nonchecked=
    checked=
    checked_resolved=
    needed_to_build=
    non_buildable=
    origin=

    safe_counter='16384'

    if test -t 0 ; then
        die "resolve_rebuild reads from stdin"
    fi
    while read nonchecked <&0 ; do
        resolved_origin="${nonchecked}"

        while test -n "${nonchecked}" -a ${safe_counter} -gt '0' ; do
            safe_counter=$(( ${safe_counter} - 1 ))
            if test -n "${resolved_origin}" ; then
                resolved_single_depend="${resolved_origin}"
                origin="${origin} ${resolved_origin}"
                single_depend=
                resolved_origin=
                nonchecked=

            else
                single_depend="$(get_first_word_from_list "${nonchecked}")"
                nonchecked="$( delete_word_from_list "${nonchecked}" "${single_depend}" )"
                # whether fresh single_depend already has been checked in previous iterations
                if word_in_list "${checked}" "${single_depend}" ; then
                    continue
                fi

                method="$(depend_detect_sortversion_method ${single_depend})"
                name_to_match="$( depend_get_pack_noboundary "${single_depend}" )"

                matching_array="$(printf "%s\n" "${available_current}" | depend_match_name "${name_to_match}" )"
                resolved_single_depend="$( ${method} "${name_to_match}" "${matching_array}" )" || nonfail

                if test -n "${resolved_single_depend}" ; then
                    if ! word_in_list "${checked_resolved}" "${resolved_single_depend}" ; then
                        checked_resolved="${checked_resolved} ${resolved_single_depend}"
                    fi
                    checked="${checked} ${single_depend}"
                    continue
                fi

                matching_array="$(printf "%s\n" "${build_available}" | depend_match_name "${name_to_match}" )"
                resolved_single_depend="$( ${method} "${name_to_match}" "${matching_array}" )" || nonfail
            fi

            if test -n "${resolved_single_depend}" ; then
                if ! word_in_list "${needed_to_build}" "${resolved_single_depend}" ; then
                    needed_to_build="${resolved_single_depend} ${needed_to_build}"
                fi
                new_depends="$(depend_get_single_build ${resolved_single_depend})"
                nonchecked="${new_depends} ${nonchecked}"

                if ! word_in_list "${checked_resolved}" "${resolved_single_depend}" ; then
                    checked_resolved="${checked_resolved} ${resolved_single_depend}"
                fi
                checked="${checked} ${single_depend}"
                continue
            else
                if ! word_in_list "${non_buildable}" "${single_depend}" ; then
                    non_buildable="${non_buildable} ${single_depend}"
                fi
                checked="${checked} ${single_depend}"
            fi

        done
    done

    if test "${safe_counter}" -le '0' ; then
        printf "%s" "--nonresolved"
        die "
Counter of dependency iterations reached maximum. Reduce quantity of packages in the request."
    fi

    if test -n "${non_buildable}" ; then
        printf "%s" "--nonresolved"
        die "
These dependencies are not resolved with current packages:
${non_buildable}
"
    fi

    # we need to detect: are direct outer entries in 'available' base or not?
    # if not, they should be manually added to the end of final output. Otherwise they will not be printed.
    origin_nonavailable=
    for origin_entry in ${origin} ; do
        # firstly, filter out excess checks
        if word_in_list "${checked}" "${origin_entry}" ; then
            continue
        fi
        # if nothing found in base, write that one down
        match="$(printf "%s\n" "${build_available}" | depend_match_name_exact "${origin_entry}" )"
        if test -z "${match}" ; then
            origin_nonavailable="${origin_nonavailable}
${origin_entry}"
        fi
    done
    # remove first empty string
    origin_nonavailable="${origin_nonavailable#
}"

    # print sorted needed packages according to base of build available
    mkdir -p "${T}"
    storage_needed="${T}/resolve_needed"
    printf "%s\n" "${needed_to_build}" | tr ' ' '\n' | sed -e '/^$/d' > "${storage_needed}"
    printf "%s\n" "${build_available}" | grep -f "${storage_needed}"
    # add entries, absent in available base
    if test -n "${origin_nonavailable}" ; then
        printf "%s\n" "${origin_nonavailable}"
    fi
}

resolve_atom()
{
    if test -t 0 ; then
        die "resolve_atom reads from stdin"
    fi
    if test -n "${1}" -a "${1}" = 'complete' ; then
        # request complete list, disregarding available deployed pool
        available_current=
    else
        available_current="$( depend_get_available )"
    fi
    build_available="$( depend_get_available_desired )"
    nonresolved=
    resolved=
    checked=

    while read nonchecked <&0 ; do
        depends_current="$( depend_get_single_build "${nonchecked}" )"

        for single_depend in ${depends_current} ; do
            method="$(depend_detect_sortversion_method ${single_depend})"
            name_to_match="$( depend_get_pack_noboundary "${single_depend}" )"

            matching_array="$(printf "%s\n" "${available_current}" | depend_match_name "${name_to_match}" )"
            resolved_single_depend="$( ${method} "${name_to_match}" "${matching_array}" )"

            # depend is deployed and we do not need it in results
            # in 'complete' mode it should always be empty
            if test -n "${resolved_single_depend}" ; then
                continue
            fi

            matching_array="$(printf "%s\n" "${build_available}" | depend_match_name "${name_to_match}" )"
            resolved_single_depend="$( ${method} "${name_to_match}" "${matching_array}" )"

            if test -z "${resolved_single_depend}" ; then
                nonresolved="${nonresolved} ${single_depend}"
            else
                resolved="${resolved} ${resolved_single_depend}"
            fi
        done
        checked="${checked} ${nonchecked}"
    done

    if test ! -z "${nonresolved}" ; then
        printf "%s\n" "--nonresolved"

        message="\
Build dependencies below were not resolved. You should make needed packages available.
${nonresolved}
"
        awarn "${message}"
        return 1
    fi

    # if entries from outer source had been given explicitly,
    # do not list them among dependencies
    # so we divide packages into nonoverlapping sets:
    # dependencies and content of request itself
    resolved_filtered=
    for entry in ${resolved} ; do
        if ! word_in_list "${checked}" "${entry}" ; then
            resolved_filtered="${resolved_filtered}
${entry}"
        fi
    done
    # remove first empty string
    resolved_filtered="${resolved_filtered#
}"
    if test -z "${resolved_filtered}" ; then
        return
    fi

    # print sorted needed packages according to a base of available ones
    mkdir -p "${T}"
    storage_needed="${T}/resolve_needed"
    printf "%s\n" "${resolved_filtered}" > "${storage_needed}"
    printf "%s\n" "${build_available}" | grep -f "${storage_needed}"
}

resolve_reverse()
{
    if test -t 0 ; then
        die "resolve_reverse reads from stdin"
    fi
    storage_request=

    while read nonchecked <&0 ; do
        storage_request="${storage_request}
${nonchecked}"
    done

    depend_get_available_desired | while read reverse_check ; do
        depends_current="$( depend_get_single_build "${reverse_check}" )"
        for single_depend in ${depends_current} ; do
            method="$(depend_detect_sortversion_method ${single_depend})"
            name_to_match="$( depend_get_pack_noboundary "${single_depend}" )"
            matching_array="$(printf "%s\n" "${storage_request}" | depend_match_name "${name_to_match}" )"

            resolved_single_depend="$( ${method} "${name_to_match}" "${matching_array}" )"

            if test -n "${resolved_single_depend}" ; then
                printf "%s\n" "${reverse_check}"
                break
            fi
        done
    done
}
