#!/bin/sh

INSTALL=${INSTALL:-"install"}
COPY=${COPY:-"cp -Rf"}
MKDIR=${MKDIR:-"mkdir -p"}
DESTDIR=${DESTDIR:-"${HOME}/rrr"}
PREFIX=${PREFIX:-}
P=${P:-"any"}
BINDIR="${BINDIR:-${PREFIX}/bin}"
DOCDIR="${DOCDIR:-${PREFIX}/share/doc/${P}}"
MANDIR="${MANDIR:-${PREFIX}/share/man}"
LIBDIR="${LIBDIR:-${PREFIX}/lib}"
SUBCOMDIR="${PREFIX}/lib/any"
NORDD=

if [ -f conf/config.sh ] ; then
    . conf/config.sh
fi

install_system()
{
    ${MKDIR} \
        "${DESTDIR}${DOCDIR}/" \
        "${DESTDIR}${MANDIR}/" \
        "${DESTDIR}${BINDIR}/"

    ${INSTALL} -m644 'README-any' 'License-ISC' "${DESTDIR}${DOCDIR}/"
    if test -d doc/html/ ; then
        ${COPY} doc/html/ "${DESTDIR}${DOCDIR}/"
    fi
    if test -d doc/man/ ; then
        ${COPY} \
            doc/man/man1/ \
            doc/man/man3/ \
            doc/man/man7/ \
            "${DESTDIR}${MANDIR}/"
    fi
    ${INSTALL} -m555 \
        'bin/anch' \
        'bin/any' \
        'bin/anyinit' \
        'bin/chrootenv' \
        'bin/dumpbuild' \
        'bin/lspkg' \
        'bin/tmpdroprdd' \
        "${DESTDIR}${BINDIR}/"

    if test -z "${NORDD}" ; then
        ${INSTALL} -m555 \
            'bin/auserdd' \
            'bin/callrdd' \
            'bin/datardd' \
            'bin/droprdd' \
            'bin/dumprdd' \
            'bin/lsrdd' \
            'bin/rdd' \
            'bin/rootrdd' \
            "${DESTDIR}${BINDIR}/"

        ${MKDIR} "${DESTDIR}${LIBDIR}/"
        ${COPY} \
            'lib/rdd/' \
            "${DESTDIR}${LIBDIR}/"
    fi

    if test -n "${progchroot_flag}" ; then
        sed -e "s/^PROGCHROOT=.*/PROGCHROOT=\"\${PROGCHROOT:-${progchroot_value}}\"/" \
            "${DESTDIR}${BINDIR}/anch" > "${DESTDIR}${BINDIR}/anch.edit"
        mv -f "${DESTDIR}${BINDIR}/anch.edit" "${DESTDIR}${BINDIR}/anch"
    fi
    if test -n "${progsu_flag}" ; then
        sed -e "s/^PROGSU=.*/PROGSU=\"\${PROGSU:-${progsu_value}}\"/" \
            "${DESTDIR}${BINDIR}/anch" > "${DESTDIR}${BINDIR}/anch.edit"
        mv -f "${DESTDIR}${BINDIR}/anch.edit" "${DESTDIR}${BINDIR}/anch"
    fi
}

install_tools()
{
    for tool in toolsrc/* ; do
        if test -d $tool ; then
        (
            cd $tool
            ./install.sh "$@"
        )
        fi
    done
}

install_commands()
{
    mkdir -p ${DESTDIR}${SUBCOMDIR}
    commands=
    for one in libexec/* ; do
        commands="${commands} ${one}"
    done
    ${INSTALL} -m555 $commands ${DESTDIR}${SUBCOMDIR}
}

set -o errexit
set -o xtrace

progsu_value=
progsu_flag=
progchroot_value=
progchroot_flag=
while test -n "$1" ; do
    arg="$1"
    case $arg in
        COMMANDS=*)
            COMMANDS="${arg#COMMANDS=}"
            break
        ;;
        PROGSU=*)
            progsu_value="${arg#PROGSU=}"
            progsu_flag=1
        ;;
        PROGCHROOT=*)
            progchroot_value="${arg#PROGCHROOT=}"
            progchroot_flag=1
        ;;
        *=*)
            value="${arg#*=}"
            varname="${arg%%=*}"
            eval "${varname}=\"${value}\""
        ;;
    esac
    shift
done

if test -n "${COMMANDS}" ; then
    install_commands
    exit
fi

install_system
install_tools "$@"
