# API variables
# the ones, used in build scripts, are all upper case, used inside engine begin with lower case.

# The code of entire engine is included inside this file.

set -o errexit

ANYAPI="0"
P=${rdd_atom_id}

# check if we were launched directly without frontend
if test "${0}" != "${0%.build}" ; then
    bool_DIRECT_LAUNCH=1
    draft_name="${0%/*}"
    if test -z "${draft_name}" ; then
        draft_name='.'
    fi
    draft_name="$(cd ${draft_name} ; pwd -P)"
    P="${draft_name##*/}"
    unset draft_name
fi

APROFILE=${any_prof_prefix}${any_prof_core}${any_prof_postfix}

BASEDIR=${any_base_dir}
if test ! -z "${BASEDIR}" -a "${BASEDIR}" != '/' ; then
    BASEDIR="${BASEDIR}/"
fi

WORKDIR=${BASEDIR}${any_write_root}/work/${APROFILE}
S=${WORKDIR}/${P}
TMPDIR=${BASEDIR}${any_write_root}/tmp/${APROFILE}
T=${TMPDIR}/${P}
IMGDIR=${BASEDIR}${any_write_root}/image/${APROFILE}
D=${IMGDIR}/${P}
PACKDIR=${BASEDIR}${any_write_root}/pack/${APROFILE}
LOGDIR=${BASEDIR}${any_write_root}/log/${APROFILE}
CONTEXTDIR=${BASEDIR}${any_write_root}/context/${APROFILE}
if test "${any_plain_root}" = '1' ; then
    ROOT=${ROOT:-/}
    readonly_ROOT=1
elif test "${any_static_root}" = '1' ; then
    ROOT=${ROOT:-${CONTEXTDIR}/static/root}
    static_ROOT=1
else
    ROOT=${ROOT:-${CONTEXTDIR}/${P}/root}
fi
pack_CONTEXT=${CONTEXTDIR}/${P}/pack
common_EXEC=${TMPDIR}/static/bin
pack_EXEC=${T}/bin

ANYSRCDIR=${BASEDIR}${any_src_dir}
ANYDEVSRCDIR=${BASEDIR}${any_dev_src_dir}

empty_DIR=${BASEDIR}${any_write_root}/empty
out_BASEDIR="${any_out_basedir}"

unset bool_SANDBOX
if test -n "${any_bool_sandbox}" -a "${any_bool_sandbox}" = '1' ; then
    bool_SANDBOX='1'
fi
if test -z "${any_bool_sandbox}" -a "${BASEDIR}" = '/' ; then
    bool_SANDBOX='1'
fi

SUBFS="${SUBFS:-${any_program_subfs}}"
PREFIX="${PREFIX:-${any_program_prefix}}"
prefix_list_tmp="${any_program_prefix_list}"
if test -z "${PREFIX}" ; then
    tail_tmp=${prefix_list_tmp#* }
    PREFIX=${prefix_list_tmp% ${tail_tmp}}
fi
if test -z "${prefix_list_tmp}" ; then
    prefix_list_tmp="${PREFIX}"
fi

ROOT_CFLAGS=
ROOT_LDFLAGS=
ROOT_DIRS=
PREFIX_LIST=
for pref in ${prefix_list_tmp} ; do
    PREFIX_LIST="${PREFIX_LIST} ${SUBFS}${pref}"
    if test "${pref}" = '/' ; then
        pref=
    fi
    root_prep="${ROOT}"
    if test "${root_prep}" = '/' ; then
        root_prep=
    fi
    ROOT_CFLAGS="${ROOT_CFLAGS} -I${root_prep}${SUBFS}${pref}/include"
    ROOT_LDFLAGS="${ROOT_LDFLAGS} -L${root_prep}${SUBFS}${pref}/lib -Wl,-rpath-link,${root_prep}${SUBFS}${pref}/lib"
    ROOT_DIRS="${ROOT_DIRS} ${root_prep}${SUBFS}${pref}"
done
unset prefix_list_tmp pref tail_tmp root_prep

# no root \ arch cxxflags
ARCH_FLAGS="${ARCH_FLAGS:-${any_arch_flags}}"
ARCH_LDFLAGS="${ARCH_LDFLAGS:-${any_arch_ldflags}}"

OPT_FLAGS="${OPT_FLAGS:-${any_opt_flags}}"
OPT_LDFLAGS="${OPT_LDFLAGS:-${any_opt_ldflags}}"

ENGINE_CFLAGS="${ARCH_FLAGS} ${ROOT_CFLAGS} ${any_morebuild_cflags}"
ENGINE_CXXFLAGS="${ARCH_FLAGS} ${ROOT_CFLAGS} ${any_morebuild_cxxflags}"
ENGINE_LDFLAGS="${ARCH_LDFLAGS} ${ROOT_LDFLAGS} ${any_morebuild_ldflags}"

root_TEXTBUILD_FILES="${root_TEXTBUILD_FILES:-${any_root_textbuild_files}}"
root_TRACKED_DIRS="${root_TRACKED_DIRS:-${any_root_tracked_dirs}}"
root_TRACKED_BIN_DIRS="${root_TRACKED_BIN_DIRS:-${any_root_tracked_bin_dirs}}"

image_TRACKED_LIB_DIRS="${any_image_libs}"
image_TRACKED_EXEC_DIRS="${any_image_exec}"

edit_RULES_FILE='tmp_edit_rules'
msg_FETCH_STOP='nodev'

MAKEOPTS="${MAKEOPTS:-${any_make_opts}}"

log_OUTER=${rdd_log_stdout}
log_PROFILE=${LOGDIR}/${APROFILE}'.log'
log_TERMINAL=${rdd_log_num_terminal:-1}
LOG=${LOGDIR}/${P}.log
ENVDUMP=${LOGDIR}/env.${P}
DUMP="${any_dump:-${rdd_atom_dumpdir}/dump.sh}"

# toolchain variables

PLAIN_CC="${PLAIN_CC:-${any_tch_cc}}"
PLAIN_CXX="${PLAIN_CXX:-${any_tch_cxx}}"
PLAIN_CCLD="${PLAIN_CCLD:-${any_tch_ccld}}"
PLAIN_CXXLD="${PLAIN_CXXLD:-${any_tch_cxxld}}"
PLAIN_CPP="${PLAIN_CPP:-"${any_tch_cc} -E"}"
if test -z "${PLAIN_CCLD}" ; then
    PLAIN_CCLD="${PLAIN_CC}"
fi
if test -z "${PLAIN_CXXLD}" ; then
    PLAIN_CXXLD="${PLAIN_CXX}"
fi

HOST_CC="${HOST_CC:-${any_tch_host_cc}}"
HOST_CXX="${HOST_CXX:-${any_tch_host_cxx}}"
HOST_CCLD="${HOST_CCLD:-${any_tch_host_ccld}}"
HOST_CXXLD="${HOST_CXXLD:-${any_tch_host_cxxld}}"
HOST_CPP="${HOST_CPP:-${any_tch_host_cc} -E}"
if test -z "${HOST_CCLD}" ; then
    HOST_CCLD="${HOST_CC}"
fi
if test -z "${HOST_CXXLD}" ; then
    HOST_CXXLD="${HOST_CXX}"
fi

CC="${CC:-${PLAIN_CC}}"
CXX="${CXX:-${PLAIN_CXX}}"
CCLD="${CCLD:-${PLAIN_CCLD}}"
CXXLD="${CXXLD:-${PLAIN_CXXLD}}"
CPP="${CPP:-${PLAIN_CPP}}"

UTILROOT="${any_util_root}"
UTILMAKE="${any_util_make:-make}"

LINGUAS="${any_program_linguas}"

DISTDIR="${any_rdd_root}/${rdd_atom_path}/${P}"
if test ! -d "${DISTDIR}" ; then
    # package names can be hashed with first letter
    postfix_aname="${P#[a-zA-Z0-9]}"
    pack_hash="${P%${postfix_aname}}"
    if test -d "${any_rdd_root}/${rdd_atom_path}/${pack_hash}/${P}" ; then
        DISTDIR="${any_rdd_root}/${rdd_atom_path}/${pack_hash}/${P}"
    fi
    unset postfix_aname pack_hash
fi
DISTBASEDIR="${any_rdd_root}/${rdd_atom_path}"
METADIR="${BASEDIR}${any_meta_dir}"
MINTDIR="${METADIR}/mint/${APROFILE}/${P}"
MINTNEUTRALDIR="${METADIR}/all/${P}"

atom_static_PATCHDIR=${DISTDIR}/patch
static_PATCHDIR=${BASEDIR}${any_static_patch_dir}/${P}
PATCHDIR=${BASEDIR}${any_write_root}/patch/${P}
distfiles_DIR=${DISTDIR}/files

CONFIGCACHE="${T}/config.cache"

base_INNER_VERSION="${any_base_inner_version}"
RELEASE="${RELEASE:-${any_release_id}}"
RELEASE_VERSION="${any_release_version}"
RELEASEDIR="${BASEDIR}${any_release_dir:-release}"

libcore_DIR=${BASEDIR}any/corelib
binany_DIR=${BASEDIR}any/bin

AUSE="${rdd_prf_all}"

# entire engine

if test "${any_bool_loaddump}" != '1' ; then
    . ${libcore_DIR}/proc.sh
    . ${libcore_DIR}/interfaces.sh
    . ${libcore_DIR}/prepare.sh
    . ${libcore_DIR}/filter.sh
    . ${libcore_DIR}/print.sh
    . ${libcore_DIR}/pack.sh
    . ${libcore_DIR}/image.sh
    . ${libcore_DIR}/root.sh
    . ${libcore_DIR}/fetch.sh
    . ${libcore_DIR}/patches.sh
    . ${libcore_DIR}/status.sh
    . ${libcore_DIR}/depend.sh
    . ${libcore_DIR}/keywords.sh
    . ${libcore_DIR}/admin.sh
fi

. ${libcore_DIR}/pkg.sh
. ${libcore_DIR}/src.sh

# default map

. ${libcore_DIR}/map.sh

# set of generic non-any variables like PATH

. ${libcore_DIR}/env.sh

PN=${P%%-[0-9][^-]*}
PN=${PN%%-[0-9]}
PV=${P##${PN}-}
if test "${PV}" = "${PV#[0-9]}" ; then
    PV='0.0'
fi
MY_P=${MY_P:-${P}}
MY_PD=${MY_PD:-${P}}

BP="${P}"
BPN="${PN}"
BPV="${PV}"
BPI="${PV}"
if test "${any_bool_autobpi}" = 1 ; then
    first_num=${BPI%%.*}
    if test "${first_num}" != "${BPI}" ; then
        second_num=${BPI#${first_num}.}
        second_num=${second_num%%.*}
        BPI="${first_num}.${second_num}"
    fi
    unset first_num second_num
fi

DEPEND=
RDEPEND=
HDEPEND=
KEYWORDS=
FLAVOUR=
PATCHLIST=

SRC_URI=
DESCRIPTION=
INNER_VERSION=

ANYARCH="${any_arch}"
HOSTARCH="${any_host_arch}"
ANYARCHMODE="${any_arch_family}"
ANYARCHCAP="${any_arch_capacity}"
ANYARCHENDIAN="${any_arch_endian}"
ANYOS="${any_os}"
if test -z "${ANYARCH}" ; then
    ANYARCH='all'
fi
if test -z "${ANYARCHMODE}" ; then
    ANYARCHMODE='all'
fi
if test -z "${ANYARCHCAP}" ; then
    ANYARCHCAP='64'
fi

arch_related=${any_autoconf_arch}
if test -z "${arch_related}" && \
    test "${ANYARCH}" != 'all' && \
    test -n "${HOSTARCH}" && \
    test "${ANYARCH}" != "${HOSTARCH}" ; then
    arch_related=${ANYARCH}
fi
os_related=${any_autoconf_os}
if test -n "${arch_related}" -a -z "${os_related}" ; then
    os_related=${ANYOS}
fi
separator=
if test -n "${arch_related}" -a -n "${os_related}" ; then
    separator='-'
fi

if test -n "${arch_related}" ; then
    CONFIG_TARGET="${arch_related}${separator}${os_related}"
fi
if test -n "${HOSTARCH}" ; then
    CONFIG_HOST="${HOSTARCH}"
else
    CONFIG_HOST="${CONFIG_TARGET}"
fi

unset arch_related os_related separator

if test -f "${DISTDIR}/${PN}.build" ; then
    abuild_SRC="${DISTDIR}/${PN}.build"
    checked_abuild=1
else
    abuild_SRC="${DISTDIR}/${P}.build"
fi

# behaviour from command words

loaded_MODULES=

for engine in ${any_profile_engine} ; do
    for prof in ${AUSE} ; do
        if test -f ${BASEDIR}${engine}/${prof}.sh ; then
            . ${BASEDIR}${engine}/${prof}.sh
            loaded_MODULES="${loaded_MODULES}
${BASEDIR}${engine}/${prof}.sh"
        fi
    done
done

if test "${any_bool_loaddump}" != 1 ; then
    for engine in ${any_profile_lib} ; do
        for prof in ${AUSE} ; do
            if test -f ${BASEDIR}${engine}/${prof}.sh ; then
                . ${BASEDIR}${engine}/${prof}.sh
                loaded_MODULES="${loaded_MODULES}
${BASEDIR}${engine}/${prof}.sh"
            fi
        done
    done
fi

# include personal material from atom

if test -z "${bool_DIRECT_LAUNCH}" ; then
    if test "${checked_abuild}" = 1 -o -f "${abuild_SRC}" ; then
        . "${abuild_SRC}"
    fi
fi
unset checked_abuild
for prof in ${AUSE} ; do
    if test -f ${DISTDIR}/${prof}.sh ; then
        . ${DISTDIR}/${prof}.sh
        loaded_MODULES="${loaded_MODULES}
${DISTDIR}/${prof}.sh"
    fi
done

# ---------------------------------------------------------
# Copyright (c) 2015-2022 Random Crew.
# opendistro.org/any
#
# Available under ISC License.
# ---------------------------------------------------------

