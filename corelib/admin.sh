admin_host()
{
    # do not mess up with host without explicit setting
    if test -z "${any_host_update}" ; then
        return
    fi

    return 0
}

admin_owner()
{
    return 0
}

admin_owner_mini()
{
    if test "$(id -u)" = '0' ; then
        if test -d ${T} ; then
            chown -R ${any_admin_userid}:${any_admin_groupid} ${T}
        fi
        if test -d ${TMPDIR} ; then
            chown ${any_admin_userid}:${any_admin_groupid} ${TMPDIR}
        fi
        if test -d ${rdd_atom_dumpdir} ; then
            chown -R ${any_admin_userid}:${any_admin_groupid} ${rdd_atom_dumpdir}
        fi
    fi
}
