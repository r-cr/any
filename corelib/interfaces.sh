# all purpose interfaces for builds

awarn()
{
    if test ! -z "${@}" ; then
        printf "%s\n" "${@}" 1>&2
    fi
}

alog()
{
    if test ! -z "${@}" ; then
        printf "%s\n" "${@}"
    fi
}

nonfail()
{
    return 0
}

die()
{
    awarn "${@}"
    exit 1
}

skip()
{
    awarn "${@}"
    exit 13
}

skiplater()
{
    awarn "${@}"
    status_set_skip
}
dielater()
{
    awarn "${@}"
    status_set_fail
}

forcetobuild()
{
    mkdir -p "${S}"
    cd "${S}"
}

tobuild()
{
    if test -d "${S}" ; then
        cd "${S}"
    fi
}

rmbuild()
{
    mkdir -p ${T}
    cd ${T}
    if test ! -z "${P}" -a -d ${WORKDIR}/${P} ; then
        rm -rf ${WORKDIR}/${P}
    fi
    if test ! -z "${MY_P}" -a -d ${WORKDIR}/${MY_P} ; then
        rm -rf ${WORKDIR}/${MY_P}
    fi
}

dopatch()
{
    input="${1}"

    localdir=$(pwd)
    patchname=
    serialdir=

    # for code readability, check directory and simple filename
    # separately in two different cases, though check conditions are the same
    case "${input}" in
        # if input is absolute filename, prepend nothing to name
        /*)
            if test -d "${input}" ; then
                serialdir="${input}"
            fi
        ;;
        # otherwise check possible directory locations
        *)
            if test -d "${localdir}/${input}" ; then
                serialdir="${localdir}/${input}"
            elif test -d "${PATCHDIR}/${input}" ; then
                serialdir="${PATCHDIR}/${input}"
            elif test -d "${S}/${input}" ; then
                serialdir="${S}/${input}"
            fi
        ;;
    esac

    if test -n "${serialdir}" ; then
        patchlist "${serialdir}" | while read nested_name ; do
            alog "Apply patch ${serialdir}/${nested_name}"
            patch -Np1 < "${serialdir}/${nested_name}"
            alog " "
        done
        # First end of function
        return 0
    fi

    case "${input}" in
        # if input is absolute filename, prepend nothing to name
        /*)
            patchname="${input}"
            if test ! -f "${patchname}" ; then
                awarn "Patch ${patchname} does not exist (as file or directory)"
                return 2
            fi
        ;;
        # otherwise try to find out the location of patch
        *)
            if test -f "${localdir}/${input}" ; then
                patchname="${localdir}/${input}"
            elif test -f "${PATCHDIR}/${input}" ; then
                patchname="${PATCHDIR}/${input}"
            elif test -f "${S}/${input}" ; then
                patchname="${S}/${input}"
            else

                awarn "Patch ${input} was not found here (as file or directory):
${localdir}/${input}
${PATCHDIR}/${input}
${S}/${input}
Check out the name and correctness of your patches.
"
                return 1
            fi
        ;;
    esac


    alog "Apply patch ${patchname}"
    patch -Np1 < "${patchname}"
    alog " "
}

use()
{
    arg=$1

    case " ${USE} " in
        *" ${arg} "*)
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

use_enable()
{
    arg=$1
    text=$2

    if use $arg ; then
        if test -z "${text}" ; then
            text="${arg}"
        fi
        printf "%s" "${text}"
    fi
}

use_notenable()
{
    arg=$1
    text=$2

    if ! use $arg ; then
        if test -z "${text}" ; then
            text="${arg}"
        fi
        printf "%s" "${text}"
    fi
}

ause()
{
    arg=$1

    case " ${AUSE} " in
        *" ${arg} "*)
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

ause_enable()
{
    arg=$1
    text=$2

    if ause $arg ; then
        if test -z "${text}" ; then
            text="${arg}"
        fi
        printf "%s" "${text}"
    fi
}

ause_notenable()
{
    arg=$1
    text=$2

    if ! ause $arg ; then
        if test -z "${text}" ; then
            text="${arg}"
        fi
        printf "%s" "${text}"
    fi
}

available()
{
    arg=$1

    argdir="$(ls ${IMGDIR}/ 2>/dev/null | grep -e "^${arg}" | tail -1)"
    if test -n "${argdir}" -a -d "${IMGDIR}/${argdir}" ; then
        printf "%s" "${arg}"
    fi
}

installed()
{
    arg=$1

    for registered_pack in $(root_get_installed $arg) ; do

        installed_candidate="$( available ${registered_pack} )"
        if test -n "${installed_candidate}" -a "${installed_candidate}" = "${registered_pack}" ; then
            printf "%s" "${installed_candidate}"
            return
        fi
    done
}

linkfile()
{
    link_file "$@"
}

linkdir()
{
    link_dir "$@"
}

unlinkcopy()
{
    unlink_and_copy "$@"
}

rootlink()
{
    root_link "$@"
}

pkgbin()
{
    params=

    if test ! -t 0 ; then
        while read line_input_pack <&0 ; do
            for input_pack in ${line_input_pack} ; do
                params=1
                status_set_ok
                (
                load_env ${input_pack}
                pack_name_file
                ) || nonfail
            done
        done
    fi

    while test -n "$1" ; do
        params=1
        status_set_ok
        (
        load_env $1
        pack_name_file
        ) || nonfail
        shift
    done

    if test -z "${params}" ; then
        keywords_check
        status_check
        pack_name_file
    fi
}

pkgname()
{
    params=

    if test ! -t 0 ; then
        while read line_input_pack <&0 ; do
            for input_pack in ${line_input_pack} ; do
                params=1
                status_set_ok
                (
                load_env ${input_pack}
                pack_name
                ) || nonfail
            done
        done
    fi

    while test -n "$1" ; do
        params=1
        status_set_ok
        (
        load_env $1
        pack_name
        ) || nonfail
        shift
    done

    if test -z "${params}" ; then
        keywords_check
        status_check
        pack_name
    fi
}

pkgbase()
{
    params=

    if test ! -t 0 ; then
        while read line_input_pack <&0 ; do
            for input_pack in ${line_input_pack} ; do
                params=1
                status_set_ok
                (
                load_env ${input_pack}
                pack_name_base
                ) || nonfail
            done
        done
    fi

    while test -n "$1" ; do
        params=1
        status_set_ok
        (
        load_env $1
        pack_name_base
        ) || nonfail
        shift
    done

    if test -z "${params}" ; then
        keywords_check
        status_check
        pack_name_base
    fi
}

pkgbinfiles()
{
    params=

    if test ! -t 0 ; then
        while read line_input_pack <&0 ; do
            for input_pack in ${line_input_pack} ; do
                params=1
                status_set_ok
                (
                load_env ${input_pack}
                pack_name_file "checked"
                ) || nonfail
            done
        done
    fi

    while test -n "$1" ; do
        params=1
        status_set_ok
        (
        load_env $1
        pack_name_file "checked"
        ) || nonfail
        shift
    done

    if test -z "${params}" ; then
        keywords_check
        status_check
        pack_name_file "checked"
    fi
}

inherit()
{
    word="$1"

    hashed_word="$(hash_atom_name "${word}" )"
    for local_base in "${DISTBASEDIR}/${word}" "${DISTBASEDIR}/${hashed_word}" ; do
        if test -d "${local_base}" ; then

            needed=
            shortword="$(depend_get_pack_name ${word})"
            if test -f "${local_base}/${shortword}.build" ; then
                needed="${local_base}/${shortword}.build"
            elif test -f "${local_base}/${word}.build" ; then
                needed="${local_base}/${word}.build"
            fi
            if test -n "${needed}" ; then
                . "${needed}"
                loaded_MODULES="${loaded_MODULES}
${needed}"
            fi

            for prof in ${AUSE} ; do
                if test -f "${local_base}/${prof}.sh" ; then
                    . "${local_base}/${prof}.sh"
                    loaded_MODULES="${loaded_MODULES}
${local_base}/${prof}.sh"
                fi
            done
            # use one variant of two, not both
            break
        fi
    done
    unset word hashed_word
}

patchlist()
{
    dir_to_list="${1}"
    if test -z "${dir_to_list}" ; then
        dir_to_list="${PATCHDIR}"
    elif test -d "${dir_to_list}" ; then
        dir_to_list="$(cd ${dir_to_list} ; pwd -P)"
    else
        awarn "Non-directory as argument for patchlist: ${dir_to_list}"
        return 1
    fi

    if test -d "${dir_to_list}" ; then
        # template in -name: exclude hidden files, beginning with dot symbol
        find "${dir_to_list}" \( ! -path "${dir_to_list}" -prune \) -type f -name "[^\.]*" -print | sort | \
            while read name ; do
                printf "%s\n" "${name#${dir_to_list}/}"
            done
    fi
}

getsrc()
{
    save_trace_and_hide_verbose

    name=
    path=
    actual_patchlist=
    while test -n "${1}" ; do
        case "${1}" in
            PATCHLIST=*)
                actual_patchlist="${1#PATCHLIST=}"
            ;;
            *)
                if test -z "${name}" ; then
                    name="${1}"
                elif test -z "${path}" ; then
                    path="${1}"
                fi
            ;;
        esac
        shift
    done

    if test -z "${name}" ; then
        die "No package name for getsrc was given"
    fi

    workpath=
    if test -z "${path}" ; then
        workpath="$(pwd)"
    else
        mkdir -p ${path}
        if ! test -d "${path}" ; then
            die "Source directory ${path} for getsrc is unavailable"
        fi
        workpath="$(cd ${path} ; pwd -P)"
    fi
    path="${workpath}/${name}"

    (
    actual_tmpdir=${T}/getsrc
    MY_P=${name}
    MY_PD=${name}
    load_env ${name}

    T=${actual_tmpdir}
    rm -rf ${T}
    # PATCHLIST was redefined explicitly
    if test -n "${actual_patchlist}" ; then
        PATCHLIST="${actual_patchlist}"
    fi
    # Condition "${ANYDEVSRCDIR}/${P}" = "${PATCHDIR}" means our development sources
    # are in the form of patches.
    # In that case we need to change PATCHDIR and ANYDEVSRCDIR together,
    # so that results after src_fetch will be in the dir, known by src_prepare.
    if test "${ANYDEVSRCDIR}/${P}" = "${PATCHDIR}" ; then
        ANYDEVSRCDIR=${actual_tmpdir}/patch
    fi
    PATCHDIR=${actual_tmpdir}/patch/${P}
    WORKDIR=${workpath}
    S=${path}

    src_fetch
    src_prepare

    # some hack: move fetched patches to directory with main sources
    # it would be possible to access patches of another package from calling script \ build,
    # as the caller knows only the path to main sources of another package ${S}
    if test -d "${PATCHDIR}" -a -d "${S}" && \
        test ! -d "${S}/patch" ; then
        mv ${PATCHDIR} ${S}/patch
    fi
    )
    restore_trace_state
}

storesrc()
{
    name="$1"

    (
    MY_P=${name}
    MY_PD=${name}
    load_env ${name}

    src_store

    )
}

directbuild()
{
    if test -n "${bool_DIRECT_LAUNCH}" ; then
        return 0
    fi
    return 1
}

dumpbuild()
{
    return 0
}


# ----------- option interfaces ------------------

configtoolchain()
{
    if test -n "${CC}" ; then
        printf "%s\n" "CC='${CC}'"
    fi
    if test -n "${CXX}" ; then
        printf "%s\n" "CXX='${CXX}'"
    fi
    if test -n "${LD}" ; then
        printf "%s\n" "LD='${LD}'"
    fi
    if test -n "${CXXLD}" ; then
        printf "%s\n" "CXXLD='${CXXLD}'"
    fi
    if test -n "${AR}" ; then
        printf "%s\n" "AR='${AR}'"
    fi
    if test -n "${RANLIB}" ; then
        printf "%s\n" "RANLIB='${RANLIB}'"
    fi
    if test -n "${STRIP}" ; then
        printf "%s\n" "STRIP='${STRIP}'"
    fi
    if test -n "${OBJDUMP}" ; then
        printf "%s\n" "OBJDUMP='${OBJDUMP}'"
    fi
    if test -n "${NM}" ; then
        printf "%s\n" "NM='${NM}'"
    fi
}

configflags()
{
    if test -n "${CFLAGS}" ; then
        printf "%s\n" "CFLAGS='${CFLAGS}'"
    fi
    if test -n "${CXXFLAGS}" ; then
        printf "%s\n" "CXXFLAGS='${CXXFLAGS}'"
    fi
    if test -n "${LDFLAGS}" ; then
        printf "%s\n" "LDFLAGS='${LDFLAGS}'"
    fi
}

# the most generic options only.
# for redefinition by platform

configopts()
{
    printf "%s\n" "
--prefix='${PREFIX}'
"
    if test -n "${CONFIG_TARGET}" ; then
        printf "%s\n" "
--build='${CONFIG_HOST}'
--host='${CONFIG_TARGET}'
"
    fi
}

configdefault()
{
    configtoolchain
    configflags
    configopts
}

aconf()
{

    CONFIGURE="${CONFIGURE:-./configure}"
    eval \
    ${CONFIGURE} \
        $(configdefault) \
        "${@}"
}

aconf_empty()
{
    CONFIGURE="${CONFIGURE:-./configure}"
    ${CONFIGURE} \
        --prefix="${PREFIX}" \
        "${@}"
}

amake()
{
    UTILMAKE="${UTILMAKE:-make}"
    ${UTILMAKE} DESTDIR=${D} ${MAKEOPTS} \
        "${@}"
}

loadinclude()
{
    local word="$1"

    for moddir in ${any_profile_engine} ; do
        if test -f "${BASEDIR}${moddir}/${word}.sh" ; then
            . "${BASEDIR}${moddir}/${word}.sh"
            loaded_MODULES="${loaded_MODULES}
${BASEDIR}${moddir}/${word}.sh"
        fi
    done
}

loadlib()
{
    local word="$1"

    for moddir in ${any_profile_lib} ; do
        if test -f "${BASEDIR}${moddir}/${word}.sh" ; then
            . "${BASEDIR}${moddir}/${word}.sh"
            loaded_MODULES="${loaded_MODULES}
${BASEDIR}${moddir}/${word}.sh"
        fi
    done
}

loadpack()
{
    local word="$1"

    for moddir in ${DISTDIR} ; do
        if test -f "${BASEDIR}${moddir}/${word}.sh" ; then
            . "${BASEDIR}${moddir}/${word}.sh"
            loaded_MODULES="${loaded_MODULES}
${BASEDIR}${moddir}/${word}.sh"
        fi
    done
}

load()
{
    local word="$1"

    loadinclude "${word}"
    loadlib "${word}"
    loadpack "${word}"
}
