# functions to log

print_session()
{
    save_trace_state
    set +o xtrace

    session_info="\

APROFILE: ${APROFILE}
AUSE: ${AUSE}
ANYARCH: ${ANYARCH} HOSTARCH: ${HOSTARCH} ANYOS: ${ANYOS}
ANYARCHMODE: ${ANYARCHMODE} ANYARCHCAP: ${ANYARCHCAP} ANYARCHENDIAN: ${ANYARCHENDIAN}
PLAIN_CC: ${PLAIN_CC}
PLAIN_CXX: ${PLAIN_CXX}

"

(
    printf "%s" "${session_info}"
) >& ${log_TERMINAL} 2>& ${log_TERMINAL}

    mkdir -p "${LOGDIR}"
(
    printf "%s" "${session_info}"
) >> ${log_PROFILE} 2>> ${log_PROFILE}

    restore_trace_state
}       

print_log_header()
{
    save_trace_state

    log_info="\

---------------------------------

Date: $(date)

APROFILE: ${APROFILE}
AUSE: ${AUSE}
USE: ${USE}
P: ${P}
D: ${D}
S: ${S}
PN: ${PN} PV: ${PV}
ANYSRCDIR: ${ANYSRCDIR}
PATCHDIR: ${PATCHDIR}


ANYARCH: ${ANYARCH} HOSTARCH: ${HOSTARCH} ANYOS: ${ANYOS}
ANYARCHMODE: ${ANYARCHMODE} ANYARCHCAP: ${ANYARCHCAP} ANYARCHENDIAN: ${ANYARCHENDIAN}
ENGINE_CFLAGS: ${ENGINE_CFLAGS}
ENGINE_CXXFLAGS: ${ENGINE_CXXFLAGS}
ENGINE_LDFLAGS: ${ENGINE_LDFLAGS}
MAKEOPTS: ${MAKEOPTS}


Loaded modules: ${loaded_MODULES}


PLAIN_CC: ${PLAIN_CC}
PLAIN_CXX: ${PLAIN_CXX}
PLAIN_LD: ${PLAIN_LD}
PLAIN_CXXLD: ${PLAIN_CXXLD}
PLAIN_CPP: ${PLAIN_CPP}


HOST_CC: ${HOST_CC} HOST_CXX: ${HOST_CXX}
HOST_LD: ${HOST_LD} HOST_CXXLD: ${HOST_CXXLD}


PATH: ${PATH}
PREFIX: ${PREFIX}
SUBFS: ${SUBFS}

---------------------------------

"
    printf "%s" "${log_info}"

    restore_trace_state
}

print_atom()
{
    save_trace_state
    set +o xtrace

    allowed_atom_length=$(( ${maximum_line_length}-${result_line_length} ))
    atom_length=$( printf "%s" "${what_to_print_about_atom}" | wc -m | tr -d ' ' )
    if [ ${atom_length} -gt ${allowed_atom_length} ] ; then
        what_to_print_about_atom=$( printf "%s" "${what_to_print_about_atom}" | cut -c 1-${allowed_atom_length} )
    fi
(
    printf "%s" "${what_to_print_about_atom}"
) >& ${log_TERMINAL} 2>& ${log_TERMINAL}
(
    printf "%s" "${what_to_print_about_atom}"
) >> ${log_PROFILE} 2>> ${log_PROFILE}

    restore_trace_state
}

print_final_atom()
{
    save_trace_state
    set +o xtrace

    time_string="(${time_TOTAL})"
    left_string="${what_to_print_about_atom}"
    left_length=$(printf "%s" "${left_string}" | wc -m | tr -d ' ' )
    time_alignment=$(( ${maximum_line_length}-${status_line_length}-${left_length} ))
    status_alignment=$(( ${status_line_length} ))
(
    if [ -t ${log_TERMINAL} ] ; then
        printf "%${time_alignment}s\e${status_COLOR}%${status_alignment}s\e${color_blank}\n" "${time_string}" ${status_GLOBAL}
    else
        printf "%${time_alignment}s%${status_alignment}s\n" "${time_string}" ${status_GLOBAL}
    fi
) >& ${log_TERMINAL} 2>& ${log_TERMINAL}
(
    printf "%${time_alignment}s%${status_alignment}s\n" "${time_string}" ${status_GLOBAL}
) >> ${log_PROFILE} 2>> ${log_PROFILE}

    restore_trace_state
}

print_path_log()
{
    save_trace_state
    set +o xtrace

    if [ "${status_GLOBAL}" != 'FAIL' ] ; then
        return 0
    fi
    log_path="${LOG}"
    if [ -n "${bool_SANDBOX}" ] ; then
        log_path="${out_BASEDIR}${log_path}"
    fi
    (
        printf "%s\n" "${log_path}"
    ) >& ${log_TERMINAL} 2>& ${log_TERMINAL}

    restore_trace_state
}
