# functions to support single root image, visible for build process

root_create_rules_to_install()
{
    mkdir -p ${T}
    : > ${T}/${edit_RULES_FILE}

    is_root_prefix=
    for path_string in $( get_plain_tracked_dirs ) ; do
        printf "%s\n" "s#^([^ $\/]*)[\/]*${path_string}#\1${ROOT}${path_string}#g" >> ${T}/${edit_RULES_FILE}
        printf "%s\n" "s#( [^ $\/]*)[\/]*${path_string}#\1${ROOT}${path_string}#g" >> ${T}/${edit_RULES_FILE}
    done

    for pref in ${PREFIX_LIST} ; do
        if [ ${pref} = '/' ] ; then
            is_root_prefix=1
            continue
        else
            printf "%s\n" "s#^([^ $\/]*)[\/]*${pref}#\1${ROOT}${pref}#g" >> ${T}/${edit_RULES_FILE}
        fi
    done

    if [ -n "${is_root_prefix}" ] ; then

        for tracked_dir in ${root_TRACKED_DIRS} ; do
            path_string="/${tracked_dir}"
            printf "%s\n" "s#^([^ $\/]*)${path_string}#\1${ROOT}${path_string}#g" >> ${T}/${edit_RULES_FILE}
            printf "%s\n" "s#( [^ $\/]*)${path_string}#\1${ROOT}${path_string}#g" >> ${T}/${edit_RULES_FILE}
        done
        printf "%s\n" "s#^([^ $\/]*)/\$#\1${ROOT}#g" >> ${T}/${edit_RULES_FILE}
    fi
}

root_setup_paths_to_install()
{
    alog "Preparing files to point to ROOT:"

    root_create_rules_to_install

    where_to_find=$( get_installed_tracked_dirs ${ROOT} )
    if [ -z "${where_to_find}" ] ; then
        return
    fi

    what_to_find="$(printf "%s\n" "${root_TEXTBUILD_FILES}" | tr ' ' '\n' )"
    mkdir -p ${T}
    file_what_to_find=${T}/'grep_template'
    printf "%s\n" "${what_to_find}" > ${file_what_to_find}

    find ${where_to_find} -type f -o -type l | grep -f ${file_what_to_find} | \
        filter_text_executable_files | \
        unlink_and_copy | \
        edit_by_rules
}

root_register()
{
    alog "Registering pack ${P} in ROOT"

    mkdir -p ${ROOT}/pkgdb
    : > ${ROOT}/pkgdb/${P}

    version_part=
    if test "${BPI}" != "${PV}" ; then
        version_part=":${BPI}"
    fi

    if test "${BP}" != "${P}" ; then
        for entry in ${BP} ; do
            pack_try="$(depend_get_pack_name ${entry})"
            printf "%s\n" "${pack_try}${version_part}" >> ${ROOT}/pkgdb/${P}
        done
    elif test "${BPN}" != "${PN}" ; then
        for entry in ${BPN} ; do
            printf "%s\n" "${entry}${version_part}" >> ${ROOT}/pkgdb/${P}
        done
    elif test -n "${version_part}" ; then
        printf "%s\n" "${version_part}" >> ${ROOT}/pkgdb/${P}
    fi
}

root_link()
{
    # TODO - misguiding var names
    local base_dir="${1}"

    local target_dir="${base_dir#${D}/}"
    if test "${target_dir}" = "${base_dir}" ; then
        # base_dir does not contain $D
        # then it should contain $IMGDIR + one more dir
        target_dir="${base_dir#${IMGDIR}}"
        if test "${target_dir}" = "${base_dir}" ; then
            awarn "Directory ${base_dir} does not contain \${D} or \${IMGDIR}"
            return 1
        fi
        target_dir="/${target_dir#/*/}"
    else
        target_dir="/${target_dir}"
    fi

    target_dir="${ROOT}${target_dir}"
    if test -d "${base_dir}" ; then
        mkdir -p "${target_dir}"
        link_dir "${base_dir}" "${target_dir}"
    else
        link_file "${base_dir}" "${target_dir}"
    fi
    alog "${base_dir}"
}

root_install()
{

    alog "Mirroring dirs to ROOT: ${ROOT}"
    for base_dir in $( get_installed_tracked_dirs ) ; do
        root_link ${base_dir}
    done

    local target_dir=
    alog "Mirroring exec files to ROOT: ${ROOT}"
    for base_dir in $( get_installed_tracked_exec_dirs ) ; do

        target_dir="${base_dir#${D}}"
        if test "${target_dir}" = "${base_dir}" ; then
            # base_dir does not contain $D
            # then it should contain $IMGDIR + one more dir
            target_dir="${base_dir#${IMGDIR}}"
            if test "${target_dir}" = "${base_dir}" ; then
                awarn "Directory ${base_dir} does not contain \${D} or \${IMGDIR}"
                continue
            fi
            target_dir="/${target_dir#/*/}"
        fi
        target_dir="${ROOT}${target_dir}"
        mkdir -p "${target_dir}"
    done

    for base_exec in $( get_installed_tracked_exec_files ) ; do
        root_link "${base_exec}"
    done

    root_register
}

root_get_installed()
{
    template=${1}

    ls ${ROOT}/pkgdb/ 2> /dev/null | grep -e "^${template}"
}

root_unregister()
{
    rm ${ROOT}/pkgdb/${P} 2> /dev/null || nonfail
    rmdir ${ROOT}/pkgdb 2> /dev/null || nonfail
}

root_clean_pack()
{
    pack=$1
    if [ -z "${pack}" ] ; then
        return
    fi

    where_to_find=$( get_installed_tracked_dirs ${IMGDIR}/${pack} )
    if [ -z "${where_to_find}" ] ; then
        return
    fi
    find ${where_to_find} -type f -o -type l | sed -e "s#${IMGDIR}/${pack}#${ROOT}#" | xargs -I '{}' rm -f '{}' || nonfail
}

root_clean_by_fire()
{
    if [ -n "${ROOT}" -a "${ROOT}" != "${ROOT#${CONTEXTDIR}}" ] ; then
        rm -rf "${ROOT}"
    fi
}

root_platform_populate()
{
    if test -d "${CONTEXTDIR}/platform/" ; then
        save_trace_and_set_verbose
        mkdir -p ${ROOT}
        for component in ${CONTEXTDIR}/platform/*.tar ; do
            alog "Extracting ${component} to ${ROOT}"
            tar xfp ${component} -C ${ROOT}
        done
        restore_trace_state
    fi
}

root_save_context()
{
    if test "${any_context_save}" = '0' ; then
        return 0
    fi

    (
    cd ${pack_CONTEXT}
    content="$(ls)"
    if test -n "${content}" ; then
        tar cf ${pack_CONTEXT}.tar ${content}
    fi
    )
}

root_extract_archived_context()
{
    pack="$1"
    if test -z "${pack}" ; then
        pack="${P}"
    fi
    if test -f "${CONTEXTDIR}/${pack}/pack.tar" ; then
        mkdir -p ${ROOT}
        alog "Extracting ${CONTEXTDIR}/${pack}/pack.tar to ${ROOT}"
        tar xfp ${CONTEXTDIR}/${pack}/pack.tar -C ${ROOT}
    fi
}

root_install_context()
{
    pack="$1"
    if test -z "${pack}" ; then
        pack="${P}"
    fi
    if test -f "${CONTEXTDIR}/${pack}/pack.tar" ; then
        mkdir -p ${ROOT}
        alog "Extracting ${CONTEXTDIR}/${pack}/pack.tar to ${ROOT}"
        tar xfp ${CONTEXTDIR}/${pack}/pack.tar -C ${ROOT}
    else
    (
        external_root=${ROOT}
        load_env_nocheck ${pack}
        ROOT=${external_root}
        pkg_roothandler
    )
    fi
}

root_dep_populate()
{
    if [ -n "${static_ROOT}" ] ; then
        return
    fi
    for dep in $(depend_resolve_current_atom) ; do
        if [ "${dep}" != "${dep#'--'}" ] ; then
            # '--' marker for non-resolved dependencies
            die
        fi
        root_install_context ${dep}
    done

    root_setup_paths_to_install
}

