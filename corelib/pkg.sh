
# default pkg_* functions

pkg_postpretend()
{
    return
}

pkg_pretend()
{
    save_trace_and_hide_verbose

    keywords_check
    status_check

    restore_trace_state
    pkg_postpretend
}

pkg_presetup()
{
    return
}
pkg_postsetup()
{
    return
}
pkg_setup_extend_pre()
{
    return
}
pkg_setup_extend()
{
    return
}

pkg_setup()
{
    pkg_presetup

    save_trace_and_hide_verbose

    pkg_setup_extend_pre

    prepare_clean_tmp_timestamp
    prepare_wrap

    pkg_setup_extend
    restore_trace_state

    pkg_postsetup
}

pkg_preinstall()
{
    return
}
pkg_postinstall()
{
    return
}
pkg_install_extend()
{
    return
}
pkg_install_extend_pre()
{
    return
}

pkg_install()
{
    pkg_preinstall

    save_trace_and_hide_verbose

    pkg_install_extend_pre

    image_remove_la

    pkg_install_extend
    restore_trace_state

    pkg_postinstall
}

pkg_preroot()
{
    return
}

pkg_postroot()
{
    return
}
pkg_root_extend()
{
    return
}
pkg_root_extend_pre()
{
    return
}

# non-standard method structure

pkg_roothandler()
{
    pkg_preroot

    save_trace_and_hide_verbose

    pkg_root_extend_pre

    root_install

    pkg_root_extend
    restore_trace_state

    pkg_postroot
}

pkg_root()
{
    (
    ROOT=${pack_CONTEXT}
    root_clean_by_fire
    pkg_roothandler
    )

    root_save_context
    root_extract_archived_context
}

pkg_preconfig()
{
    return
}

pkg_postconfig()
{
    return
}

pkg_config_extend()
{
    return
}
pkg_config_extend_pre()
{
    return
}

pkg_config()
{
    pkg_preconfig

    save_trace_and_hide_verbose
    pkg_config_extend_pre

    image_split_pack
    pack_gen_meta

    pkg_config_extend
    restore_trace_state

    pkg_postconfig
}

pkg_prepack()
{
    return
}

pkg_postpack()
{
    return
}

pkg_pack_extend_pre()
{
    return
}

pkg_pack_extend()
{
    return
}

pkg_pack()
{
    pkg_prepack

    save_trace_and_hide_verbose
    pkg_pack_extend_pre

    pack_create
    pack_info

    pkg_pack_extend
    restore_trace_state

    pkg_postpack
}

pkg_precontext()
{
    return
}
pkg_postcontext()
{
    return
}
pkg_context_extend()
{
    return
}
pkg_context_extend_pre()
{
    return
}

pkg_context()
{
    pkg_precontext

    save_trace_and_hide_verbose
    pkg_context_extend_pre

    root_clean_by_fire
    root_platform_populate
    root_dep_populate

    pkg_context_extend
    restore_trace_state

    pkg_postcontext
}

pkg_prerminstall()
{
    return
}

pkg_postrminstall()
{
    return
}

pkg_rminstall_extend()
{
    return
}
pkg_rminstall_extend_pre()
{
    return
}

pkg_rminstall()
{
    pkg_prerminstall

    save_trace_and_hide_verbose
    pkg_rminstall_extend_pre

    image_clean

    pkg_rminstall_extend
    restore_trace_state

    pkg_postrminstall
}

pkg_deploy()
{
    pkg_pretend
    pack_unpack
    pkg_root
}
