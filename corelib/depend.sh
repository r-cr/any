# methods to handle dependencies

depend_get_current_build()
{
    printf "%s" "${DEPEND}"
}

depend_get_single_build()
{
    single=$1
    (
    load_env ${single}
    printf "%s" "${DEPEND}"
    ) || nonfail
}

depend_get_single_runtime()
{
    single=$1
    (
    load_env ${single}
    printf "%s" "${RDEPEND}"
    ) || nonfail
}

depend_get_single_host()
{
    single=$1
    (
    load_env ${single}
    printf "%s" "${HDEPEND}"
    ) || nonfail
}


depend_detect_version_boundary()
{
    depend="$1"

    case "${depend}" in
        '>='*)
            printf "%s" '>='
        ;;
        '>'*)
            printf "%s" '>'
        ;;
        '<='*)
            printf "%s" '<='
        ;;
        '<'*)
            printf "%s" '<'
        ;;
        '='*)
            printf "%s" '='
        ;;
    esac
}

depend_detect_sortversion_method()
{
    depend="$1"

    case "${depend}" in
        '>='*)
            printf "%s" "depend_sortversion_ge"
        ;;
        '>'*)
            printf "%s" "depend_sortversion_gt"
        ;;
        '<='*)
            printf "%s" "depend_sortversion_le"
        ;;
        '<'*)
            printf "%s" "depend_sortversion_lt"
        ;;
        '='*)
            printf "%s" "depend_sortversion_e"
        ;;
        *)
            printf "%s" "depend_sortversion_ge"
        ;;
    esac
}

depend_get_pack_noboundary()
{
    depend="$1"

    name_to_match="${depend#[>=<]}"
    name_to_match="${name_to_match#[>=<]}"

    printf "%s" "${name_to_match}"
}

depend_get_pack_version()
{
    pack=$1
    version=${pack##$(depend_get_pack_name $pack)-}
    if test "${version}" = "${version#[0-9]}" ; then
        version='0.0'
    fi
    printf "%s" ${version}
}

depend_get_pack_name()
{
    pack=$1
    pack=${pack%%-[0-9][^-]*}
    pack=${pack%%-[0-9]}

    printf "%s" "${pack}"
}


depend_sortversion_ge()
{
    name_to_match="$1"
    candidates_array="$2"

    resolved_single_depend=
    current_single_depend="${name_to_match}"
    for candidate in ${candidates_array} ; do
        if arith_greater_equal "${current_single_depend}" "${candidate}"  ; then
            current_single_depend="${candidate}"
            resolved_single_depend=1
        fi
    done
    if test -n "${resolved_single_depend}" ; then
        resolved_single_depend="${current_single_depend}"
        printf "%s" "${resolved_single_depend}"
    fi
}

depend_sortversion_gt()
{
    name_to_match="$1"
    candidates_array="$2"

    resolved_single_depend=
    current_single_depend="${name_to_match}"
    for candidate in ${candidates_array} ; do
        if arith_greater_then "${current_single_depend}" "${candidate}" ; then
            current_single_depend="${candidate}"
            resolved_single_depend=1
        fi
    done
    if test -n "${resolved_single_depend}" ; then
        resolved_single_depend="${current_single_depend}"
        printf "%s" "${resolved_single_depend}"
    fi
}

depend_sortversion_e()
{
    name_to_match="$1"
    candidates_array="$2"

    resolved_single_depend=
    current_single_depend="${name_to_match}"
    for candidate in ${candidates_array} ; do
        if arith_equal "${candidate}" "${current_single_depend}" ; then
            current_single_depend="${candidate}"
            resolved_single_depend=1
        fi
    done
    if test -n "${resolved_single_depend}" ; then
        resolved_single_depend="${current_single_depend}"
        printf "%s" "${resolved_single_depend}"
    fi
}

depend_sortversion_le()
{
    name_to_match="$1"
    candidates_array="$2"

    resolved_single_depend=
    current_single_depend=
    for candidate in ${candidates_array} ; do
        if arith_lower_equal "${name_to_match}" "${candidate}" ; then
            if arith_greater_equal "${current_single_depend}" "${candidate}" ; then
                current_single_depend="${candidate}"
            fi
            resolved_single_depend=1
        fi
    done
    if test -n "${resolved_single_depend}" ; then
        resolved_single_depend="${current_single_depend}"
        printf "%s" "${resolved_single_depend}"
    fi
}

depend_sortversion_lt()
{
    name_to_match="$1"
    candidates_array="$2"

    resolved_single_depend=
    current_single_depend=
    for candidate in ${candidates_array} ; do
        if arith_lower_then "${name_to_match}" "${candidate}" ; then
            if arith_greater_equal "${current_single_depend}" "${candidate}" ; then
                current_single_depend="${candidate}"
            fi
            resolved_single_depend=1
        fi
    done
    if test -n "${resolved_single_depend}" ; then
        resolved_single_depend="${current_single_depend}"
        printf "%s" "${resolved_single_depend}"
    fi
}


depend_get_available()
{
    if test -d ${IMGDIR} ; then
        find ${IMGDIR} -maxdepth 1 -type d   | while read line ; do
            name="${line##*/}"
            printf "%s\n" "${name}"
        done
    fi
}

depend_get_available_desired()
{
    all_profs="$(printf "%s" "${AUSE}" | tr ' ' ',' )"

    entry_all="${any_list_all}"
    # if any_list_all is not defined, leave entry_all blank on purpose.
    # in that case lsrdd will output the default listing,
    # which often is the needed thing
    lsrdd ${all_profs} ${entry_all}
}

depend_get_host_content()
{
    ls -q /var/pkgdb/ 2> /dev/null
}

depend_match_name()
{
    name_to_match="$1"

    current_name=$(depend_get_pack_name ${name_to_match})
    # this is faster then in-shell reading with 'while + case'
    grep -e "^${current_name}-[0-9]" -e "^${current_name}\$" <& 0 || nonfail
}

depend_match_name_exact()
{
    name_to_match="$1"

    # this is faster then in-shell reading with 'while + case'
    grep -e "^${name_to_match}\$" <& 0 || nonfail
}

depend_resolve_current_atom()
{
    depends_current="$( depend_get_current_build )"
    available_current="$( depend_get_available )"
    nonresolved=
    resolved=

    for single_depend in ${depends_current} ; do

        method="$(depend_detect_sortversion_method ${single_depend})"
        name_to_match="$( depend_get_pack_noboundary "${single_depend}" )"
        matching_array="$(printf "%s\n" "${available_current}" | depend_match_name "${name_to_match}" )"

        resolved_single_depend="$( ${method} "${name_to_match}" "${matching_array}" )"

        if test -z "${resolved_single_depend}" ; then
            nonresolved="${nonresolved} ${single_depend}"
        else
            resolved="${resolved} ${resolved_single_depend}"
        fi
    done

    if test ! -z "${nonresolved}" ; then
        printf "%s\n" "--nonresolved"

        message="\
Build dependencies below were not resolved. You should make needed packages available.
${nonresolved}
"
        awarn "${message}"
        return 1
    fi

    printf "%s" "${resolved}"
}

depend_get_required_host()
{
    host_total=
    local_required=
    if [ -n "${any_list_required}" ] ; then
        local_required="${any_list_required}"
    else
        local_required="${rdd_list_entry}"
    fi
    for host_pack in $(lsrdd ${rdd_prf_entry} ${local_required}) ; do
        host_total="${host_total}
$(depend_get_single_host $host_pack)"
    done
    printf "%s\n" ${host_total} | sort | uniq
}

depend_resolve_host()
{
    method_to_list="depend_get_host_content"
    depends_current="$( depend_get_required_host )"
    available_current="$( ${method_to_list} )"
    nonresolved=

    for single_depend in ${depends_current} ; do
        method="$(depend_detect_sortversion_method ${single_depend})"
        name_to_match="$( depend_get_pack_noboundary "${single_depend}" )"
        matching_array="$(printf "%s\n" "${available_current}" | depend_match_name "${name_to_match}" )"

        resolved_single_depend="$( ${method} ${name_to_match} "${matching_array}" )"

        if test -z "${resolved_single_depend}" ; then
            single_pack="$(depend_get_pack_noboundary ${single_depend})"
            single_pack="$(depend_get_pack_name ${single_pack})"
            nonresolved="${nonresolved}
${single_pack}"
        fi
    done

    printf "%s\n" "${nonresolved}"
}

depend_get_current_runtime_binary()
{
    cand_rundeps="$1"
    if test -z "${cand_rundeps}" ; then
        cand_rundeps="${RDEPEND}"
    fi
    if test -z "${cand_rundeps}" ; then
        cand_rundeps="${DEPEND}"
    fi
    packs_built_with="$(root_get_installed | tr ' ' '\n')"

    for rdepend in ${cand_rundeps} ; do

        skip_to_next=
        modificator=$(depend_detect_version_boundary ${rdepend})
        record_from_rdepend=${rdepend#${modificator}}
        name_from_rdepend=$(depend_get_pack_name ${record_from_rdepend})
        version_from_rdepend=$(depend_get_pack_version ${record_from_rdepend})
        pack_buildtime=$(printf "%s\n" "${packs_built_with}" | \
            grep -e "^${name_from_rdepend}-[0-9]" | tail -1)

        pack_rundep=
        if test -n "${pack_buildtime}" ; then
            version_buildtime="$(depend_get_pack_version ${pack_buildtime})"
            record_build=
            for record_build in $(cat "${ROOT}/pkgdb/${pack_buildtime}") ; do
                name_runtime="${record_build%:*}"
                version_runtime="${record_build#*:}"
                if test "${name_runtime}" = "${version_runtime}" ; then
                    version_runtime=
                fi

                if test "${name_runtime}" = 'NONE' ; then
                    skip_to_next=1
                    break
                fi
                if test -z "${name_runtime}" ; then
                    name_runtime="${name_from_rdepend}"
                fi
                if test -z "${version_runtime}" ; then
                    version_runtime="${version_buildtime}"
                fi
                if test -z "${modificator}" ; then
                    modificator='>='
                fi
                case ${modificator} in
                    '>='|'>')
                        pack_rundep=">=${name_runtime}-${version_runtime}"
                        ;;
                    '<='|'<')
                        pack_rundep=">=${name_runtime}-${version_runtime}
                            ${modificator}${name_runtime}-${version_from_rdepend}"
                        ;;
                    *)
                        pack_rundep="${modificator}${name_runtime}-${version_from_rdepend}"
                        ;;
                esac

                if test -n "${pack_rundep}" ; then
                    skip_to_next=1
                    printf "%s\n" "${pack_rundep}"
                fi
            done

            if test -n "${skip_to_next}" ; then
                continue
            fi
            if test -z "${modificator}" ; then
                modificator='>='
            fi
            case ${modificator} in
                '>='|'>')
                    pack_rundep=">=${pack_buildtime}"
                    ;;
                '<='|'<')
                    pack_rundep=">=${pack_buildtime}
                        ${rdepend}"
                    ;;
                *)
                    pack_rundep="${rdepend}"
                    ;;
            esac
        fi
        if test -z "${pack_rundep}" ; then
            pack_rundep="${rdepend}"
        fi

        printf "%s\n" "${pack_rundep}"

    done
}
