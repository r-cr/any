# unpack sources

fetch_local_src()
{
    mkdir -p ${WORKDIR}

    src_search=1
    for dir_src in \
        ${ANYSRCDIR}/${P} \
        ${ANYSRCDIR}/${MY_P} \
        ${DISTDIR}/${P} \
        ${DISTDIR}/${MY_P}
    do
        if test -d "${dir_src}" ; then
            cp -pRf ${dir_src} ${WORKDIR}/
            awarn "Local sources from ${dir_src} are used."
            # message to the callee to stop further source fetching
            printf "%s" "${msg_FETCH_STOP}"
            src_search=
            break
        fi
    done

    for dir_src in ${ANYSRCDIR} ${DISTDIR} ; do
        if test -z "${src_search}" ; then
            break
        fi
        for archive_src in \
            ${MY_P}.tar.xz \
            ${MY_P}.txz \
            ${MY_P}.tar.gz \
            ${MY_P}.tgz \
            ${MY_P}.tar.bz2 \
            ${MY_P}.tbz2 \
            ${MY_P}.tz2
        do
            if test -f "${dir_src}/${archive_src}" ; then
                tar xf ${dir_src}/${archive_src} -C ${WORKDIR}
                awarn "Source archive ${dir_src}/${archive_src} has been found."
                src_search=
                break
            fi
        done
        if test -f "${dir_src}/${MY_P}.zip" ; then
            archive_src="${MY_P}.zip"
            unzip "${dir_src}/${archive_src}" -d ${WORKDIR}
            awarn "Source archive ${dir_src}/${archive_src} has been found."
            src_search=
            break
        fi
    done

    if test -n "${src_search}" ; then
        awarn "Source ${MY_P} for ${P} has not been found."
    fi

    if test ! -d ${S} -a -d ${WORKDIR}/${MY_P} ; then
        mkdir -p $(dirname ${S})
        mv ${WORKDIR}/${MY_P} ${S}
    fi

    if test -d ${S} ; then
        awarn "Sources are fetched into ${S}"
    fi

    unset dir_src archive_src src_search
}

fetch_inner_version()
{
    explicit_version="$(strip_blank "${INNER_VERSION}" )"
    if test -n "${explicit_version}" ; then
        printf "%s" "${explicit_version}"
        return 0
    fi
    if test -s "${MINTNEUTRALDIR}/inner_version" ; then
        inner_candidate="$(cat ${MINTNEUTRALDIR}/inner_version 2> /dev/null || nonfail)"
    elif test -s "${DISTDIR}/inner_version" ; then
        inner_candidate="$(cat ${DISTDIR}/inner_version 2> /dev/null || nonfail)"
    elif test -s "${T}/inner_version" ; then
        inner_candidate="$(cat ${T}/inner_version 2> /dev/null || nonfail)"
    fi
    if test -z "${inner_candidate}" ; then
#       awarn "Inner version of ${P} is unavailable. Run fetch_dev or fetch_dev_inner_version to get it."
        return
    fi
    inner_candidate=$(( ${base_INNER_VERSION:-0} + ${inner_candidate} ))
    if test "${inner_candidate}" -gt 0 ; then
        printf "%s" "${inner_candidate}"
    fi
}

fetch_store_mint()
{
    if [ ! -f "${MINTNEUTRALDIR}/inner_version" -a -f "${T}/inner_version" ] ; then
        mkdir -p ${MINTNEUTRALDIR}
        cp -f "${T}/inner_version" "${MINTNEUTRALDIR}/inner_version"
    fi
}

fetch_packinstall()
{
    if [ -d ${WORKDIR}/${P} ] ; then
        mkdir -p ${ANYSRCDIR}
        tar czfp ${ANYSRCDIR}/${MY_P}.tar.gz ${P} -C ${WORKDIR}
    fi
}

fetch_build_remove()
{
    if [ "${any_builddir_keep}" != '1' ] ; then
        rmbuild
    fi
}

fetch_store_patches()
{
    if [ -d "${PATCHDIR}" ] ; then
        base_static_patchdir=$(dirname ${static_PATCHDIR})
        mkdir -p ${base_static_patchdir}
        cp -pRf ${PATCHDIR} ${base_static_patchdir}
    fi
}

fetch_clean_patches()
{
    rm -rf ${PATCHDIR}
}

# remote and dev procedures are done in lib modules

fetch_remote_src()
{
    return
}

fetch_dev_inner_version()
{
    return
}

fetch_dev()
{
    return
}

fetch_merge_patchdir()
{
    if test -n "$(ls -q ${static_PATCHDIR}/ 2>/dev/null )" ; then
        rm -rf ${PATCHDIR}
        mkdir -p ${PATCHDIR}
        cp -vRf ${static_PATCHDIR}/* ${PATCHDIR}/
    elif test -n "$(ls -q ${atom_static_PATCHDIR}/ 2>/dev/null )" ; then
        rm -rf ${PATCHDIR}
        mkdir -p ${PATCHDIR}
        cp -vRf ${atom_static_PATCHDIR}/* ${PATCHDIR}/
    fi

    if test -d "${distfiles_DIR}" ; then
        filedir="$(basename ${distfiles_DIR})"
        if test ! -d "${PATCHDIR}/${filedir}" ; then
            mkdir -p ${PATCHDIR}
            cp -vRf "${distfiles_DIR}" "${PATCHDIR}/${filedir}"
        fi
    fi
}

fetch_gen_patches()
{
    fetch_merge_patchdir
}
