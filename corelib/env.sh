# operations on general environment
# set up his majesty PATH

inner_path=
for single_inner_path in ${any_inner_paths} ; do
    inner_path=${BASEDIR}${single_inner_path}:${inner_path}
done

tracked_path=
if ! ause 'plainroot' ; then
    for single_tracked_dir in $(get_tracked_exec_dirs ${ROOT}) ; do
        tracked_path=${single_tracked_dir}:${tracked_path}
    done
fi

if test -n "${bool_SANDBOX}" ; then
    mint_PATH=${inner_path}${binany_DIR}:${any_abs_path}
else
    mint_PATH=${inner_path}${binany_DIR}:${PATH}
fi

prepend_path="${tracked_path}"
if ause 'wrapcc' ; then
    prepend_path="${prepend_path}${pack_EXEC}:${common_EXEC}:"
fi
PATH="${prepend_path}${mint_PATH}"
unset inner_path tracked_path prepend_path

# set up PKG_CONFIG_*

pkg_intermediate=
for single_pref in ${PREFIX_LIST} ; do
    pkg_intermediate=${ROOT}${single_pref}/lib/pkgconfig:${pkg_intermediate}
done

PKG_CONFIG_PATH=${pkg_intermediate%:}
unset pkg_intermediate

# all exports

export PATH PKG_CONFIG_PATH
