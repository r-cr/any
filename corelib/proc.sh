# general purpose functions for engine

relative_dir()
{
    dir_first="$1"
    dir_second="$2"

    dir_first="$(cd "${dir_first}" ; pwd -P)"
    dir_second="$(cd "${dir_second}" ; pwd -P)"

    # remove common part from both paths

    backup_IFS="${IFS}"
    IFS=/
    for first_element in ${dir_first} ; do
        if test -z "${first_element}" ; then
            continue
        fi
        temp_element="${dir_second#/${first_element}}"
        if test "${temp_element}" != "${dir_second}" ; then
            case "${temp_element}" in
                /*|'')
                    dir_second="${temp_element}"
                    delete_from_first="${delete_from_first}/${first_element}"
                ;;
                *)
                    break
                ;;
            esac
        else
            break
        fi
    done
    dir_first="${dir_first#${delete_from_first}}"

    # replace remaining part of first path with backward paths /..
    backed_path=
    for dir in ${dir_first} ; do
        if test -z "${dir}" ; then
            continue
        fi
        backed_path="${backed_path}/.."
    done
    IFS="${backup_IFS}"

    if test -n "${backed_path}" ; then
        backed_path=${backed_path#/}
    elif test -n "${dir_second}" ; then
        backed_path="."
    fi
    backed_path="${backed_path}${dir_second}"

    printf "%s\n" "${backed_path}"
}

parent_dir()
{
    input_file="$1"

    target_dir="${input_file%/*}"
    if test "${target_dir}" = "${input_file}" ; then
        # current directory
        target_dir='.'
    fi
    if test -z "${target_dir}" ; then
        # file in root directory
        target_dir='/'
    fi

    printf "%s\n" "${target_dir}"
}


# get the link to file and calculate the real file
extract_link()
{
    link_to_extract="$1"

    if test ! -h "${link_to_extract}" ; then
        return 1
    fi
    resolved_string="$(ls -ldq "${file_to_make_copy}" )"
    target_file="${resolved_string#* -> }"
    # possible quotes
    target_file="${target_file#'}"
    target_file="${target_file%'}"
    # resolved string can be relative to link
    if test "${target_file}" = "${target_file#/}" ; then
        # path is not absolute
        # construct the full destination
        dir_with_link="$(parent_dir "${file_to_make_copy}" )"
        target_file="${dir_with_link}/${target_file}"
    fi

    printf "%s\n" "${target_file}"
}

unlink_and_copy()
{
    while read file_to_make_copy <&0 ; do
        target_file="$(extract_link "${file_to_make_copy}" )"  || continue
        rm -f "${file_to_make_copy}" || nonfail
        cp -LRf "${target_file}" "${file_to_make_copy}"
        printf "%s\n" "${file_to_make_copy}"
    done
}

edit_by_rules()
{
    while read file_to_edit <&0 ; do
        sed -r -f ${T}/${edit_RULES_FILE} "${file_to_edit}" > "${file_to_edit}.tmp"
        mv "${file_to_edit}.tmp" "${file_to_edit}"
        printf "%s\n" "${file_to_edit}"
    done
}

get_plain_tracked_dirs()
{
    for pref in ${PREFIX_LIST} ; do
        if [ ${pref} = '/' ] ; then
            continue
        fi
        for tracked_dir in ${root_TRACKED_DIRS} ; do
            printf "%s\n" "${pref}/${tracked_dir}"
        done
    done
}

get_installed_tracked_dirs()
{
    if test -n "${1}" ; then
        base_list="${1}"
    else
        fallback="$(pack_fallback_allowed)"

        for binname in $(pack_name) ; do
            bindir=$(pack_dir_for_runtime_entry "${binname}" "${fallback}" )
            if test -d "${bindir}" ; then
                base_list="${base_list} ${bindir}"
            fi
        done
    fi

    for base in ${base_list} ; do
        for pref in ${PREFIX_LIST} ; do
            tracked_dir=
            for tracked_dir in ${root_TRACKED_DIRS} ; do
                if [ -d "${base}${pref}/${tracked_dir}" ] ; then
                    printf "%s\n" "${base}${pref}/${tracked_dir}"
                fi
            done
            if [ -z "${tracked_dir}" ] ; then
                if [ -d "${base}${pref}" ] ; then
                    printf "%s\n" "${base}${pref}"
                fi
            fi
        done
    done
}

get_tracked_exec_dirs()
{
    if test -n "${1}" ; then
        base_list="${1}"
    else
        fallback="$(pack_fallback_allowed)"

        for binname in $(pack_name) ; do
            bindir=$(pack_dir_for_runtime_entry "${binname}" "${fallback}" )
            if test -d "${bindir}" ; then
                base_list="${base_list} ${bindir}"
            fi
        done
    fi

    for base in ${base_list} ; do
        for pref in ${PREFIX_LIST} ; do
            tracked_dir=
            local_pref=${pref}
            if [ ! ${pref} = '/' ] ; then
                local_pref="${local_pref}/"
            fi
            for tracked_dir in ${root_TRACKED_BIN_DIRS} ; do
                printf "%s\n" "${base}${local_pref}${tracked_dir}"
            done
        done
    done
}


get_installed_tracked_exec_dirs()
{
    if test -n "${1}" ; then
        base_list="${1}"
    else
        fallback="$(pack_fallback_allowed)"

        for binname in $(pack_name) ; do
            bindir=$(pack_dir_for_runtime_entry "${binname}" "${fallback}" )
            if test -d "${bindir}" ; then
                base_list="${base_list} ${bindir}"
            fi
        done
    fi

    for base in ${base_list} ; do
        for pref in ${PREFIX_LIST} ; do
            tracked_dir=
            for tracked_dir in ${root_TRACKED_BIN_DIRS} ; do
                if [ -d "${base}${pref}/${tracked_dir}" ] ; then
                    printf "%s\n" "${base}${pref}/${tracked_dir}"
                fi
            done
        done
    done
}

get_installed_tracked_exec_files()
{

    where_to_find=
    for exec_dir in $(get_installed_tracked_exec_dirs) ; do
        where_to_find="${where_to_find} ${exec_dir}/"
    done
    if [ -z "${where_to_find}" ] ; then
        return 0
    fi

    (
    cd ${empty_DIR}

    find ${where_to_find} \
        \( -type f -o -type l \) \
        $(filter_executable_name_template)  \
        | filter_text_executable_files
    )
}

link_file()
{
    local from_file="${1}"
    local dest_file="${2}"

    if test ! -f "${from_file}" -a ! -h "${from_file}" ; then
        awarn "File ${from_file} to create link does not exist"
        return 0
    fi
    local from_dir="$( parent_dir "${from_file}" )"
    local dest_dir="$( parent_dir "${dest_file}" )"

    local back_dir="$(relative_dir "${dest_dir}" "${from_dir}" )"
    local from_file_only="${from_file##*/}"
    ln -sf "${back_dir}/${from_file_only}" "${dest_file}"
}

link_dir()
{
    local from_dir="${1}"
    local dest_dir="${2}"
    local file_extension="${3}"

    if test ! -d "${from_dir}" -a ! -h "${from_dir}" ; then
        awarn "Directory ${from_dir} to create link does not exist."
        return 0
    fi
    mkdir -p "${dest_dir}"
    # link_file works correctly only with absolute paths
    from_dir="$(cd ${from_dir} ; pwd -P)"
    dest_dir="$(cd ${dest_dir} ; pwd -P)"
    if test ! -d "${dest_dir}" ; then
        awarn "Directory ${dest_dir} for links has not been created."
        return 2
    fi

    find "${from_dir}" -type d | while read single_from_dir ; do
        from_subdir="${single_from_dir#${from_dir}}"
        single_dest_dir="${dest_dir}${from_subdir}"
        mkdir -p "${single_dest_dir}"
    done
    find "${from_dir}/" -type f -name "*${file_extension}" | while read from_file ; do
        from_subfile="${from_file#${from_dir}/}"
        dest_file="${dest_dir}/${from_subfile}"
        link_file "${from_file}" "${dest_file}"
    done
    find "${from_dir}/" -type l | while read from_file ; do
        from_subfile="${from_file#${from_dir}/}"
        dest_file="${dest_dir}/${from_subfile}"
        link_file "${from_file}" "${dest_file}"
    done
}

save_trace_state()
{
    trace_STATE="$(set +o | grep xtrace)"
}

save_trace_and_hide_verbose()
{
    trace_STATE="$(set +o | grep xtrace)"
    set +o xtrace
}

save_trace_and_set_verbose()
{
    trace_STATE="$(set +o | grep xtrace)"
    set -o xtrace
}

restore_trace_state()
{
    ${trace_STATE}
}

# method to load shell code for non-current atom
# it will initialise all engine variables, providing access to the data of another
# objects to current environment, such as dependencies.
# as method changes current shell context, it must be called inside sub-shell 
# to continue normal work with current atom afterwards.

# ---- dump reading methods ----

# when plain read method is used, data from atom.conf of other object is ignored, 
# as full-pledged rdd is not called to access it.
# therefore it is crucially important not to access any data, influenced by atom.conf,
# from load_dump* methods, using "plain read" strategy.

# heavy read method is the most universal and exact ; the most slow also.
# besides speed, generated scripts cannot be launched anymore autonomously from rdd.

# crooked read method allows full atom.conf usage in autonomous mode. 
# It requires pre-generation of all used atoms' dumps beforehand.
# It also requires single profile combination for all atoms.

load_dump_for_pack()
{
    if [ "${any_bool_loaddump}" = '1' ] ; then
        die "Recursion in dump load detected"
    fi
    any_bool_loaddump='1'

    if [ "${any_engine_dump}" = "heavy" ] ; then
        load_dump_heavy_read ${1}
    elif [ "${any_engine_dump}" = "crooked" ] ; then
        load_dump_crooked_read ${1}
    else
        load_dump_plain_read ${1}
    fi
}

load_dump_plain_read()
{
    name=$1

    if [ ! -f "${DUMP}" ] ; then
        die "Language dump required when call was dumpless"
    fi

    mkdir -p ${T}
    sed \
        -e "s/rdd_atom_id=.*/rdd_atom_id=\"${name}\"/" \
        -e '/rdd_prf_all/s%\"$% loaddump\"%' \
        ${DUMP} > ${T}/${name}.dump.sh
    . ${T}/${name}.dump.sh
}

load_dump_heavy_read()
{
    name=$1

    mkdir -p ${T}
    dumprdd ${rdd_prf_entry},loaddump ${name} > ${T}/${name}.dump.sh
    . ${T}/${name}.dump.sh
}

load_dump_crooked_read()
{
    name=$1
    
    neighbour_dump=$(printf "%s" ${DUMP} | sed -e "s%${P}%${name}%" )
    if [ ! -f "${neighbour_dump}" ] ; then
        die "Using shell dump of neighbours and dumps aren't found"
    fi
    mkdir -p ${T}
    sed \
        -e '/rdd_prf_all/s%\"$% loaddump\"%' \
        ${neighbour_dump} > ${T}/${name}.dump.sh

    . ${T}/${name}.dump.sh
}

load_dump()
{
    status_set_ok
    load_dump_for_pack "$1"
    keywords_check
    status_check
}

load_env_nocheck()
{
    name="$1"

    any_bool_loaddump='1'
    rdd_atom_id="$name"
    . ${libcore_DIR}/unsetapi.sh
    . ${libcore_DIR}/api.sh
}

# method to load variables from another atom without dump file
load_env()
{
    name="$1"

    if test "${any_bool_loaddump}" = '1' ; then
        die "Recursion in env load detected"
    fi
    status_set_ok

    load_env_nocheck "${name}"

    keywords_check
    status_check
}

get_first_word_from_list()
{
    list="$1"
    for word in ${list} ; do
        printf "%s" "${word}"
        return
    done
}

get_first_string()
{
    list="$1"
    printf "%s" "$list" | head -1
}

delete_first_string()
{
    list="$1"
    remove_string="$2"

    if test -z "$remove_string" ; then
        remove_string="$(get_first_string "$list" )"
    fi
    todelete="${remove_string}
"
    printf "%s" "${list#${todelete}}"
}

word_in_list()
{
    list="$1"
    word="$2"

    checker_word="${list%%${word}*}"
    if test "${list}" != "${checker_word}" ; then
        return 0
    fi
    return 1
}

delete_word_from_list()
{
    list="$1"
    word="$2"

    printf "%s\n" "${list}" | sed \
            -e "s/^${word}[ ][ ]*//g" \
            -e "s/ ${word}[ ][ ]*/ /g" \
            -e "s/ ${word}\$//g" \
            -e "s/^${word}\$//"
}

hash_atom_name()
{
    atom="$1"

    postfix="${atom#[a-zA-Z0-9]}"
    hash="${atom%${postfix}}"

    printf "%s" "${hash}/${atom}"
}

generate_pkg_listing_single()
{
    (
    cd "${1}"

    find . | sort | sed -e 's/^\.//' -e '/^$/d'
    )
}

generate_pkg_listing()
{
    if test -n "$1" ; then
        if test -d "$1" ; then
            packdir="$1"
        else
            packdir="${IMGDIR}/$1"
        fi
        if test ! -d "${packdir}" ; then
            awarn "No package in ${packdir} found"
            return 1
        fi
        generate_pkg_listing_single "${packdir}"
    else
        fallback="$(pack_fallback_allowed)"
        for binpack in $(pack_name) ; do
            bindir="$(pack_dir_for_runtime_entry "${binpack}" "${fallback}" )"
            generate_pkg_listing_single "${bindir}"
        done
    fi

}

is_text_good()
{
    text="$1"
    (
    LC_CTYPE=C
    case "${text}" in
        [:alnum:_]*)
            return 0
        ;;
        *)
            return 1
        ;;
    esac
    ) || return $?

    return 0
}

strip_blank()
{
    string="$1"

    remove="${string##[[:blank:]
]}"
    while test "${string}" != "${remove}" ; do
        string="${remove}"
        remove="${string##[[:blank:]
]}"
    done

    remove="${string%%[[:blank:]
]}"
    while test "${string}" != "${remove}" ; do
        string="${remove}"
        remove="${string%%[[:blank:]
]}"
    done

    printf "%s\n" "${string}"
}

get_field_text()
{
    string="$1"
    numeric="$2"

    text=
    while test -n "${string}" ; do
        if test -n "${numeric}" ; then
            remove="${string#[[:digit:]]}"
        else
            remove="${string#[^[:digit:]]}"
        fi
        char="${string%${remove}}"
        if test -n "${char}" ; then
            text="${text}${char}"
            string="${string#${char}}"
        else
            break
        fi
    done

    if test -n "${text}" ; then
        printf "%s\n" "${text}"
    fi
}

compare_versions()
{
    old="$1"
    new="$2"

    if test "${old}" = "${new}" ; then
        status='eq'
        printf "%s\n" "${status}"
        return 0
    fi

    status='gt'
    while test -n "${old}" -a -n "${new}" ; do

        # process numeric part of version
        digits_old="$(get_field_text "${old}" 'num')"
        digits_new="$(get_field_text "${new}" 'num')"

        if test -n "${digits_old}" -a -z "${digits_new}" ; then
            status='lt'
            break
        fi
        if test -z "${digits_old}" -a -n "${digits_new}" ; then
            status='gt'
            break

        fi
        if test -n "${digits_old}" -a -n "${digits_new}" ; then

            if test "${digits_new}" -ne "${digits_old}" ; then
                if test "${digits_new}" -gt "${digits_old}" ; then
                    status='gt'
                else
                    status='lt'
                fi
                break
            fi

            old="${old#${digits_old}}"
            new="${new#${digits_new}}"
            # now numeric part has been removed
        fi
        # process textual part of version
        text_old="$(get_field_text "${old}")"
        text_new="$(get_field_text "${new}")"

        if test -n "${text_old}" -a -z "${text_new}" ; then
            status='lt'
            break
        fi
        if test -z "${text_old}" -a -n "${text_new}" ; then
            status='gt'
            break
        fi
        if test -n "${text_old}" -a -n "${text_new}" ; then

            if test "${text_new}" '!=' "${text_old}" ; then
                if test "${text_new}" '>' "${text_old}" ; then
                    status='gt'
                else
                    status='lt'
                fi
                break
            fi

            old="${old#${text_old}}"
            new="${new#${text_new}}"
            # now text part has been removed
        fi

    done

    printf "%s\n" "${status}"
}

arith_greater_then()
{
    status="$(compare_versions "$1" "$2" )"
    if test "${status}" = "gt" ; then
        return 0
    fi
    return 1
}

arith_greater_equal()
{
    status="$(compare_versions "$1" "$2" )"
    if test "${status}" = "gt" -o "${status}" = "eq" ; then
        return 0
    fi
    return 1
}

arith_equal()
{
    status="$(compare_versions "$1" "$2" )"
    if test "${status}" = "eq" ; then
        return 0
    fi
    return 1
}

arith_lower_then()
{
    status="$(compare_versions "$1" "$2" )"
    if test "${status}" = "lt" ; then
        return 0
    fi
    return 1
}

arith_lower_equal()
{
    status="$(compare_versions "$1" "$2" )"
    if test "${status}" = "lt" -o "${status}" = "eq" ; then
        return 0
    fi
    return 1
}

rmdir_nested()
{
    toremove="$1"
    if test -d "${toremove}" ; then
        toremove="$(cd ${toremove} ; pwd -P)"
    else
        return 1
    fi

    while test -d "${toremove}" ; do
        rmdir "${toremove}" 2> /dev/null || break
        toremove="${toremove%/*}"
    done
}

local_which()
{
    arg="$1"

    if test -x "${arg}" ; then
        printf "%s\n" "${arg}"
        return 0
    fi

    IFS=:
    for dir in ${PATH} ; do
        if test -x "${dir}/${arg}" ; then
            printf "%s\n" "${dir}/${arg}"
            return 0
        fi
    done
    return 1
}
