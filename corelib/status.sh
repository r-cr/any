# functions to summarise results

status_set_ok()
{
    status_GLOBAL='OK'
    export status_GLOBAL
    status_COLOR="${color_green}"
}
status_set_fail()
{
    status_GLOBAL='FAIL'
    export status_GLOBAL
    status_COLOR="${color_red}"
}
status_set_skip()
{
    status_GLOBAL='SKIP'
    export status_GLOBAL
    status_COLOR="${color_yellow}"
}

status_time_start()
{
    export time_BEGIN=$(date +%s)
    printf "%s" "${time_BEGIN}" > ${T}/timestamp
}

status_time_catch()
{
    time_END=$(date +%s)
    if [ -z "${time_BEGIN}" ] ; then
        time_BEGIN=$(cat ${T}/timestamp || nonfail)
    fi
    if [ ! -z "${time_BEGIN}" ] && [ ${time_END} -ge ${time_BEGIN} ] ; then
        time_TOTAL=$(( ${time_END} - ${time_BEGIN} ))
    else
        time_TOTAL='NULL'
    fi
    unset time_END
    unset time_BEGIN
    export time_TOTAL

    return 0
}

status_terminate()
{
    set +o errexit
    ( return ${rdd_exit_status} )
    status_sum
}

status_sum()
{
    status=$?
    
    if [ ${status} = '0' ] ; then
        status_set_ok
    elif [ ${status} = '13' ] ; then
        status_set_skip
    else
        status_set_fail
    fi

    status_time_catch
}

status_check()
{
    if [ "${status_GLOBAL}" = 'FAIL' ] ; then
        die
    fi
    if [ "${status_GLOBAL}" = 'SKIP' ] ; then
        skip
    fi
}

status_bool()
{
    if test "${status_GLOBAL}" = 'FAIL' ; then
        return 1
    fi
    if test "${status_GLOBAL}" = 'SKIP' ; then
        return 1
    fi

    return 0
}
