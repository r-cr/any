prepare_tmp()
{
    mkdir -p "${T}" "${empty_DIR}"
}

prepare_clean_tmp()
{
    rm -rf "${T}"
}

prepare_clean_tmp_timestamp()
{
    if test -f "${T}/timestamp" ; then
        mkdir -p "${T}.side"
        mv "${T}/timestamp" "${T}.side/"
    fi
    rm -rf "${T}"
    if test -d "${T}.side" ; then
        mv "${T}.side" "${T}"
    fi
}

prepare_log()
{
    mkdir -p "${LOGDIR}"
    link_file "${log_OUTER}" "${LOG}"
}

prepare_dump_env()
{
    rm -f "${ENVDUMP}" 2> /dev/null || nonfail
    set | grep -e '=' | \
        sed \
        -e "s/'//g" \
        -e "s/=/='/" \
        -e "s/\$/'/g" | \
        sort > "${ENVDUMP}"
}

prepare_all()
{
    prepare_tmp
    status_time_start
    prepare_log
#    prepare_dump_env
}

prepare_wrap()
{
    return
}
