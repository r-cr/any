
src_prefetch()
{
    return
}

src_postfetch()
{
    return
}

src_fetch_extend()
{
    return
}

src_fetch_extend_pre()
{
    return
}

src_fetch()
{
    src_prefetch

    save_trace_and_hide_verbose

    src_fetch_extend_pre

    fetch_build_remove
    fetch_remote_src
    local_status="$( fetch_local_src )"

    if [ "${local_status}" != "${msg_FETCH_STOP}" ] ; then

        fetch_dev
        fetch_gen_patches

    fi
    unset local_status

    tobuild

    src_fetch_extend
    restore_trace_state

    src_postfetch
}

src_postprepare()
{
    return
}
src_prepare_extend()
{
    return
}
src_prepare_extend_pre()
{
    return
}

src_prepare()
{
    save_trace_and_hide_verbose

    src_prepare_extend_pre

    patches_all
    patches_files
    patches_auto

    src_prepare_extend
    restore_trace_state

    src_postprepare
}

src_config()
{
    return
}

src_compile()
{
    return
}
src_install()
{
    return
}

src_prestore()
{
    return
}

src_poststore()
{
    return
}

src_store()
{
    src_prestore

    fetch_store_mint
    fetch_store_patches
    fetch_packinstall

    src_poststore
}
