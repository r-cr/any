# standard map functions with the sequence of build phases

map()
{
    set -o xtrace

    pkg_pretend
    pkg_setup
    pkg_context

    src_fetch
    src_prepare

    src_config
    src_compile
    pkg_rminstall
    src_install

    pkg_install
    pkg_config
    pkg_root
    pkg_pack

    set +o xtrace
}

map_autopre()
{
    prepare_all
    print_atom
    print_log_header
    keywords_check
    status_check
    tobuild
}

map_autopost()
{
    status_sum
    print_final_atom
}

map_postmortem()
{
    status_terminate
    print_final_atom
    print_path_log
}

map_group()
{
    (

    admin_host || nonfail
    print_session

    ) >& ${log_TERMINAL} 2>& ${log_TERMINAL}
}

map_direct()
{
    if directbuild ; then
        map
    fi
}
