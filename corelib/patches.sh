# to patch the sources

patches_all()
{
    current_cycle=
    if test -d "${PATCHDIR}" ; then

        if test -n "${PATCHLIST}" ; then
            current_cycle="${PATCHLIST}"
        else
            current_cycle="$(patchlist)"
        fi

        for single_patch in ${current_cycle} ; do
            if test -f "${PATCHDIR}/${single_patch}" ; then
                dopatch ${PATCHDIR}/${single_patch}
            fi
        done
    fi
}

patches_files()
{
    if test -n "$(ls -q ${ANYDEVSRCDIR}/${P}/ 2>/dev/null )" && \
            test ! -d "${PATCHDIR}/" ; then

        mkdir -p ${S}
        # caveat: no hidden files from root src directory to build directory
        cp -Rf ${ANYDEVSRCDIR}/${P}/* ${S}/
    fi

    if test -n "$(ls -q ${PATCHDIR}/files/ 2>/dev/null )" ; then
        mkdir -p ${S}
        cp -Rf ${PATCHDIR}/files/* ${S}/
    fi
}

patches_auto()
{
    return
}
