# to work with set of installed packages

image_clean()
{

    if test -d "${D}" ; then
        rm -rf "${D}"
    fi
}

image_create_rules()
{
    mkdir -p ${T}
    : > ${T}/${edit_RULES_FILE}
    printf "%s\n" "s#(${ROOT})+##g" >> ${T}/${edit_RULES_FILE}
}

image_setup_paths()
{
    alog "Preparing files for native package"

    image_create_rules

    find ${D} -type f | grep -v -e "\.h$" | filter_text_files | edit_by_rules
}

image_split_pack()
{
    fallback="$(pack_fallback_allowed)"
    if test -n "${fallback}" ; then
        return
    fi
    install_source="install"
    if test "${any_split_pack}" = 'copy' ; then
        split_pack="cp -Rf"
    else
        split_pack="mv -f"
    fi

    for name in $(pack_name_base) ; do
        binname="$(pack_name_from_shortname ${name})"
        installfile=$(pack_metafile_for_runtime_entry "packdeb/${name}" "${install_source}" "${fallback}")
        if test -z "${installfile}" ; then
            installfile=$(pack_metafile_for_runtime_entry "${name}" "${install_source}" "${fallback}")
        fi
        if test -f "${installfile}" ; then
            bindir="${IMGDIR}/${binname}"
            if test "${bindir}" = "${D}" ; then
                continue
            fi
            rm -rf "${bindir}"
            mkdir -p "${bindir}"
            while read line ; do
                content="${D}/${line}"
                dircontent="${content%/*}"
                dirs_to_create="$(
                eval "ls -1dq ${dircontent}" | while read contline ; do
                    targetline="${contline#${D}}"
                    targetline="${bindir}${targetline}"
                    printf "%s\n" "${targetline}"
                done
                )"
                mkdir -p "${dirs_to_create}"

                eval "ls -1dq ${content}" | while read contline ; do
                    targetline="${contline#${D}}"
                    targetline="${bindir}${targetline}"
                    ${split_pack} "${contline}" "${targetline}"
                done
            done < ${installfile}
        fi
    done
}

image_strip_set()
{
    names="$1"
    stripoptions="$2"

    if test -z "${names}" ; then
        return
    fi

    nonwritable="$(find ${names} -perm -u-w)"
    if test -n "${nonwritable}" ; then
        chmod u+w ${nonwritable}
    fi

    "${strip_IMAGE}" ${stripoptions} ${names}

    if test -n "${nonwritable}" ; then
        chmod u-w ${nonwritable}
    fi
}

image_strip_bin()
{
    if test -z "${STRIP}" -a -n "${AR}" ; then
        awarn "Looks like non-default toolchain is used (AR variable is initialised).
But variable \${STRIP} is not initialised.
That can lead to incorrect work of strip and possible failures of the build."
    fi
    strip_IMAGE="${STRIP:-strip}"
    libdirs=
    execdirs=
    for pref in ${PREFIX_LIST} ; do
        for single_dir in ${image_TRACKED_LIB_DIRS} ; do
            if test -d "${D}${pref}/${single_dir}" ; then
                libdirs="${libdirs} ${D}${pref}/${single_dir}"
            fi
        done
        for single_dir in ${image_TRACKED_EXEC_DIRS} ; do
            if test -d "${D}${pref}/${single_dir}" ; then
                execdirs="${execdirs} ${D}${pref}/${single_dir}"
            fi
        done
    done

    names="$(binfiles -s ${libdirs})"
    image_strip_set "${names}" '--strip-debug'
    names="$(binfiles -d ${libdirs})"
    image_strip_set "${names}" '--strip-unneeded'
    names="$(binfiles ${execdirs})"
    image_strip_set "${names}" '--strip-all'
}

image_remove_files()
{
    local path="$1"
    local obj="$2"

    local query="$(
    printf "%s\n" "${obj}" | while read objline ; do
        if test -n "${objline}" ; then
            printf "%s\n" " -o -name ${objline}"
        fi
    done
    )"
    query="${query# -o }"

    eval
    (
    cd ${empty_DIR}
    find "${path}" \
            ${query} \
            -type f ) | \
            while read rmname ; do
                rm -f "${rmname}"
                awarn "File ${rmname} is removed"
                dir_to_clean="${rmname%/*}"
                rmdir_nested "${dir_to_clean}" || nonfail
            done
}

image_remove_la()
{
    image_remove_files "${D}" "*.la"
}

# remove files, given by config option
image_remove_from_config()
{
    toremove="${any_image_remove}"

    origin_IFS="${IFS}"
    IFS=,
    remove_list=
    for element in ${toremove} ; do
        remove_list="${remove_list}
${element}
"
    done
    IFS="${origin_IFS}"

    image_remove_files "${D}" "${remove_list}"
}
