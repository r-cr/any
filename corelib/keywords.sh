keywords_check()
{
    negative_words=
    excluding=
    allowed_words=
    for word in ${KEYWORDS} ; do
        maskword="${word#'~'}" || nonfail
        if test "${maskword}" != "${word}" ; then
            negative_words="${negative_words} ${maskword}"
        else
            excluding='1'
            allowed_words="${allowed_words} ${word}"
        fi
    done
    
    finally_allowed='1'
    if test -n "${excluding}" ; then
        finally_allowed=
        for good_word in ${allowed_words} ; do
            for myword in ${AUSE} ; do
                if test "${good_word}" = "${myword}" ; then
                    finally_allowed='1'
                    break
                fi
            done
            case ${good_word} in
                "${ANYOS}"|"${ANYARCH}"|"${ANYARCHMODE}"|"${ANYARCHCAP}"|"${ANYARCHENDIAN}")
                    finally_allowed='1'
                ;;
            esac
            if test "${finally_allowed}" = '1' ; then
                break
            fi
        done
        if test "${ANYOS}" = "all" ; then
            finally_allowed='1'
        fi
        if test "${ANYARCH}" = "all" ; then
            finally_allowed='1'
        fi
        if test "${ANYARCHMODE}" = "all" ; then
            finally_allowed='1'
        fi
    fi
    if test -n "${finally_allowed}" ; then
        for bad_word in ${negative_words} ; do
            for myword in ${AUSE} ; do
                if test "${bad_word}" = "${myword}" ; then
                    finally_allowed=
                    break
                fi
            done
            if test "${finally_allowed}" != '1' ; then
                break
            fi
        done
    fi
    
    if test "${finally_allowed}" != '1' ; then
        status_set_skip
    fi
    
    unset negative_words excluding allowed_words maskword word good_word bad_word finally_allowed
}
