# methods to filter text

filter_text_files()
{
    mkdir -p ${T}
    : > ${T}/text_match_template
    for match_word in ${any_root_exec_editable} ; do
        printf "%s\n" "${match_word}" >> ${T}/text_match_template
    done
    while read file_to_check <&0 ; do
        output=$( file -b --mime-type ${file_to_check} 2>/dev/null \
                | grep -f ${T}/text_match_template || nonfail )
        if [ -n "${output}" ] ; then
            printf "%s\n" "${file_to_check}"
        fi
    done
}

# use as alias for generic filter_text_files
filter_text_executable_files()
{
    while read file_to_check <&0 ; do
        printf "%s\n" "${file_to_check}" | filter_text_files
    done
}

filter_executable_name_template()
{
    if [ -n "${any_root_exec_name_template}" ] ; then
        printf "%s" " -a ( "
        first_word='yes'
        for template_word in ${any_root_exec_name_template} ; do
            if [ -z "${first_word}" ] ; then
                printf "%s" " -o "
            fi
            printf "%s" "-name ${template_word}"
            first_word=
        done
        printf "%s" " ) "
    fi
}
