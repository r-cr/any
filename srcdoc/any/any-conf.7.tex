\documentclass{article}
\author{Random Crew}
\usepackage[utf8]{inputenc}
\usepackage{latex2man}
\usepackage{devdoc}

\setVersion{0.16}
\begin{document}

\begin{Name}{7}{any-conf}{Random Crew}{any manuals}{any-conf reference page}

    \Prog{any-conf}
    – set of configuration options of \Cmd{any}{1} engine.

\section{Description}
    \Prog{any-conf} describes all options, used by \Cmd{any}{1} for configuration of build
    at user-level, i.e. without code writing.
    Options are stored in configuration files.
    \Cmd{rdd}{1} extracts them, converts to shell variables and creates executable scripts.
    Scripts are resided in \File{var/dump/}.
    Variables, converted from options, initialise all anybuild-oriented variables from API,
    such as \Envar{ROOT}, \Envar{D} or \Envar{DEPEND}.

    To view configuration data without building, \Cmd{datardd}{1} may be used.
    \begin{verbatim}
    datardd amd64
    \end{verbatim}

    To view constructed executable shell script (or \texttt{dump}) with all input data prepared,
    \Cmd{dumprdd}{1} may be used.
    \begin{verbatim}
    dumprdd amd64
    \end{verbatim}

    Path to constructed executable script for current package is available inside shell code
    in variable \Envar{DUMP}.

\section{Options}
    All paths, stored in options, are relative to the working directory.
    The engine prepends them with needed base so user variables contain absolute paths.
    So for directory \File{/home/user/work/} and absolute path \File{/home/user/work/build/src/}
    the option value in config files would be \File{build/src}. The exception are:
    \begin{itemize}
        \item[ ] \Opt{any\_base\_dir} and \Opt{any\_rdd\_root}, which are properly initialised
            with \Cmd{rdd}{1} variable;
        \item[ ] \Opt{any\_abs\_path}, which contains set of predefined known to work absolute paths.
    \end{itemize}

    Options \Opt{any\_root\_*}, \Opt{any\_engine\_*} are engine related and are much more obscure,
    then the rest ones. Their description is separated to own subsection.
    That options are unlikely needed for general user.
    Refer to \Cmd{image\_setup\_paths}{3} and \Cmd{pkg\_install}{3} for in-depth description
    of \Opt{any\_root\_*} area.

    \begin{description}
        \item[\Opt{any\_abs\_path}]
            \Envar{PATH} inside containerised build environment is reset to predefined value.
            This option keeps the start entry for that value. Resulting path is expanded
            with several more entries, \Opt{any\_inner\_path} keeps them.
            Resetting is done to ensure needed \Envar{PATH} is at the state as expected
            by sandbox host environment.\\
            When environment is considered as non-containered
            (automatically or explicitly with the option \Opt{any\_bool\_sandbox}\Arg{=0}),
            \Opt{any\_abs\_path} is not used.\\
            Default value is: \Arg{/usr/bin:/bin:/usr/sbin:/sbin}

        \item[\Opt{any\_admin\_groupid}, \Opt{any\_admin\_userid}]
            Primary group id and user id, respectively.
            That is used in containerised environment when switching between general and privileged user.

        \item[\Opt{any\_arch}]
            Detailed cpu architecture for binaries.
            Possible examples are \Arg{armv7}, \Arg{power6}, \Arg{ultrasparc3}.\\
            Initialises variable \Envar{ANYARCH}.

        \item[\Opt{any\_arch\_capacity}]
            Capacity of binaries: \Arg{32} or \Arg{64}.\\
            Initialises variable \Envar{ANYARCHCAP}.

        \item[\Opt{any\_arch\_flags}]
            Architecture-dependent flags for compiler at compiling stage to generate correct code
            for chosen architecture.\\
            Initialises variable \Envar{ARCH\_FLAGS}.

        \item[\Opt{any\_arch\_endian}]
            Endianness of binaries: \Arg{little} or \Arg{big}.\\
            Initialises variable \Envar{ANYARCHENDIAN}.

        \item[\Opt{any\_arch\_family}]
            Generalised binary architecture. Possible examples are \Arg{arm}, \Arg{power}, \Arg{sparc}.\\
            Initialises variable \Envar{ANYARCHMODE}.

        \item[\Opt{any\_arch\_ldflags}]
            Architecture-dependent flags for compiler at linking stage to generate correct code
            for chosen architecture.\\
            Initialises variable \Envar{ARCH\_LDFLAGS}.

        \item[\Opt{any\_base\_dir}]
            Option to contain the root directory, where engine is extracted.
            Current implementation relies on the value of \File{/} for containerised environment.\\
            Initialises variable \Envar{BASEDIR}.

        \item[\Opt{any\_base\_inner\_version}]
            Integer positive number, which is added to inner version of a package,
            when it is calculated automatically inside \Cmd{fetch\_dev}{3}.
            Used to rise the inner versions centrally, but with remaining dynamic part.\\
            Initialises variable \Envar{base\_INNER\_VERSION}.

        \item[\Opt{any\_bool\_autobpi}]
            If set to \Arg{1}, variable \Envar{BPI} is calculated by first two major numbers of the version.
            For example, for version \Arg{1.2.3} the value of \Envar{BPI} will be \Arg{1.2}.\\
            Unset by default.\\
            Initialises variable \Envar{BPI}.

        \item[\Opt{any\_bool\_loaddump}]
            Option is generated inside engine unlike all others.
            If set to \Arg{1}, option marks, that current process is launched from the shell dump.
            That means further loading dumps of other packages is forbidden to avoid recursion.
            It can be used as command line option as well to forbid nested dump loads.

        \item[\Opt{any\_bool\_sandbox}]
            If set to \Arg{1}, environment is considered as sandboxed.
            If set otherwise, it is considered the opposite.
            If it is not set, container status is defined automatically.\\
            Initialises variable \Envar{base\_SANDBOX}.

        \item[\Opt{any\_builddir\_keep}]
            If set to \Arg{1}, interface \Cmd{fetch\_build\_remove}{3} will not remove build directory.

        \item[\Opt{any\_context\_save}]
            If set to \Arg{0}, interface \Cmd{root\_save\_context}{3} will not save a tar file
            with the context, provided by the current package for other packages. Unset by default.

        \item[\Opt{any\_dev\_src\_dir}]
            Location of development sources, or sources with changes for the package.
            Such sources are expected to be kept in version control system (git, mercury, cvs)
            and engine module is required for its support.\\
            Initialises variable \Envar{ANYDEVSRCDIR}.

        \item[\Opt{any\_dev\_src\_version}]
            Space separated list of possible versions for development sources to use.
            If the version in remote repository exists as given in option entry, it is used.
            If not, check is repeated for next entry.

        \item[\Opt{any\_dump}]
            Explicit location of dump file (constructed shell script) with build context.
            The option is not set by default.
            When it is empty, dump location is calculated, basing on \Opt{rdd\_atom\_dumpdir} variable.\\
            Initialises variable \Envar{DUMP}.

        \item[\Opt{any\_host\_arch}]
            Architecture of the host platform.\\
            Initialises variable \Envar{HOSTARCH}.

        \item[\Opt{any\_host\_update}]
            If not set, stops the work of \Prog{admin\_host} interface, so host environment
            will not be modified automatically during the build. Unset by default.

        \item[\Opt{any\_image\_exec}]
            Holds directories with executable files inside packages.
            Directories are relative to values from \Opt{any\_program\_prefix}.
            Initialises variable \Envar{image\_TRACKED\_EXEC\_DIRS}.
            Default value is: \Arg{bin sbin}

        \item[\Opt{any\_image\_libs}]
            Holds directories with library files inside packages.
            Directories are relative to values from \Opt{any\_program\_prefix}.
            Initialises variable \Envar{image\_TRACKED\_LIB\_DIRS}.
            Default value is: \Arg{lib}

        \item[\Opt{any\_image\_remove}]
            Holds files to remove automatically from binary packages.
            Filename templates are supported.
            File sets are separated with comma, for example:
            \begin{verbatim}
                any_image_remove = *.la, *.lo, .packlist
            \end{verbatim}
            Default value is: \Arg{*.la}.

        \item[\Opt{any\_inner\_paths}]
            Space separated list of relative paths, which are prepended to \Envar{PATH} variable
            for the build environment (both in container or not).\\
            Default value is: \Arg{rrr/bin}

        \item[\Opt{any\_list\_all}]
            List of all packages, available for the build.
            It is used by methods, which gather build tree by dependencies.
            Assigned with \Arg{all.src} by default, like \Opt{rdd\_list\_entry}.
            If option is not set, the value of \Opt{rdd\_list\_entry} is taken.

        \item[\Opt{any\_list\_required}]
            Option provides the opportunity to define explicitly set of packages,
            which needs to operate with.
            The set is independent from current value of \Opt{rdd\_list\_entry}.
            Format is the same as in \Opt{rdd\_list\_entry} and \Cmd{lsrdd}{1}:
            whether file with a list or direct string with packages, separated by comma.\\
            Unset by default. If it is unset, \Opt{rdd\_list\_entry} used at its place.

        \item[\Opt{any\_local\_src\_hashing}]
            If set to \Arg{1}, turns on the hashing by first letter of local sources names.
            The storage scheme without hashing:
            \begin{itemize}
                \item[ ] a\_package
                \item[ ] abb\_package
                \item[ ] c\_package
            \end{itemize}
            The storage scheme with hashing:
            \begin{itemize}
                \item[ ] a/a\_package
                \item[ ] a/abb\_package
                \item[ ] c/c\_package
            \end{itemize}

        \item[\Opt{any\_make\_opts}]
            The options for \Cmd{make}{1} command, passed through \Prog{amake} interface.
            The most often usage is options for parallel make launch.\\
            Initialises variable \Envar{MAKEOPTS}.

        \item[\Opt{any\_meta\_dir}]
            Location of \Envar{METADIR} in working directory.\\
            Initialises variable \Envar{METADIR}.

        \item[\Opt{any\_morebuild\_cflags}, \Opt{any\_morebuild\_cxxflags}, \Opt{any\_morebuild\_ldflags}]
            Additional compile-time C, compile-time C++ and link-time flags, respectively.
            These can be used to add custom flags to \Envar{ENGINE\_*} series,
            besides the engine-generated values. That series of variables is added automatically
            to build strings for appropriate code and search paths.
            Options are not set by default.\\
            Initialise variables \Envar{ENGINE\_CFLAGS}, \Envar{ENGINE\_CXXFLAGS}, \Envar{ENGINE\_LDFLAGS}.

        \item[\Opt{any\_opt\_flags}]
            The options for compiler optimisations. They refer to code performance, not to its correctness.\\
            Initialises variable \Envar{OPT\_FLAGS}.

        \item[\Opt{any\_opt\_ldflags}]
            The options for linker optimisations.\\
            Initialises variable \Envar{OPT\_LDFLAGS}.

        \item[\Opt{any\_os}]
            The operation system of target packages.\\
            Initialises variable \Envar{ANYOS}.

        \item[\Opt{any\_out\_basedir}]
            The former absolute path to the working directory out of container.
            Used for user diagnostic. Generated by \Cmd{any-do}{1} and not set manually.\\
            Initialises variable \Envar{out\_BASEDIR}.

        \item[\Opt{any\_pack\_list}]
            One more option to keep lists of packages.
            That one is used in package and release methods.
            Unset by default.

        \item[\Opt{any\_prof\_core}, \Opt{any\_prof\_postfix}, \Opt{any\_prof\_prefix}]
            Components of build profile, text identificator of the build tree inside working directory.
            That identificator is available as \Envar{APROFILE} in shell.
            Profile is part of the paths like
            \File{build/image/}\Arg{APROFILE}\File{/} or \File{build/pool/}\Arg{APROFILE}\File{/}.
            That means changing the profile name leads to independent build tree.
            So the options with profile name should be included in that sections,
            which mean incompatibility: they declare new ABI, operation system or binary architecture.
            For example, config sections for \Arg{mips} and \Arg{sparc} should set up unique profiles,
            as packages for them are needed to be built separately from each other.
            \medskip
            Three different options to name profiles are used for the capability
            to add prefixes or postfixes to already existing profile name.
            Thus the build will be separated, but visually will be related with the parent.
            For example, dataword \Arg{cap64} may include \Opt{any\_prof\_postfix}=\Arg{64}.
            If generic architectures \Arg{mips} or \Arg{arm} have the profile ids as
            \Arg{mips} and \Arg{arm}, with additional dataword \Arg{mips,cap64} and \Arg{arm,cap64}
            they have new build profiles \Arg{mips64} and \Arg{arm64}.
            At the same time \Arg{cap64} is universal: it may be added equally to all other datawords.\\
            Initialise variable \Envar{APROFILE}.

        \item[\Opt{any\_profile\_engine}]
            Space separated list of directories for modular inclusion.
            Included engine modules end with \texttt{.sh} extension.\\
            Default value is: \File{any/include} \File{local/include}

        \item[\Opt{any\_profile\_lib}]
            Space separated list of directories for modular inclusion of library code.
            The code must not change because of API variables changing. Therefore it is not reincluded
            second time when shell dumps of other packages are loaded into current context.
            That allows to speed up dump loading.
            The required condition is always done when library code contains only methods.
            Included library engine modules end with \texttt{.sh} extension.\\
            Default value is: \File{any/lib} \File{local/lib}

        \item[\Opt{any\_program\_linguas}]
            Space separated list of language codes.
            May be used to reduce internationalisation texts to selected minimum.\\
            Initialises variable \Envar{LINGUAS}.

        \item[\Opt{any\_program\_prefix}]
            Essential prefix to locate programs inside install tree.
            Used in build interfaces like \Prog{aconf}.
            Examples are \File{/usr} or \File{/usr/pkg}.\\
            Default value is: \File{/usr}\\
            Initialises variable \Envar{PREFIX}.

        \item[\Opt{any\_program\_prefix\_list}]
            Space separated list of prefixes, used among all programs in distro project.
            Used when all packages are not configured for single prefix.
            First entry from the list is used for \Envar{PREFIX}, if one is not set.
            The option is used for engine-related compilation flags: for each entry of the list
            search path for headers and libraries inside \Envar{ROOT} with that prefix is added.\\
            Initialises variables \Envar{PREFIX\_LIST}, \Envar{PREFIX}, \Envar{ROOT\_CFLAGS},
            \Envar{ROOT\_LDFLAGS}, \Envar{ROOT\_DIRS}.

        \item[\Opt{any\_program\_subfs}]
            Additional root path inside \Envar{D}, so that compiler deals with
            \Envar{\$ROOT}\Envar{\$SUBFS} instead of simple \Envar{\$ROOT}.
            This is needed for packages, configured to something like \File{/opt/prog/usr}
            (where \File{/opt/usr} would be \Envar{SUBFS}). Empty by default.\\
            Initialises variable \Envar{SUBFS}.

        \item[\Opt{any\_rdd\_root}]
            Reserved for future use.

        \item[\Opt{any\_release\_dir}]
            Directory for final built release material (whatever should it be).
            When option is not set, value \Arg{release} for \Envar{RELEASEDIR} is taken.\\
            Initialises variable \Envar{RELEASEDIR}.

        \item[\Opt{any\_release\_id}]
            Short text identificator of the release files.
            It is used in inner versions of binary packages to identify the release they belong to:
            \Arg{slack14.2}, \Arg{debian9}.\\
            Initialises variable \Envar{RELEASE}.

        \item[\Opt{any\_release\_version}]
            Full version of the release to write down in changelog:
            \Arg{Debian-9.0}, \Arg{Slackware-14.2}.\\
            Initialises variable \Envar{RELEASE\_VERSION}.

        \item[\Opt{any\_remote\_src\_hashing}]
            If set to \Arg{1}, turns on the hashing by first letter of development sources names.
            That suggests sources are kept at remote server by hashing scheme.
            See description of \Opt{any\_local\_src\_hashing}.

        \item[\Opt{any\_src\_dir}]
            Directory with original package sources: they are taken as is from here.
            Patches or modifications are applied on top of them later.\\
            Default value is: \Arg{ports/srcstatic}\\
            Initialises variable \Envar{ANYSRCDIR}.

        \item[\Opt{any\_static\_patch\_dir}]
            Directory with plain patches, kept separately from ports.
            This is one of possible patch sources.\\
            Default value is: \Arg{ports/patch}\\
            Initialises variable \Envar{static\_PATCHDIR}.

        \item[\Opt{any\_static\_root}]
            If set to \Arg{1}, the work with \Envar{ROOT} moves to static mode without dependencies handling:
            single \Envar{ROOT} directory is created for all packages, and \Cmd{pkg\_root}{3}
            of each one mirrors its files into that general directory.
            Files of entire \Envar{ROOT} are available to the package while its building
            instead of materials of dedicated packages, requested by \Envar{DEPEND}. Unset by default.\\
            Initialises variables \Envar{static\_ROOT}, \Envar{ROOT}.

        \item[\Opt{any\_tch\_cc}, \Opt{any\_tch\_ccld}, \Opt{any\_tch\_cxx}, \Opt{any\_tch\_cxxld}]
            Elements of C and C++ toolchain: C compiler, C link string, C++ compiler, C++ link string.\\
            Initialise variables \Envar{PLAIN\_CC}, \Envar{PLAIN\_CXX}, \Envar{PLAIN\_CCLD},
            \Envar{PLAIN\_CXXLD}, \Envar{PLAIN\_CPP}, \Envar{CC}, \Envar{CXX}, \Envar{CCLD},
            \Envar{CXXLD}, \Envar{CPP}.

        \item[\Opt{any\_tch\_host\_cc}, \Opt{any\_tch\_host\_ccld}, \Opt{any\_tch\_host\_cxx},
            \Opt{any\_tch\_host\_cxxld}]
            Elements of C and C++ toolchain in host environment. When no cross-compiling is used,
            host toolchain equals to the main toolchain and host series of options is not needed.\\
            Initialise variables \Envar{HOST\_CC}, \Envar{HOST\_CXX}, \Envar{HOST\_CCLD}, \Envar{HOST\_CXXLD},
            \Envar{HOST\_CPP}.

        \item[\Opt{any\_util\_root}]
            Option with the name of utility to create root-owned materials,
            when running as general user.

        \item[\Opt{any\_write\_root}]
            Base directory to write everything during the work of \Cmd{any}{1},
            possibly excluding the release production.\\
            Default value is: \Arg{build}
    \end{description}

\subsection{Options of engine tuning}

    \begin{description}
        \item[\Opt{any\_engine\_dump}]
            Scheme of work with multiple dumps. The scheme defines how dump of a new (non-current)
            package will be got when its context is needed. Available ones are:
            \begin{itemize}
                \item[ ] \Arg{plain}
                \item[ ] \Arg{heavy}
                \item[ ] \Arg{crooked}
            \end{itemize}
            \Arg{plain} scheme is the default.
            It creates new dump, basing on already existing dump of the current package.\\
            \Arg{heavy} scheme generates new dump with \Cmd{dumprdd}{1}.\\
            \Arg{crooked} scheme relies on dump, created beforehand.

        \item[\Opt{any\_root\_exec\_editable}]
            Space separated list of file types, which should be automatically edited
            by \Cmd{image\_setup\_paths}{3} inside \Cmd{pkg\_install}{3}.
            That method aims to remove all inclusion of build-related paths, such as \Envar{ROOT},
            from installable files. File type is defined by \Cmd{file}{1} with the call of:
            \begin{verbatim}
            file -b --mime-type filename 
            \end{verbatim}
            Default value is:
            \Arg{text/} \Arg{application/xml} \Arg{application/x-shellscript} \Arg{inode/symlink}

        \item[\Opt{any\_root\_exec\_name\_template}]
            Space separated list of templates for \Cmd{find}{1} to determine names of executable
            non-binary scripts, which should be installed to \Envar{ROOT} as real copies
            (instead of symlinks) and edited to work from that directory.
            The scripts are searched in prefixes from \Envar{PREFIX\_LIST},
            concatenated with each entry of \Envar{root\_TRACKED\_BIN\_DIRS}
            (according option \Opt{any\_root\_tracked\_bin\_dirs}).\\
            For example, when \Envar{PREFIX\_LIST} contains \Arg{/usr} and \Arg{/usr/local},
            and \Envar{root\_TRACKED\_BIN\_DIRS} contains \Arg{bin} and \Arg{sbin},
            search for executables will go in \File{/usr/bin/}, \File{/usr/sbin/},
            \File{/usr/local/bin/}, \File{/usr/local/sbin/}.\\
            Default value is: \Arg{*-config} \Arg{*-config-*}

        \item[\Opt{any\_root\_textbuild\_files}]
            Space separated list of templates for \Cmd{grep}{1} to determine names of text files,
            which should be installed to \Envar{ROOT} as real copies (instead of symlinks)
            and edited to work from that directory.
            The files are searched in prefixes from \Envar{PREFIX\_LIST}, concatenated with each entry
            from \Envar{root\_TRACKED\_DIRS} (according option \Opt{any\_root\_tracked\_dirs}).\\
            For example, when \Envar{PREFIX\_LIST} contains \Arg{/usr} and \Arg{/usr/local},
            and \Envar{root\_TRACKED\_DIRS} contains \Arg{lib} and \Arg{include},
            search for files will go in
            \File{/usr/lib/}, \File{/usr/include/}, \File{/usr/local/lib/}, \File{/usr/local/include/}.\\
            By default \File{.pc} files are edited: they are part of \Cmd{pkg-config}{1} scheme
            to provide information about includes and libraries.
            Search template in default value is regular expression instead of simple text.\\
            Default value is: \verb+\.pc$+\\
            Initialises variable \Envar{root\_TEXTBUILD\_FILES}.

        \item[\Opt{any\_root\_tracked\_bin\_dirs}]
            Space separated list of directories, where executable files for \Envar{ROOT} edit
            are searched. See \Opt{any\_root\_exec\_name\_template} description.\\
            Default value is: \Arg{bin}\\
            Initialises variable \Envar{root\_TRACKED\_BIN\_DIRS}.

        \item[\Opt{any\_root\_tracked\_dirs}]
            Space separated list of directories, where simple text files for \Envar{ROOT} edit
            are searched. See \Opt{any\_root\_textbuild\_files} description.\\
            Default value is: \Arg{lib} \Arg{include}\\
            Initialises variable \Envar{root\_TRACKED\_DIRS}.

    \end{description}

\section{See also}
    \Cmd{any}{1}, \Cmd{any-do}{1}, \Cmd{rdd}{1}, \Cmd{datardd}{1}, \Cmd{dumprdd}{1}, \Cmd{lsrdd}{1},
    \Cmd{any-api}{7}, \Cmd{any-build}{7}

%@% IF !embed %@%
\section{Authors}
    Random Crew\\
    \URL{https://opendistro.org/any}
%@% END-IF %@%

\end{Name}

\LatexManEnd
\end{document}
