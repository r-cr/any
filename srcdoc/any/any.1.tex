\documentclass{article}
\author{Random Crew}
\usepackage[utf8]{inputenc}
\usepackage{latex2man}
\usepackage{devdoc}

\setVersion{0.16}
\begin{document}

\begin{Name}{1}{any}{Random Crew}{any manuals}{any reference page}

    \Prog{any}
    – the engine to build software, distributable in POSIX universe.

\section{Description}
    \Prog{any} at its core is the set of POSIX shell functions.
    The functions are designed for launch with \Prog{anch do} or non-container \Prog{any do} commands
    (see description of both in \Cmd{any-do}{1}).

    The build of each package is described by file called \emph{anybuild}, build file, build script or just build.
    Commands in a build are written in shell and grouped into aimed methods like source fetching, configure,
    compile or packing.

    Single build file may produce installable binary archives in formats for different package managers,
    for several platforms and with conformance to various development policies.


\section{Tutorial documentation}

    \begin{itemize}
        \item[] \Cmd{any-guide}{7} - generic introduction, the description of basic working concepts,
            the description of work with the system in examples. Start with that guide.
        \item[] \Cmd{any-howto}{7} contains brief summary of typical build tasks.
        \item[] \Cmd{any-build}{7} describes in examples how to create a new anybuild.
    \end{itemize}


\section{Local pages in man and html}

    One may view the man documentation on build commands and functions like that:
    \begin{verbatim}
        any man <subject>
    \end{verbatim}

    The \Cmd{any-man}{1} command has the same syntax, as the system \Cmd{man}{1}.

    Local docs in HTML format are located at \File{doc/html/index.html}.


\section{any, anch}

    \Prog{anch} is the command, performing its next command inside isolated file environment
    (host, container), locating in the working directory.
    \verb+anch do+ is the semantic equivalent for \verb+chroot <workdir> ; any do+

    All commands, starting with \Prog{any}, may be launched with \Prog{anch}:
    \begin{verbatim}
        any do id package-1.2.3
        # chrooted variant
        anch do id package-1.2.3

        any build id package-1.2.3
        # chrooted variant
        anch build id package-1.2.3

        ... etc.
    \end{verbatim}


\section{Reference documentation}

    For the work with existing packages:
    \begin{itemize}
        \item[] \Cmd{any-do}{1} describes the main lauch utility and possibilities of the build
            through command line.
        \item[] \Cmd{any-workdir}{7} describes the project directory to work with:
            its installation, inner structure and the purpose of each directory inside.
        \item[] \Cmd{any-conf}{7} describes all options inside configuration files.
    \end{itemize}

    For debug and writing of the new builds:
    \begin{itemize}
        \item[] \Cmd{any-map}{7} describes all essential methods and interfaces,
            which perform the actual building.
        \item[] \Cmd{any-api}{7} describes all interfaces, provided by the engine for the use
            inside build scripts.
    \end{itemize}

    For project development:
    \begin{itemize}
        \item[] \Cmd{any-dev}{7} describes the conventions about writing new code for the engine.
        \item[] \Cmd{any-distro}{7} describes the creation of new build projects.
    \end{itemize}

    Specialized areas:
    \begin{itemize}
        \item[] \Cmd{anymods}{7} lists the additional modules with support of version control systems,
            binary package formats and other outer infrastructure.
        \item[] \Cmd{rdd}{1} low-level description of frontend: how the data is read from config files
            and how the launch of build process is implemented.
    \end{itemize}


\section{Glossary}

    \begin{description}

        \item[Working directory]
            Autonomous directory with complete build suite. Working directories are isolated from each other and,
            in case of chrooted / containerised environment, from host system as well.
            See \Cmd{any-workdir}{7} for more detailed description of working directory.

        \item[Configs, configuration files, configuration options]
            All settings for the build process are stored in the configuration files \File{rdd.conf.d/}
            and \File{rdd.def} at the root of working directory. \File{ports/} directory, fixed version
            of \Prog{any} and aforesaid configuration files altogether represent exhaustive input data
            for the build.

        \item[Ports directory]
            Directory with the individual build scripts for packages.
            It also contains materials, common for the entire package set, such as etalon data for checkers.
            See \Cmd{any-workdir}{7} for more details.

        \item[Package]
            Single unit of software to build.
            Name of a package or a package list is the second argument in \Cmd{any-do}{1}
            launch string.
            \medbreak
            Package may be also referenced as \emph{binary package}, meaning installable archive
            with files and metainformation, intended for package manager.
            One source package can produce one or several binary packages.
            Software name inside package database may differ from the name in source form.
            In the \Prog{any} series of manuals the term \emph{package} means source package
            or build-time entity, unless specified otherwise.

        \item[Package list]
            Plain text file with a list of packages.
            Each package is on a separate line, comments and empty lines are ignored.
            Used for the build of package sets in single launch.
            Lists must be located at their own directory, \File{ports/list/} as given
            by default \File{any.conf} config.

        \item[Configuration name, data words]
            One or several words, identifying the data from configs, involved in a build.
            It's the first argument in \Cmd{any-do}{1} launch string.
            The examples from the above commands are \Arg{amd64} or \Arg{amd64,packslack}.
            \medbreak
            Data reading is designed in a way the first main id like \Arg{amd64}
            usually loads the full chain of other identificator words and their according data.
            The details of data reading are in \Cmd{any-do}{1}.

        \item[Build profile]
            A string, identifying the build inside working directory.
            It presents in paths to logs, temporary directories and stuff alike.
            In simple case the profile is equal to the configuration name,
            but it is not so in common case.
            For the configuration \Arg{amd64,packslack} the build profile remains \Arg{amd64}.
            \medbreak
            The build profile is set up explicitly inside configuration files.
            See \Cmd{any-conf}{7} for more detailed description.

        \item[Build map]
            The main shell method, executed along the work of \Prog{any}.
            By default it performs the complete build.
            See \Cmd{any-map}{7} for more details.
            \medbreak
            There can be other maps also, for example, \Prog{map\_autopre}, which contains sequence
            of inner auxiliary methods to prepare the building:
            output starting information to stdout, create needed temporary files.

        \item[Launch methods]
            It is possible to launch arbitrary methods instead of build map.
            They are given as the third argument in \Cmd{any-do}{1} launch string.
            Methods may perform arbitrary actions, not only package building.
            The main limitation to the methods is absence of input parameters,
            but they can use variables from \Cmd{any-api}{7}.

        \item[Anybuild, build file, build script, build]
            The file in shell with methods and variables, which build the package.
            The most often these methods are
            \Cmd{src\_config}{3}, \Cmd{src\_compile}{3} and \Cmd{src\_install}{3}.
            Variables are used in engine procedures, such as dependencies handling.
            See \Cmd{any-build}{7} for further details.

        \item[Interface method]
            Method with determined aim, but possibly different implementations.
            Different implementations of interface may depend on configuration and
            on the set of modules, used during the build. That provides function
            flexibility.
            \medbreak
            Build interfaces are described in \Cmd{any-map}{7}.

        \item[Any API]
            Set of global shell variables and methods, allowed for use inside anybuilds.
            Described by \Cmd{any-api}{7}.

        \item[Engine module]
            Functionality, not turned on by default.
            It may be utilized with addition of needed identificator-word to the name of configuration.
            Module files with code usually reside in \File{any/lib/}, with own variables in
            \File{any/include/}.
            A module may have its own configs, declare new methods, global variables,
            implement interfaces of \Cmd{any-map}{7} in its own way and extend the functionality
            of the engine in almost arbitrary manner.
            \medbreak
            \Cmd{anymods}{7} contains module list, available by default with release of \Prog{any}.

    \end{description}


%@% IF !embed %@%
\section{Authors}
    Random Crew\\
    \URL{https://opendistro.org/any}
%@% END-IF %@%

\end{Name}

\LatexManEnd
\end{document}
