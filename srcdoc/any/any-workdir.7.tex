\documentclass{article}
\author{Random Crew}
\usepackage[utf8]{inputenc}
\usepackage{latex2man}
\usepackage{devdoc}

\setVersion{0.16}
\begin{document}

\begin{Name}{7}{any-workdir}{Random Crew}{any manuals}{any-workdir reference page}

    \Prog{any-workdir}
    – the local autonomous dir with complete build environment for \Cmd{any}{1} engine.


\section{Description}

    \begin{description}
        \item[Working directory]
            independent folder with all build materials, related to your project.
            It keeps sources, build directories, all temporary files, built binaries,
            ready to install archives and the copy of build system itself.
    \end{description}

    The build is designed for non-priveleged user accounts.
    No impact on filesystem outside working directory is done during the build.
    Different users may create unlimited quantity of working directories on a single machine.

    In case of building in container (the most reliable way) the working directory contains local
    executable files of host, isolating the build process from outer host system.
    As the result we have reproducible build environment, portable between different machines.
    The engine is copied into each working directory, so the configuration of one does not impact the other.

    When host container is not applied, the user must support on his own the sane environment from
    the generic operation system for needed build tasks.
    One of the examples on no possibility of isolation: the public server does not provide permissions
    for \Cmd{sudo}{1}, which is applied for the isolation command \Cmd{chroot}{1}.


\section{Installation and configuration}

    New directory creation for the work with \Prog{any} comes down to the following:

    \begin{itemize}
        \item[] unpacking the main archive with build release of your project:
            \begin{verbatim}
    tar xzf distro-project.tgz
    mv distro-project your-name
    cd your-name/
            \end{verbatim}
        \item[] if additional stuff is present (host, toolchain), unpack that:
            \begin{verbatim}
    tar xzf add-files.tgz
    tar xzf more-add-files.tgz
    ...
            \end{verbatim}
    \end{itemize}

    The configuration of working directory and preparation of the work is described in more details
    in \Cmd{any-guide}{7}.


\section{Essential directories}

    The essential directories of a working directory are:
    \begin{itemize}
        \item[]\File{any/}
        \item[]\File{rdd.conf.d/}
        \item[]\File{rrr/}
        \item[]\File{build/}
        \item[]\File{ports/}
        \item[]\File{release/}
    \end{itemize}

    The brief overview of each dir:
    \begin{description}
        \item[\File{any/}]
            is the directory with files of \Prog{any} build engine itself.
            Each working directory has its own copy of that files.
            They do not interfere with other working directories.

        \item[\File{rdd.conf.d/}, \File{rdd.def}]
            are the place for configuration files of a project.
            All settings for build process are assigned through these configs.

            \File{rdd.conf.d/} is the directory with generic config set for all purposes.
            The library-like settings are kept here for different platforms, architectures, toolchains.

            \File{rdd.def} is the single config file to choose the current settings.
            You may store configuration name or particular build properties here,
            which are constantly in use. After that here will be no need to assign them each time
            in command line.

            Essential options from configuration files are described in \Cmd{any-guide}{7}.
            All possible options are described in details in \Cmd{any-conf}{7}.

        \item[\File{rrr/}]
            location of build utils, installed locally.
            There are directories like \File{rrr/bin/} and \File{rrr/share/} inside.

            Here is the place for utils from \Prog{any} as well as for third-party programms,
            if they implement some specific functions for generic build system.
            Note the generic programs like \Prog{cp} or \Prog{sed} are not installed here.

            When the build process is launched, the engine adds the path \File{rrr/bin/} to the variable
            \Envar{PATH} automatically to make available the programs from that local path by default.

        \item[\File{build/}]
            is the directory to write files during the build, as given by default \File{any.conf} config.\\
            Here the build directories with sources and object files are stored, compilation takes place,
            ready package files are installed and binary archives are created.
            All subdirectories of \File{build/} are created during build process.
            \medbreak
            The dir must not keep any unique or unproducible data, concerning the engine or input.
            It is safe to clear it and rebuild the same project from the start.
            This dir is absent until the first build.
            The locally saved sources or patches, which are not intended to be available from the network,
            should not be kept here, \File{ports/} is used for that instead.

        \item[\File{build/pack/}]
            is the directory where packaged results of successfull build are stored.

        \item[\File{ports/}]
            is the directory with the content of the distro, as given by default \File{any.conf} config.\\
            In the first turn, those are scripts, personal for each package,
            with commands to build them. Etalon input data for checkers is kept here as well.
            Opposed to that, generic build commands and methods, same for all packages,
            are stored inside engine \File{any/}.
            \medbreak
            In case of several various distro projects, which share little common between each other,
            it is possible to keep their data in separate directories, analogues to \File{ports/},
            and choose the needed one between them with configs.

        \item[\File{release/}]
            is the directory with final release results of entire distro project.
            Its contents depend on the meaning of 'final result', taken by the project.
            It can be single installable image or some predefined set of packages.
            \medbreak
            The directory is not normally populated automatically after buildage of single packages.

    \end{description}

\section{Host environment}
    The entire host environment is copied with new working directory creation by \Cmd{anyinit}{1},
    unless configured otherwise. It includes all executables to enter new shell session
    in containerised environment and execute generic build procedure:
    libc, POSIX baseutils, shell implementation, make. It must include toolchain with compiler and linker.
    Many utils will be needed for documentation generation and other help tasks during the build.

    The host files are installed into root of the working directory with the same layout
    as they have in generic OS:
    \File{bin/}, \File{lib/}, \File{usr/lib/}, \File{usr/bin/}, \File{etc/} and so on.
    After \Cmd{chroot}{1} or other container implementation the build process will see general filesystem,
    localised inside working dir.

    The build process with \Prog{any} is designed to use vast majority of files from \File{build/},
    which is populated from packages, which are built inside the project or installed by some request.
    Installations inside \File{build/} are granulated by profiles, can be tweaked and rebuilt by lots of ways.

    The purpose of host is bare run-time support for all utilities which handle the build,
    while introducing minimal impact on compilation and linkage with host libraries and include files.
    The ideal host from that point of view contains no libraries and includes at all,
    besides the ones for native toolchain support.
    Such host is not so easy to reach, especially to get rid off dynamic shared libraries for binaries.
    Much easier to remove or hide everything from
    \File{usr/lib/pkgconfig/} and all \File{usr/bin/*-config} scripts,
    and leave only those files from \File{usr/include/} to keep compiler working.

\section{Engine directories}
    The entire set of engine directories inside a working dir is:

    \begin{itemize}
        \item[] \File{any/bin/}
        \item[] \File{any/corelib/}
        \item[] \File{any/include/}
        \item[] \File{any/lib/}
        \item[] \File{build/context/}
        \item[] \File{build/empty/}
        \item[] \File{build/image/}
        \item[] \File{build/log/}
        \item[] \File{build/pack/}
        \item[] \File{build/patch/}
        \item[] \File{build/src/}
        \item[] \File{build/tmp/}
        \item[] \File{build/work/}
        \item[] \File{ports/packages/}
        \item[] \File{ports/list/}
        \item[] \File{ports/meta/}
        \item[] \File{ports/patch/}
        \item[] \File{ports/srcstatic/}
        \item[] \File{rdd.conf.d/}
        \item[] \File{release/}
        \item[] \File{rrr/bin/}
        \item[] \File{var/dump/}
        \item[] \File{var/lib/}
    \end{itemize}

    \begin{description}

        \item[\File{any/bin/}]
            Directory for inner executables of \Prog{any} itself.
            Such executables are supposed non-usable outside of engine.
            That can be C binaries, implementing engine methods instead of ones in shell.
            The path is added to \Envar{PATH} variable inside build environment.

        \item[\File{any/corelib/}]
            Directory with main engine libraries, which implement core functional and declare
            all mandatory interfaces. All libraries from here are always included in the launch
            disregarding used configuration and modules.
            Some interfaces contain empty or trivial implementation:
            that means useful implementation is provided by some additional code and depends
            on user's choice (one or another control version system, for example).
            Such support is distributed through additional modules, installed into \File{any/lib/}.

        \item[\File{any/include/}]
            Directory for shell API constructing beyond core functionality.
            If some module declares global variables, it should place their assignments
            in the file inside this dir.

        \item[\File{any/lib/}]
            Directory for all possible modules with additional functional.
            Some of them are included in pack of \Prog{any}, some of them come from \Cmd{anymods}{7},
            still more may be third-party.\\
            Modules within this dir must contain only method implementations.
            No calls out of methods' bodies are allowed.
            No declarations of global variables are allowed as well.\\
            The files of the same module may be placed in both
            \File{any/include/}\Arg{module}\File{.sh} and \File{any/lib/}\Arg{module}\File{.sh}.
            The first file will contain variable declarations, the second one methods.

        \item[\File{build/context/}]
            Directory to keep file context for package building.
            Context is composed from files of other packages, used by the current building one.
            Composing is done with symbolic links from real installed files of other packages into context dir.
            Several types of text files are copied, as they are edited by engine.
            Each package has its own separate context directory.
            The set of files, used for context, is defined by \Cmd{any-conf}{7} settings.
            The list of packages, needed for current one (build dependencies),
            is defined by anybuild of current package, as described in \Cmd{any-build}{7}.
            \medbreak
            Personal context of each package is stored into dir
            \File{build/context/}\Arg{PROF}\File{/}\Arg{PACKAGENAME}\File{/root/}.\\
            For example, context of hello-world-1.0 for amd64 will be kept in
            \File{build/context/amd64/hello-world-1.0/root/} dir inside working directory.
            \medbreak
            The given scheme means the following:
            \begin{itemize}
                \item[-] The build environment is controlled explicitly by dependencies.
                    The obscure non-obvious influence on the build process is reduced significantly.
                \item[-] Build context is lightweight and quickly composed, as the most files
                    are handled with symbolic links and heavy copies are avoided.
                \item[-] Correctness of build dependencies is checked automatically -
                    in case of missing dependency in anybuild the build process
                    will not have access to corresponding files.
                \item[-] There is no changing resources, shared between building of several packages.
                    So different packages can be launched in parallel without any synchronisation at all,
                    while they do not depend on each other
                    (in other case synchronisation of dependencies handling comes to a game).
            \end{itemize}
            \medbreak
            Directory \File{build/context/} does not limit the influence of host environment,
            i.e. the binaries of container, installed into the root of working directory,
            or the entire host system without a container.
            \medbreak
            Interfaces \Cmd{pkg\_context}{3} and \Cmd{pkg\_root}{3} write to this dir.

        \item[\File{build/context/}\Arg{PROF}\File{/platform/}]
            Directory with context, equal for all packages. The context is contained in tar archives.
            Every archive from this dir is extracted to personal context directory of each package
            (into \File{build/context/amd64/hello-world-1.0/root/} for the example above).\\
            The example of platform context may be system libc or some minimal filesystem,
            which is not granulated by packages.

        \item[\File{build/context/}\Arg{PROF}\File{/static/root/}]
            Directory with single context, shared for all packages, used in \Arg{staticdeproot} mode.
            That mode makes no personal context for each package, but maintains single shared one,
            as in traditional file system. See \Cmd{any-do}{1} for description of \Arg{staticdeproot}.

        \item[\File{build/empty/}]
            Always empty dir.
            It is used to prevent expansion of meta-symbols, such as '*', in some shell methods.

        \item[\File{build/image/}]
            The dir for installed binary files of packages.
            Each package is installed into separated dir
            \File{build/image/}\Arg{PROF}\File{/}\Arg{PACKAGENAME}\File{/}.
            \medbreak
            For example, the package hello-world-1.0 for amd64 will have installed files:
            \begin{itemize}
                \item[]\File{build/image/amd64/hello-world-1.0/etc/}
                \item[]\File{build/image/amd64/hello-world-1.0/usr/bin/}
                \item[]\File{build/image/amd64/hello-world-1.0/usr/lib/}
                \item[]\File{build/image/amd64/hello-world-1.0/usr/include/}
                \item[]\File{build/image/amd64/hello-world-1.0/usr/share/}
                \item[]...
            \end{itemize}
            \medbreak
            Interfaces \Cmd{src\_install}{3}, \Cmd{pkg\_install}{3}, \Cmd{pkg\_rminstall}{3},
            \Cmd{pkg\_config}{3} write to this dir.

        \item[\File{build/log/}]
            Directory to keep logs of build process.
            Each package \Arg{PACKAGENAME} for profile \Arg{PROF} keeps its log file in
            \File{build/log/}\Arg{PROF}\File{/}\Arg{PACKAGENAME}\File{.log}.\\
            For example, the package hello-world-1.0 for amd64 will keep log file in
            \File{build/log/amd64/hello-world-1.0.log}.
            \medbreak
            Under the hood, the logs in this directory are symbolic links to files inside \File{var/dump/},
            as logs are total output redirect, constructed by \Cmd{rdd}{1},
            and that command uses \File{var/dump/} unconditionally for all its operations.
            \medbreak
            Each time new build of a package starts its log file is rewritten.
            \medbreak
            The log file of entire build session is kept in
            \File{build/log/}\Arg{PROF}\File{/}\Arg{PROF}\File{.log} file. That file is never truncated.
            Each time new build session starts, summarised information of the build is appended to profile log.
            That summarised information is the same as outputed to terminal,
            besides the explicit link to the log in case of error.

        \item[\File{build/pack/}]
            Directory for binary archives with packages.
            When no additional naming convention is included, each built package is saved into
            installable archive
            \File{build/pack/}\Arg{PROF}\File{/}\Arg{PACKAGENAME}\Arg{-postfix}\File{.tgz}.
            \Arg{postfix} part depends on target platform for the package.\\
            For example, the package hello-world-1.0 for amd64 will have simple binary archive
            \File{build/pack/amd64/hello-world-1.0-x86\_64.tgz}.\\
            Modules can change the binary format for a package and portion of data in its file name.
            \medbreak
            When some convention of archive naming takes place, each package has one or more
            corresponding binary archives with arbitrary names.
            That names are usually described in anybuild.
            To match package names in terms of \Prog{any} and names of their binary archives
            the method \Cmd{pack\_name}{3} is used.
            \medbreak
            Interface \Cmd{pkg\_pack}{3} writes to this dir.

        \item[\File{build/patch/}]
            Directory for patches, applied for packages.
            Each package \Arg{PACKAGENAME} gets his patches from
            \File{build/patch/}\Arg{PACKAGENAME}\File{/} directory.\\
            For example, the package hello-world-1.0 will use patches from \File{build/patch/hello-world-1.0/}.
            \medbreak
            There are different places, where patches may be stored, but all resulting files
            will be finally copied to \File{build/patch/}.
            \medbreak
            Interface \Cmd{src\_fetch}{3} writes to this dir.

        \item[\File{build/src/}]
            Directory to store sources of packages, retrieved from version control systems.
            Each package \Arg{PACKAGENAME} stores its sources in
            \File{build/src/}\Arg{PACKAGENAME}\File{/} directory.\\
            For example, the package hello-world-1.0 will fetch development sources into
            \File{build/src/hello-world-1.0/}.
            \medbreak
            That sources are used to generate some patches, applied before building.
            That is done by \Cmd{fetch\_gen\_patches}{3} interface.
            Core \Prog{any} engine does not generate any patches, but modules may implement
            this interface for \Cmd{git}{1}, \Cmd{hg}{1}, \Cmd{svn}{1} or another content tracker.\\
            It is possible that sources from this dir are copied 'as is' on top of previously prepared ones.
            \medbreak
            The default value of that dir can be altered with \Opt{any\_dev\_src\_dir} option.
            \medbreak
            Interface \Cmd{src\_fetch}{3} writes to this dir.

        \item[\File{build/tmp/}]
            Directory to write and keep all intermediate files during the work of \Prog{any} engine or modules.

        \item[\File{build/work/}]
            Directory to do the actual build.
            The final sources are prepared here, configure, make and make install are launched.
            Each package has that dir in \File{build/work/}\Arg{PROF}\File{/}\Arg{PACKAGENAME}\File{/}.\\
            For example, the package hello-world-1.0 for amd64 will be built in
            \File{build/work/amd64/hello-world-1.0/} directory.
            \medbreak
            The directory is kept after the successfull build, as it contains all intermediate results
            of the process and irreplaceable for debug.
            \medbreak
            Interfaces \Cmd{src\_fetch}{3}, \Cmd{src\_prepare}{3}, \Cmd{src\_config}{3},
            \Cmd{src\_compile}{3} write to this dir.

        \item[\File{ports/packages/}]
            Directory with individual build scripts, called anybuilds.
            Each package has its own separate directory, named after package.
            Anybuild script inside is named as \Arg{package-name-1.2.3}.build.
            This file is included by default for given package inside \Prog{any}.\\
            For example, anybuild for hello-world-1.0 will be in
            \File{ports/packages/hello-world-1.0/hello-world-1.0.build} file.\\
            More separate shell modules can reside in the directory.
            That modules are included after data words, saved in \Envar{rdd\_prf\_all} variable,
            as described in section \textbf{Modular code inclusion} of \Cmd{any-do}{1}.
            \medbreak
            Package directory keeps the config file of \Cmd{rdd}{1}, personal for package,
            named \File{atom.conf}. That file is analogues to the ones in \File{rdd.conf.d/},
            but applies only to that package.
            \medbreak
            The directory \File{ports/packages/}\Arg{package-name-1.2.3}\File{/patch}, if one exists,
            is used as source of patches for the package.
            That dir allows to keep all input data for the package build in a single place
            (anybuild and patches all together).
            Besides this dir, patches may be fetched from other places separately from \File{ports/}.
            \medbreak
            When binary archive format for some package manager is used, the various
            postinstall, preinstall and other scripts alike are needed.
            They are stored in the package directory inside \File{ports/packages/} as well.\\
            Personal package directory may contain other materials, used by modules
            (for example, sanity or regression checkers).
            \medbreak
            The default value of the dir can be altered with \Opt{rdd\_atom\_path} option.

        \item[\File{ports/list/}]
            Directory for package lists of \Cmd{rdd}{1}.
            Here text lists are kept with content of some buildable sets, composing final project
            such as OS distribution.
            \medbreak
            The convention is to name general hand-written lists with \Arg{.src} ending,
            and script generated ones with \Arg{.txt} ending.
            \medbreak
            List \Arg{all.src} has special meaning – it is used to keep all packages,
            available in project by default.
            Besides the other, that value is used as data source to resolve build dependencies.
            The name of the list can be changed through the option \Opt{any\_list\_all}.
            \medbreak
            The default value of the dir can be altered with \Opt{rdd\_list\_path} option.

        \item[\File{ports/meta/}]
            Directory for various meta-data, concerning the entire distro project inside \File{ports/}:
            etalon data for sanity checkers, cache for cross-compilation.
            \medbreak
            The default value of the dir can be altered with \Opt{any\_meta\_dir} option.

        \item[\File{ports/patch/}]
            Directory to keep locally saved patches for packages.
            Each package has its room in \File{ports/patch/}\Arg{PACKAGE-NAME}\File{/}.
            \medbreak
            The default value of the dir can be altered with \Opt{any\_static\_patch\_dir} option.

        \item[\File{ports/srcstatic/}]
            Directory to store source archives, or tarballs.
            All prepared patches will be applied to the sources from a found archive.
            So these archives are supposed to be original or unchanged upstream sources,
            while development changes are done through patches or other explicit actions.
            \medbreak
            The alternative usage is to keep actual full-pledged development sources in
            \File{ports/srcstatic/} and not to use patches at all.
            That variant can be handy at inner debug, when still there is no need to share changes done.
            \medbreak
            In simple case, sources are recognised by expression
            \File{ports/srcstatic/}\Arg{PACKAGENAME}\File{.}\Arg{EXT}.\\
            For example, the package \Arg{hello-world-1.0} will catch source tarball
            \File{ports/srcstatic/hello-world-1.0.tgz}.
            \medbreak
            That is not the only scheme, though, as sources may have alternate name.
            This is due to possibly different names of package at authors domain and
            in naming convention of current distro project.
            The alternate name for sources is described in \Cmd{any-api}{7} and
            \Cmd{fetch\_remote\_src}{3} method.
            \medbreak
            If plain directory with matching name is found inside \File{ports/srcstatic/}, it will be used.
            Otherwise the tar archive will be searched for.\\
            Known tarball extensions are:
            \begin{itemize}
                \item[] .tar.xz
                \item[] .txz
                \item[] .tar.gz
                \item[] .tgz
                \item[] .tar.bz2
                \item[] .tbz2
            \end{itemize}
            \medbreak
            The default value of the dir can be altered with \Opt{any\_src\_dir} option.
            \medbreak
            Interface \Cmd{src\_fetch}{3} writes to this dir.

        \item[\File{rdd.conf.d/}, config file \File{rdd.def}]
            Directory with configuration files of \Cmd{rdd}{1}.
            That's the place with all engine properties.
            Some of them belong to \Cmd{rdd}{1} domain, some of them are read by \Prog{any} part
            of the engine and described in \Cmd{any-conf}{7}.
            \medbreak
            Directory with \Prog{any} source code ships \File{example.conf.d/} subdir.
            If configs from \File{any/example.conf.d/} are added to \File{rdd.conf.d/},
            working directory will get reasonable default values to begin the work.
            \File{any.conf} is the file with all default values for engine, described in the manuals.
            Example architecture-specific settings, describing properties of target OS
            and processor architecture, are stored in \File{build.conf}.
            Arbitrary new files can be added to describe new entities or customise the build.
            \medbreak
            File \File{rdd.def} works as general config, but with higher priority.
            It is used for current settings, non-saveable for all working directories or situations.
            It can be used for experiments, usage of testing code, storage of dynamically generated
            or fetched information.

        \item[\File{release/}]
            Directory with release results of distro project.
            It can be single installable image or some predefined set of packages.

        \item[\File{rrr/bin/}]
            \File{rrr/} is the destination directory to install utilities, used by engine.
            The content of used utilities is determined by \Cmd{anyinit}{1} configuration.
            Changes to utilities should not be done here on a regular basis, better change util
            at its source and reinstall
            (though it's handy to edit something right in \File{rrr/} for quick test purposes).
            Path \File{rrr/bin/} is added to \Envar{PATH} variable inside build engine.

        \item[\File{var/dump/}]
            Directory where \Cmd{rdd}{1} keeps its intermediate files while constructing the build scripts.
            The log files of build process are kept here. This dir is the place for dumps –
            shell files with full \Prog{any} environment and gathered data for some package.
            The dumps are used at several parts of the engine.

        \item[\File{var/lib/}]
            Directory to track installed modules.
            Each module \Arg{MOD} from \Cmd{anymods}{7} leaves text file \File{var/lib/anypkg/}\Arg{MOD},
            containing list of files of that module.

    \end{description}


\section{anyinit}

    New working directories can be created with specialized command \Cmd{anyinit}{1}.
    It copies data for a new directory from the template, installed into common filesystem.

    The template can be installed from the \Prog{any} release archive:
    \begin{verbatim}
        make install-workdir
    \end{verbatim}
    Stuff is installed into home directory inside \File{\Tilde /rrr/share/any/workdir} by default.
    If commands of \Prog{any} have been installed by default before,
    then \Cmd{anyinit}{1} will find the stuff from \Arg{workdir}.

    Template can be installed into the system:
    \begin{verbatim}
        sudo env DESTDIR=/usr make install-workdir
    \end{verbatim}
    The command above will install the stuff into \File{/usr/share/any/workdir}.
    For \Cmd{anyinit}{1} to find it, the commands of \Prog{any} must be installed
    into \File{/usr} as well.

    After the template installation it is possible to create new working directory like that:
    \begin{verbatim}
        anyinit my_dir
        cd my_dir
    \end{verbatim}


\section{See also}
    \Cmd{any}{1}, \Cmd{any-do}{1}, \Cmd{anyinit}{1}, \Cmd{any-conf}{7}, \Cmd{anymods}{7},
    \Cmd{any-build}{7}, \Cmd{any-api}{7}

%@% IF !embed %@%
\section{Authors}
    Random Crew\\
    \URL{https://opendistro.org/any}
%@% END-IF %@%

\end{Name}

\LatexManEnd
\end{document}
