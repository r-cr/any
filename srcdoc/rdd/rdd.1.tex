\documentclass{article}
\author{Random Crew}
\usepackage[utf8]{inputenc}
\usepackage{latex2man}
\usepackage{devdoc}

\setVersion{2.0}
\begin{document}

\begin{Name}{1}{rdd}{Random Crew}{rdd manuals}{rdd reference page}

    \Prog{rdd}
    – tool to launch script functions.

\section{Synopsis}
    \Prog{rdd}
        \Arg{DATA}
        \oArg{options}
    \\
    \Prog{rdd}
        \Arg{DATA}
        \Arg{OBJECTS}
        \oArg{options}
    \\
    \Prog{rdd}
        \Arg{DATA}
        \Arg{OBJECTS}
        \Arg{TASKS}
        \oArg{options}
    \\
    \Prog{rdd}
        \oArg{options}
    \\
    \Prog{rdd}
        \Opt{--help}\Bar\Opt{--version}

\section{Description}
    \Prog{rdd} launches functions from selected libraries in shell. It also gathers data from config files in various combinations.\\
    The tool allows to construct scripts with input data and control flow ruled from command line.

    \Cmd{rdd-howto}{7} contains step-by-step usage description with examples. This manual contains exhaustive description of all implementation details
    and is more useful after one gets used with essentials.

    \Prog{rdd} provides command line interface to say:\\
    read the \Arg{DATA}, take some \Arg{OBJECTS} and launch these \Arg{TASKS} for these objects with given input data.

    \begin{description}
        \item[\Arg{DATA}] is summarised from command line options and plain text configuration files. Data may refer inclusion of another data,
            thus large data set is possible with mentioning single start entry.
        \item[\Arg{OBJECTS}] is the list of names. Each name can store some personal data or properties in addition to generic set.
            Launch of resulting code is done as separate process for every object in list.
        \item[\Arg{TASKS}] is the list of functions from user's libraries to launch.
            Arbitrary library functions can be called directly from command line.
    \end{description}

\section{Short-form arguments}
    First arguments (from one to three) can be passed in shortened form as the value without corresponding option.
    This is done to type essential commands as the sentence without symbols like \texttt{-} or \texttt{=}.

    The relation of positioned values to actual options is as follows:
    \begin{description}
        \item[\Arg{DATA}] first short argument, the value of \Opt{rdd\_prf\_entry} option.
        \item[\Arg{OBJECTS}] second short argument, the value of \Opt{rdd\_list\_entry} option.
        \item[\Arg{TASKS}] third short argument, the value of \Opt{rdd\_map\_entry} option.
    \end{description}

    The illustration of short forms and their equivalents:

%@% IF !MAN %@%
    \begin{itemize}
        \item[] \Prog{rdd} \Arg{DATA}
        \item[] \Prog{rdd} \Opt{rdd\_prf\_entry}=\Arg{DATA}
    \end{itemize}
%@% ELSE %@% 
    \Prog{rdd} \Arg{DATA}\\
    \Prog{rdd} \Opt{rdd\_prf\_entry}=\Arg{DATA}
%@% END-IF %@%

%@% IF !MAN %@%
    \begin{itemize}
        \item[] \Prog{rdd} \Arg{DATA} \Arg{OBJECTS}
        \item[] \Prog{rdd} \Opt{rdd\_prf\_entry}=\Arg{DATA} \Opt{rdd\_list\_entry}=\Arg{OBJECTS}
    \end{itemize}
%@% ELSE %@% 
    \Prog{rdd} \Arg{DATA} \Arg{OBJECTS}\\
    \Prog{rdd} \Opt{rdd\_prf\_entry}=\Arg{DATA} \Opt{rdd\_list\_entry}=\Arg{OBJECTS}
%@% END-IF %@%

%@% IF !MAN %@%
    \begin{itemize}
        \item[] \Prog{rdd} \Arg{DATA} \Arg{OBJECTS} \Arg{TASKS}
        \item[] \Prog{rdd} \Opt{rdd\_prf\_entry}=\Arg{DATA} \Opt{rdd\_list\_entry}=\Arg{OBJECTS} \Opt{rdd\_map\_entry}=\Arg{TASKS}
    \end{itemize}
%@% ELSE %@% 
    \Prog{rdd} \Arg{DATA} \Arg{OBJECTS} \Arg{TASKS}\\
    \Prog{rdd} \Opt{rdd\_prf\_entry}=\Arg{DATA} \Opt{rdd\_list\_entry}=\Arg{OBJECTS} \Opt{rdd\_map\_entry}=\Arg{TASKS}
%@% END-IF %@%

    When several values of the same option are given, the final resulting value is the most right one, disregarding its form, shortened or not.

    Listed three options must be initialised somehow for successfull launch of \Prog{rdd}.
    Everything missed in command line must be present in configuration files and vice versa.

\section{Common option syntax}
    Many options recieve single entry or a list of entries as the value. Entries are separated with commas.\\
    Pure text entries from the arguments may contain ascii letters, digits and underscore \texttt{\_} symbol.\\
    Paths may contain ascii letters, digits, spaces and set of symbols:
    \begin{verbatim}
        ~@.+-/_
    \end{verbatim}
    Object names may contain ascii letters, digits and set of symbols:
    \begin{verbatim}
        ~@.+-_
    \end{verbatim}

    Files and directories, when given with non-absolute paths, are prepended with the root of current working directory, which is stored in \Envar{RDD\_ROOT} variable.

    Some entries may contain specificator words before them (see \Opt{rdd\_map\_libs}).
    Specificators are separated from entries with spaces.\\

    There can be any quantity of spaces betweeen list entries, as well as between an option name and its value inside configuration files.

\section{Data gathering}
    The first part of \Prog{rdd}'s work is to gather data in the right order according to all configuration files and options. That part can be done separately with \Cmd{datardd}{1}.

    Configuration file has the following format:
    \begin{verbatim}
[section]                            

# comment on any line
option = value
option_with_list = list_value, another_list_value
...
    \end{verbatim}

    In case of the same variable with different values the value is taken from the source with highest priority.
    When data is read from sources with the same priority, first read sections have more priority over further ones.

    First sections are taken from \Opt{rdd\_prf\_id} in command line or \File{rdd.def} file.
    The next priority belongs to sections from \Opt{rdd\_prf\_entry} in command line or \File{rdd.def} file.
    Further sections are added and parsed with the addition of \Opt{rdd\_prf\_id} inside met data sections in configuration files.
    Several generated values are added to the resulting data set as described in \textbf{Generated options}.

    Sources of data in the priorities order:
    \begin{itemize}
        \item[-] command line options;
        \item[-] file \File{rdd.def} in the root of working directory;
        \item[-] file \Arg{rdd\_atom\_path}\File{/}\Arg{rdd\_atom\_id}\File{/atom.conf};
        \item[-] file \Arg{rdd\_atom\_path}\File{/}\Arg{r}\File{/}\Arg{rdd\_atom\_id}\File{/atom.conf},
            where \Arg{r} is the first letter of object name \Arg{rdd\_atom\_id} (hashed storage);
        \item[-] files \File{*.conf} inside \File{rdd.conf.d/} directory in the root of working directory;
        \item[-] only if there is no \File{rdd.conf.d/} in the working directory, files \File{*.conf} inside \File{BASEDIR/../etc/rdd} and \File{BASEDIR/../../etc/rdd} directories.
            \Arg{BASEDIR} is directory with current executable file.
    \end{itemize}

\section{Data dump}
    The second part of \Prog{rdd}'s work is handling dumps. Dump is the script in shell language, containing gathered data and library files.
    That part can be done separately with \Cmd{dumprdd}{1} (prints the dump to stdout) and \Cmd{droprdd}{1} (stores dump in the file).

    For each \Opt{rdd\_atom\_id} the object descriptor is created as the following:

%@% IF !MAN %@%
    \Arg{rdd\_atom\_id}\_\Arg{word1}\_\Arg{word2}\_...\Arg{wordN}
%@% ELSE %@% 
    rdd\_atom\_id\_word1\_word2\_...wordN
%@% END-IF %@% 

    \Arg{word1} to \Arg{wordN} are the content of \Opt{rdd\_prf\_entry}, given in the command line.
    It's the shortest key to identificate resulted data with current unchanged configuration files.

    Then the directory \File{\$RDD\_ROOT/var/dump/}\Arg{object\_descriptor}\File{/} is created.
    All the files constructed by \Prog{rdd} are stored here. That dir is referenced as \Arg{object\_dir} below.

    The file \Arg{object\_dir}\File{/dump.sh} is dump, constructed by \Prog{rdd}.
    It contains language variables with \SelfProg{rdd} data and inclusion of the library files (or library code in case of \Opt{inline} entries).
    Library files should not contain any execution commands at the moment.

\section{Function calls}
    The third part of \Prog{rdd}'s work is task launch. Task launch is a simple function call inside constructed script.

    Script \Arg{object\_dir}\File{/groupdump} contains launch of functions for group session: these functions are called once,
    disregarding the quantity of objects in the launch.

    Script \Arg{object\_dir}\File{/calldump} contains launch of all required functions for single object.

\section{Options}
    \begin{description}
        \item[\Opt{rdd\_prf\_entry}=\Arg{word1}\oArg{,word2,...}]
            Option to start data reading. It contains the name of data section or multiple section names inside configuration files.
            These data sections are also called data words. Data word is essential unit of data gathering, done by \Prog{rdd}.
            \medbreak
            The content of configuration files under given sections is read and variables from there are added to the data collection.
            The format and possible names of configuration files are described in manual section \textbf{Data gathering}.

        \item[\Opt{rdd\_prf\_id}=\Arg{word1}\oArg{,word2,...}]
            Option to add new section or sections in addition to already read ones. The format of the values is the same as in \Opt{rdd\_prf\_entry}.
            \medbreak
            When some new words are given to the launch, they are added to the summarised list of needed sections and data reading continues with the content of the new added ones.
            The addition is done with \Opt{rdd\_prf\_id} in command line or in config file, when this option is met within already loaded section.
            Thus any section can contain some data of its own and include data from other sections.
            \medbreak
            When the same option is assigned in several data sections, the value is taken from the first met section.
            Sections are read from right to left in the list, so the most right one has the highest priority.
            \medbreak
            This allows to combine data sets automatically with dependence on the order of the asked sections.
            \medbreak
            The limitation on the option usage: \Opt{rdd\_prf\_id} can not be used in \File{atom.conf} config files, individual for each object.

        \item[\Opt{rdd\_list\_entry}=\Arg{object1}\oArg{,object2,...}]
            Option with the name of object or multiple object names, used for task launch.
            \medbreak
            Object name can be a string or a file inside \Opt{rdd\_list\_path} directory.
            If a file with given name exists, its content is read as the list of object names, each name on separate line.
            Empty lines and comments (starting with \texttt{\#} symbol) are ignored. Names can be nested, so lists can contain names of another lists.\\
            Overall list is the summarised content of all given lists and separate names, in the order from left to right.\\
            String object names may contain the same symbols, as file paths, excluding space symbol and slash.

        \item[\Opt{rdd\_list\_path}=\Arg{dir}]
            Option with the path to the directory with object lists.
            Path can be absolute or relative, in the second case they are prepended with root of working directory.

        \item[\Opt{rdd\_map\_entry}=\Arg{func1}\oArg{,func2,...}]
            Option with the name of function or multiple function names used in the general launch for each object.

        \item[\Opt{rdd\_map\_lang}]
            Deprecated and ignored.

        \item[\Opt{rdd\_map\_libs}=\oArg{inline } \Arg{filelib1}\oArg{, \Lbr inline \Rbr  filelib2, ...}]
            Option contains list of files with implementation of launched functions.
            Library files are included in the script, built by \Prog{rdd}, in the order from left to right before the function calls.
            \medbreak
            When file is marked with modificator \Arg{inline}, its entire content is inserted into script instead of inclusion construction in the language of the file.
            This can be used for some code, which is important to be visible at the first sight inside resulting script for good readability and clearance.
            \medbreak
            The modificator \texttt{shell} is possible, which is deprecated and ignored: \Arg{inline shell func}.

        \item[\Opt{rdd\_map\_group}=\Arg{func1}\oArg{,func2,...}]
            Option with the name of function or multiple function names used once per launch session.
            \medbreak
            Functions from group are launched once for each \Prog{rdd} invocation, disregarding quantity of objects.
            For example, these functions can be used to print summarised info for the whole session, like date, environment or so.

        \item[\Opt{rdd\_map\_autopre}=\Arg{func1}\oArg{,func2,...}]
            Option with the name of function or multiple function names used in the general launch for each object before the \Opt{rdd\_map\_entry}.
            If \Opt{rdd\_map\_entry} is switched, functions from this option remain.
            \medbreak
            The option can be used to keep some generic methods which will be always present with any command line argument or data set.
            For example, such methods can do logging or user interaction, while \Opt{rdd\_map\_entry} content varies between different data sets or user choice.

        \item[\Opt{rdd\_map\_autopost}=\Arg{func1}\oArg{,func2,...}]
            Option with the name of function or multiple function names used in the general launch for each object after \Opt{rdd\_map\_entry}.
            Everything described for \Opt{rdd\_map\_autopre} applies here as well.

        \item[\Opt{rdd\_atom\_path}=\Arg{atomdir}]
            Option with the directory, containing personal object data.
            \medbreak
            For each object the following files are checked, in order:
            \begin{itemize}
                \item[-] \File{atomdir/}\Arg{objectname}\File{/atom.conf}
                \item[-] \File{atomdir/}\Arg{o}\File{/}\Arg{objectname}\File{/atom.conf}
            \end{itemize}
            The second checked path contains first letter of objectname as a hash.
            That can be used for more efficient storage of large quantity of objects.
            \medbreak
            If file is found, its data is loaded into the dataset.

        \item[\Opt{rdd\_lang\_shell}=\Arg{binsh}]
            Option with the interpretator for shell language. When given as non-absolute path, file \File{binsh} is searched according to \Envar{PATH}.
            \medbreak
            The default value is \textbf{sh}.

        \item[\Opt{rdd\_lang\_python}]
            Deprecated and ignored.

        \item[\Opt{rdd\_env\_filter}]
            Deprecated and ignored.

        \item[\Opt{rdd\_log\_num\_terminal}=\Arg{integernumber}]
            The number of file descriptor for a log file, if one of the \Opt{rdd\_tune\_stdout\_redirect} or \Opt{rdd\_tune\_stderr\_redirect} is turned on.
            \medbreak
            The value must be integer number between 3 and 9. If it is out of the range, the value is assigned to 3. The default value is 3.
            \medbreak
            This option is always available in the language function as variable, whether it is set explicitly in config files or not.

        \item[\Opt{rdd\_tune\_atom\_personal}]
            Deprecated and ignored.

        \item[\Opt{rdd\_tune\_stdout\_redirect}=1\Bar0]
            Whether the standart output (stdout) of launched code should be redirected to the file. Any value besides '1' is considered as '0'. Default value is 0.
            \medbreak
            When set to 1, stdout of script is redirected to the file, stored in \Opt{rdd\_log\_stdout}.
            \medbreak
            Output to terminal, visible to user, is still available with \Opt{rdd\_log\_num\_terminal}.

        \item[\Opt{rdd\_tune\_stderr\_redirect}=1\Bar0]
            Whether the standart error output (stderr) of launched code should be redirected to the file. Any value besides '1' is considered as '0'. Default value is 0.
            \medbreak
            When set to 1, stderr of script is redirected to the file, stored in \Opt{rdd\_log\_stdout}.

        \item[\Opt{rdd\_tune\_phase\_buffer\_off}]
            Deprecated and ignored.

        \item[\Opt{--version}]
            Print version and exit with 0 code.

        \item[\Opt{--help}]
            Print short help description and exit with 0 code.
    \end{description}

\section{Generated options}

    \Prog{rdd} generates options listed below during the work. The variables are absent in configs, but stored in data dump.
    Assigning these variables in config file will take no effect.

    \begin{description}
        \item[\Opt{rdd\_prf\_all}="\Arg{word1} \Arg{word2} \Arg{...}"]
            All data words (sections from configs), loaded in current set. The most right word was read first, \Arg{word1} the last.
            \medbreak
            In case some data variable is stored at once in two sections, present in \Opt{rdd\_prf\_all}, resulting value for that variable is taken from the most right section.

        \item[\Opt{rdd\_atom\_id}=\Arg{objectname}]
            The name of the current object. When \Opt{rdd\_list\_entry} is not set, the value is \texttt{repo}.

%@% IF !MAN %@%
        \item[\Opt{rdd\_atom\_dumpdir}=\File{\$RDD\_ROOT/var/dump/}\Arg{object\_descriptor}\File{/}]
%@% ELSE %@% 
        \item[\Opt{rdd\_atom\_dumpdir}=\$RDD\_ROOT/var/dump/object\_descriptor/]
%@% END-IF %@% 
            Directory to store personal atom's data. See section \textbf{Data dump} for information on \Arg{object\_descriptor} format.

%@% IF !MAN %@%
        \item[\Opt{rdd\_log\_stdout}=\File{\$RDD\_ROOT/var/dump/}\Arg{object\_descriptor}\File{/log}]
%@% ELSE %@% 
        \item[\Opt{rdd\_log\_stdout}=\$RDD\_ROOT/var/dump/object\_descriptor/log]
%@% END-IF %@% 
            File for output redirection, when it is turned on. See section \textbf{Data dump} for information on \Arg{object\_descriptor} format.

        \item[\Opt{rdd\_exit\_status}=\Arg{EXITCODE}]
            Option contains the code, returned by terminated subshell with the function launch.

    \end{description}


\section{Environment}
    \Envar{RDD\_ROOT} contains the root of working directory. The variable is set up by \Prog{rdd} and available for target code.
    It is calculated as following, in the priority order:
    \begin{itemize}
        \item[-] The dir in the path to current dir, where \File{rdd.def} is stored.
        \item[-] The dir in the path to current dir, where \File{rdd.conf.d/} is stored.
        \item[-] Current directory.
    \end{itemize}

    \Envar{RDD\_DEBUG} with non-zero value inserts shell tracing at the beginning of the constructed script.
    That helps to debug problems with library shell code.

\section{See also}
    \Cmd{rdd-howto}{7}, \Cmd{datardd}{1}, \Cmd{dumprdd}{1}, \Cmd{droprdd}{1}, \Cmd{lsrdd}{1}, \Cmd{rootrdd}{1}, \Cmd{make}{1}

%@% IF !embed %@%
\section{Authors}
    Random Crew\\
    \URL{https://opendistro.org/rdd}
%@% END-IF %@%

\end{Name}

\LatexManEnd
\end{document}
